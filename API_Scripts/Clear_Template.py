# import unittest
# import Renderer_Functions
# from Database import Postgres
# import random
# import configparser
# import datetime
#
# import string
#
# # for logging purposes
# lgr = Renderer_Functions.init_logger('resultfile.log')
#
#
# class VBTemplatesUpdateSuites(unittest.TestCase):
#     currentResult = None  # holds last result object passed to run method'
#     TestResult = {}
#     Summary = {}
#
#     # Read config file
#     config = configparser.ConfigParser()
#     config.sections()
#     config.read('api_config.ini')
#
#     def setUp(self):
#         print ("\n---------------------------------------------------------------------------------------------")
#         print ("Start run test case: {0}".format(str(self.id())))
#         lgr.info(self.id())
#         print ("-----------------------------------------------------------------------------------------------\n")
#
#     def run(self, result=None):
#         self.currentResult = result  # remember result for use in tearDown
#         unittest.TestCase.run(self, result)  # call superclass run method
#
#     def tearDown(self):
#         print ("\n---------------------------------------------------------------------------------------------")
#         print ("Completed running test case: {0}".format(str(self.id())))
#         print ("-----------------------------------------------------------------------------------------------\n")
#         lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
#                                            "----------------------------")
#         print (self.currentResult)
#
#     def negative_111_updateTemplate_mismatch_clear(self):
#
#             # Login and get token
#             resp = Renderer_Functions.initiate_login_admin()
#             cookie = resp["cookies"][0]
#             email = resp['content']['results']['user']['email_address']
#             cursor = Postgres.connect_to_db()
#
#             # Get random request data
#             # Get template ids
#             query_templates = "SELECT template.template_id ,template.smi_domain_id FROM renderer_text Inner join template on renderer_text.template_id = template.template_id "
#             cursor.execute(query_templates)
#             db_info = [r for r in cursor]
#             index1 = 0
#             #rows= db_info.__len__()
#             for index in db_info:
#                 # template_id = db_info[index1][0]
#                  template_id = index[0]
#
#                 # domain_id = db_info[index1][1]
#                  domain_id = index[1]
#
#                  index1=index1+1
#                  print(index1)
#                  # Get text_ssml,template_name,open_to_public values
#                  # query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE  template_id=" + str(
#                  # template_id) + " AND smi_domain_id=" + str(domain_id)
#                  query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE  template_id=" + str(
#                  template_id)
#                  print(template_id)
#                  cursor.execute(query_renderertext)
#                  db_info1 = [r for r in cursor]
#                  template_name = db_info1[0][0]
#                  text_ssml = db_info1[0][1]
#                  is_public = db_info1[0][2]
#                  query_domain = "SELECT smi_domain_owner_project_id,language_id FROM smi_domain WHERE smi_domain_id=" + str(
#                  domain_id)
#                  cursor.execute(query_domain)
#                  db_info2 = [r for r in cursor]
#                  project_id = db_info2[0][0]
#                  language_id = db_info2[0][1]
#                  # Get template words
#                  # query_temp_words = "SELECT  * FROM template_words WHERE template_id="+str(template_id)+"AND sub_field_id !="+'5'
#                  query_temp_words = "SELECT  * FROM template_words WHERE template_id=" + str(template_id)
#                  cursor.execute(query_temp_words)
#                  db_info = [r for r in cursor]
#                  words = []
#                  for x in range(db_info.__len__()):
#                      template_phonemes = []
#                      query_temp_phonemes = "SELECT word_phoneme_number,phoneme_text,stress_id,tobi_pitch_accent FROM " \
#                                            "template_words_phonemes WHERE template_id=" + str(
#                          template_id) + " AND word_number=" + str(db_info[x][1])
#                      cursor.execute(query_temp_phonemes)
#                      db_phonemes = [r for r in cursor]
#                      for y in range(db_phonemes.__len__()):
#                          template_phonemes.append({"phoneme_number": db_phonemes[y][0], "phoneme": db_phonemes[y][1],
#                                                    "stress_type": db_phonemes[y][2], "pitch_type": db_phonemes[y][3]})
#                      template_words = {
#                          "word_number": db_info[x][1],
#                          "sub_field_id": 0 if db_info[x][2] is None else db_info[x][2],
#                          "field_type_id": 0 if db_info[x][9] is None else db_info[x][9],
#                          "pos_code": db_info[x][4],
#                          "boundary_tone_id": db_info[x][5],
#                          "phrase_break_id": db_info[x][6],
#                          "focus": db_info[x][7],
#                          "compressed_f0": True if db_info[x][8] == 1 else False,
#                          "text": db_info[x][3],
#                          "sensitivity_next_word": True if db_info[x][12] == 1 else False,
#                          "sensitivity_gender": True if db_info[x][10] == 1 else False,
#                          "mood_id": 0 if db_info[x][11] is None else db_info[x][11], "prosody_speed": "100",
#                          "prosody_volume": "100", "prosody_pitch": "100",
#                          "phonemes": template_phonemes}
#                      words.append(template_words)
#
#                  #"template_name": template_name,
#                  # Set user parameters
#                  user = {"template_id": template_id, "domain_id": domain_id,
#                          "is_public": is_public, "text_ssml": text_ssml
#                      , "language_id": language_id, "project_id": project_id, "words": words}
#
#                  # Send request
#                  resp = Renderer_Functions.send_request(cookie, user, "templateupdate")
#
#                  # here we will check if the request is passed or failed
#                  str1=" parameter text_ssml and word array text are mismatched. "
#                  str2=resp["results"]["error_message"]
#                  if str1 in str2 :
#                      query_Cleartemplates = "DELETE from Template where template_id ="+str(template_id)
#                      cursor.execute(query_Cleartemplates)
#                      print("Template deleted")
#
#
#                  # if (self.assertEqual(resp['status_code'], 200)) is None \
#                  #    and (self.assertContains(resp["results"]["error_message"],
#                  #                          "parameter text_ssml and word array text are mismatched.") is None):
#                  #     query_Cleartemplates = "DELETE from Template where tmplate_id =" + str(template_id)
#                  #     cursor.execute(query_Cleartemplates)
#                  else:
#                     print("Template passed")
#
# if __name__ == '__main__':
#     print("-------------Test Result----------------\n")
#     testResult = unittest.main(verbosity=1)