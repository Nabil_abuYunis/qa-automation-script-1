import unittest
import Renderer_Functions
import configparser
from Database import Postgres
import random

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBDomainSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Get logged in email
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print( "-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Completed running test case: {0}".format(str(self.id())))
        print( "-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print( self.currentResult)

    def test_positive_1_domain(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        # # Get Random Language id
        # cursor = Postgres.connect_to_db()
        # # select only the language_id's that have domains
        # cursor.execute('SELECT DISTINCT language_id FROM smi_domain')
        # db_info = [r for r in cursor]
        # random_line = randint(0, cursor.rowcount-1)
        #
        # language_id = db_info[random_line][0]
        language_id = 6

        # Send to Domain
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"language_id": language_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")

        # Fetch info from DB
        # allowed_domains = Renderer_Functions.get_allowed_domains_in_allowed_projects_in_allowed_accounts(email, language_id)
        allowed_domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)

        # Fetch full domain info from DB to be compare with the API results
        cursor = Postgres.connect_to_db()
        str = "("
        for i in allowed_domains:
            str += "'" + repr(i[0]) + "', "
        str = str[:-2]
        str += ")"
        query = "select * from smi_domain where smi_domain_id in " + str
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])
        db_info = [x for x in db_info if x[4] == language_id]

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["smi_domain_id"])

        # here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        count = resp.__len__()
        for x in range(count):
            print(x)
            print (resp[x]["smi_domain_id"], repr(db_info[x][0]))
            print(resp[x]["name"], repr(db_info[x][1]))
            print(resp[x]["mood_id"], repr(db_info[x][11]))
            print(resp[x]["pitch"], repr(db_info[x][6]))
            print(resp[x]["volume"], repr(db_info[x][7]))
            print(resp[x]["style_id"], repr(db_info[x][3]))
            print(resp[x]["speed"], repr(db_info[x][5]))
            print(resp[x]["normalization_domain_id"], repr(db_info[x][8]))
            print(resp[x]["language_id"], repr(db_info[x][4]))
            print(resp[x]["smi_domain_owner_project_id"], repr(db_info[x][10]))
            print(resp[x]["open_to_public"], repr(db_info[x][9]))
            print(resp[x]["supported"], repr(db_info[x][12]))
            if (self.assertEqual(resp_status_code, 200) is None
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["smi_domain_id"],
                                                                     repr(db_info[x][0])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["name"],
                                                                     repr(db_info[x][1]).replace("'", "")))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["mood_id"],
                                                                     repr(db_info[x][11])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["open_to_public"],
                                                                     repr(db_info[x][9])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["pitch"],
                                                                     db_info[x][6]))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["supported"],
                                                                     repr(db_info[x][12])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["volume"],
                                                                     db_info[x][7]))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["style_id"],
                                                                     repr(db_info[x][3])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["speed"],
                                                                     db_info[x][5]))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["normalization_domain_id"],
                                                                repr(db_info[x][8])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["language_id"],
                                                                     repr(db_info[x][4])))):
                print(x)
                print(resp[x]["smi_domain_id"], repr(db_info[x][0]))
                print(resp[x]["name"], repr(db_info[x][1]))
                print(resp[x]["mood_id"], repr(db_info[x][11]))
                print(resp[x]["pitch"], repr(db_info[x][6]))
                print(resp[x]["volume"], repr(db_info[x][7]))
                print(resp[x]["style_id"], repr(db_info[x][3]))
                print(resp[x]["speed"], repr(db_info[x][5]))
                print(resp[x]["normalization_domain_id"], repr(db_info[x][8]))
                print(resp[x]["language_id"], repr(db_info[x][4]))
                print(resp[x]["smi_domain_owner_project_id"], repr(db_info[x][10]))
                print(resp[x]["open_to_public"], repr(db_info[x][9]))
                print(resp[x]["supported"], repr(db_info[x][12]))
            else:
                print("Positive Test Failed")
                assert False
        print ("Domain Pass")

    def test_positive_2_open_to_public_false(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        open_to_public = False

        # Send to Domain
        # allowed_projects = Renderer_Functions.get_allowed_projects_in_allowed_accounts(email)
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"language_id": language_id , "project_id": random_project, "open_to_public": open_to_public}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")

        # "The open_to_public is working as follows: a user can always see what belongs to him (in this case the project" \
        # " he’s in) and that is regardless of their open_to_public status. Now, in the request, if you ask for " \
        # "open_to_public, it adds for you also all the domains that are open_to_public regardless of the project " \
        # "they belong to.

        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)

        domains = sorted(domains, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["smi_domain_id"])

        # Here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if resp.__len__() != domains.__len__():
            assert False

        count = resp.__len__()
        for x in range(count):
            print(x)
            print(resp[x]["smi_domain_id"], repr(domains[x][0]))
            if (self.assertEqual(resp_status_code, 200) is None
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["smi_domain_id"],
                                                                repr(domains[x][0])))):
                print(x)
                print(resp[x]["smi_domain_id"], repr(domains[x][0]))

            else :
                print("Positive Test Failed")
                assert False
        print ("Domain Pass")

    def test_positive_3_open_to_public_true(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        open_to_public = True

        # Send to Domain
        # allowed_projects = Renderer_Functions.get_allowed_projects_in_allowed_accounts(email)
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"language_id": language_id, "project_id": random_project, "open_to_public": open_to_public}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")

        # user1 = {"language_id": language_id, "project_id": random_project, "open_to_public": False}
        # resp1 = Renderer_Functions.send_request(cookies, user1, "domain")

        # "The open_to_public is working as follows: a user can always see what belongs to him (in this case the project" \
        # " he’s in) and that is regardless of their open_to_public status. Now, in the request, if you ask for " \
        # "open_to_public, it adds for you also all the domains that are open_to_public regardless of the project " \
        # "they belong to.

        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)

        cursor = Postgres.connect_to_db()
        query = "select smi_domain_id from smi_domain where open_to_public = true and language_id = " + str(language_id) + " " \
                "and supported = true and smi_domain_owner_project_id ilike '" + random_project + "'"
        cursor.execute(query)
        domains_true = [r for r in cursor]

        # combine the two domains lists
        domains = list(set(domains + domains_true))


        domains = sorted(domains, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["smi_domain_id"])

        # Here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if resp.__len__() != domains.__len__():
            assert False

        count = resp.__len__()
        for x in range(count):
            print(x)
            print(resp[x]["smi_domain_id"], repr(domains[x][0]))
            if (self.assertEqual(resp_status_code, 200) is None
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["smi_domain_id"],
                                                                     repr(domains[x][0])))):
                print(x)
                print(resp[x]["smi_domain_id"], repr(domains[x][0]))

            else:
                print("Positive Test Failed")
                assert False
        print("Domain Pass")

    def test_positive_4_open_supported_true(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        send_unsupported = False

        # Send to Domain
        # allowed_projects = Renderer_Functions.get_allowed_projects_in_allowed_accounts(email)
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"language_id": language_id , "project_id": random_project, "send_unsupported": send_unsupported}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")
        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["smi_domain_id"])

        # "The open_to_public is working as follows: a user can always see what belongs to him (in this case the project" \
        # " he’s in) and that is regardless of their open_to_public status. Now, in the request, if you ask for " \
        # "open_to_public, it adds for you also all the domains that are open_to_public regardless of the project " \
        # "they belong to.

        # Fetch the info from the Domain table
        cursor = Postgres.connect_to_db()
        send_unsupported = True
        query = "select smi_domain_id from smi_domain where language_id = " + str(
            language_id) + "and supported = " + str(send_unsupported) + " and smi_domain_owner_project_id ilike '" + random_project + "'"
        cursor.execute(query)
        domains = [r for r in cursor]

        domains = sorted(domains, key=lambda k: k[0])

        # Here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if resp.__len__() != domains.__len__():
            assert False

        count = resp.__len__()
        for x in range(count):
            print(x)
            print(resp[x]["smi_domain_id"], repr(domains[x][0]))
            if (self.assertEqual(resp_status_code, 200) is None
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["smi_domain_id"],
                                                                     repr(domains[x][0])))):
                print(x)
                print(resp[x]["smi_domain_id"], repr(domains[x][0]))

            else:
                print("Positive Test Failed")
                assert False
        print("Domain Pass")

    def test_positive_5_open_supported_false(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        send_unsupported = True

        # Send to Domain
        # allowed_projects = Renderer_Functions.get_allowed_projects_in_allowed_accounts(email)
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"language_id": language_id , "project_id": random_project, "send_unsupported": send_unsupported}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")
        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["smi_domain_id"])

        # "The open_to_public is working as follows: a user can always see what belongs to him (in this case the project" \
        # " he’s in) and that is regardless of their open_to_public status. Now, in the request, if you ask for " \
        # "open_to_public, it adds for you also all the domains that are open_to_public regardless of the project " \
        # "they belong to.

        # Fetch the info from the Domain table
        cursor = Postgres.connect_to_db()
        send_unsupported = False
        # query = "select smi_domain_id from smi_domain where language_id = " + str(
        #     language_id) + " and supported = " + str(send_unsupported) + " and smi_domain_owner_project_id ilike '" + random_project + "'"
        query = "select smi_domain_id from smi_domain where language_id = " + str(language_id) + " and smi_domain_owner_project_id ilike '" + random_project + "'"
        cursor.execute(query)
        domains = [r for r in cursor]

        domains = sorted(domains, key=lambda k: k[0])

        # Here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if resp.__len__() != domains.__len__():
            assert False

        count = resp.__len__()
        for x in range(count):
            print(x)
            print(resp[x]["smi_domain_id"], repr(domains[x][0]))
            if (self.assertEqual(resp_status_code, 200) is None
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["smi_domain_id"],
                                                                     repr(domains[x][0])))):
                print(x)
                print(resp[x]["smi_domain_id"], repr(domains[x][0]))

            else:
                print("Positive Test Failed")
                assert False
        print("Domain Pass")

    def test_negative_6_domain_incorrect_language_id(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 99999
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, 6)
        random_project = random.choice(allowed_projects)
        user = {"language_id": language_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")
        print(resp["status_code"])
        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["count"], 0) is None):
            # and (self.assertEqual(resp['results']["error message"],
            #                       "Incorrect value for language_id.") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["count"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_7_domain_missing_project_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        user = {"language_id": language_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "missing parameter project_id.") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_8_domain_missing_language_id(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "missing parameter language_id.") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_9_domain_wrong_login(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"language_id": language_id, "send_unsupported": True, "project_id": random_project}
        resp = Renderer_Functions.send_request("123456789", user, "domain")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_10_domain_missing_login(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Parameters
        user = {"language_id": language_id, "send_unsupported": True, "project_id": random_project}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "domain")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_11_expired_token(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Parameters
        user = {"language_id": language_id, "send_unsupported": True, "project_id": random_project}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "domain")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False
            

if __name__ == '__main__':
    print( "-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)