import unittest
import Renderer_Functions
from Database import Postgres
import random

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBLingoSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)

    def test_positive_1_lingo(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        cursor = Postgres.connect_to_db()
        # Select domain that have lingo in it
        query = "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND " \
                "open_to_public = true AND smi_domain_id IN(select distinct smi_domain_id from smi_domain_lingo)"
        # cursor.execute(
        #     "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])

        smi_domain_id = 1099511627788
        user = {"smi_domain_id": smi_domain_id}

        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "lingo")

        # Fetch the info from the Database
        # query = "SELECT smi_domain_id, domain_name FROM smi_domain WHERE smi_domain_id = " + str(user['smi_domain_id'])
        # Get the lingo of the chosen domain and it's parents too
        lingos = list()
        query = "select distinct lingo_id from smi_domain_lingo where smi_domain_id = " + repr(smi_domain_id)
        cursor.execute(query)
        db_info = [r for r in cursor]
        lingos.append(db_info)
        while True:
                query = "select parent_id from smi_domain where smi_domain_id = " + repr(smi_domain_id)
                cursor.execute(query)
                parent_domain = [r for r in cursor]
                if parent_domain[0][0] is None:
                    break
                else:
                    smi_domain_id = parent_domain[0][0]
                    query = "select distinct lingo_id from smi_domain_lingo where smi_domain_id = " + repr(smi_domain_id)
                    cursor.execute(query)
                    db_info = [r for r in cursor]
                    lingos.append(db_info)

        newLingos = list()
        for listL in lingos:
            listL = [i[0] for i in listL]
            newLingos.append(listL)

        flat_list = []
        for sublist in newLingos:
            for item in sublist:
                flat_list.append(item)

        flat_list = list(set(flat_list))
        flat_list.sort()

        # str = "("
        # for i in flat_list:
        #     str += "'" + repr(i) + "', "
        # str = str[:-2]
        # str += ")"
        #
        # query = "select distinct smi_domain_id from smi_domain_lingo where lingo_id in " +  str
        # cursor.execute(query)
        # db_info = [r for r in cursor]
        # db_info = sorted(db_info, key=lambda k: k[0])

        # here we will check if the request is passed or failed
        count = resp['results'].__len__()

        # If they are not the same amount then fail
        if resp["results"].__len__() != flat_list.__len__():
            assert False

        for x in range(count):
            if (self.assertEqual(resp['status_code'], 200)) is None \
                    and (self.assertEqual(resp['results'][x]["lingo_id"], str(flat_list[x])) is None) :
                print ("Lingo line: {0}".format(resp['results'][x]))
            else:
                print("Positive Test Failed")
                assert False

        print("Lingo Pass")

    def test_negative_2_missing_param(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "lingo")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "missing parameter smi_domain_id.") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_3_incorrect_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {"smi_domain_id": "hello"}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "lingo")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "incorrect value for smi_domain_id.") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_4_wrong_login(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.send_request("123456789", user, "lingo")

        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_5_domain_missing_login(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "lingo")

        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_6_expired_token(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "lingo")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)