import unittest
import Renderer_Functions
import configparser
import random
import string

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBLoginSuites(unittest.TestCase):

    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Read config file
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)

    def test_positive_1_login_verify_user_can_login_with_valid_username_password(self):
        # parameters
        email = self.config.get('LOGIN', 'email')
        password = self.config.get('LOGIN', 'password')
        user = {"email": email, "password" : password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            email_address = resp["content"]["results"]["user"]["email_address"]
            if (self.assertEqual(resp["status_code"], 200)) is None \
                    and (self.assertEqual(email_address, email) is None):
                print ("-|-Result: :Login Test case Pass")
                # print ("resp: {0}".format(resp))
                print ("User: {0}".format(resp["content"]["results"]["user"]["email_address"]))
                print ("Token: {0}".format(resp["cookies"]))
                print ("Status code: {0}".format(resp["status_code"]))

        # Check error type
        else:
            print ("-|-Result:Test case Failed")
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))
            print ("Error Code: {0}".format(resp["error_code"]))
            assert False

    def test_negative_2_login_verify_user_cant_login_with_invalid_email(self):
        # Parameters
        email = "hung@speechmorphing"
        password = self.config.get('LOGIN', 'password')
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        # resp_json1 = json.loads(resp['content']["results"]["error_message"])
        print(resp['content']['results']['error_message'])
        if resp['status_code'] == 200:
            if (self.assertEqual(resp['content']['results']['error_message'], " invalid email address.") is None):
                print ("-|-Result:Test case Pass")
                print ("test_login_verify_user_cant_login_with_invalid_email")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Error Code: {0}".format(resp["content"]['results']["error_message"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_3_login_verify_user_cant_login_with_invalid_password(self):
        # parameters
        email = self.config.get('LOGIN', 'email')
        password = "abc456"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp['status_code'] == 200 \
            and (self.assertEqual(resp["content"]["results"]["error_message"],"incorrect Password.") is None) :
                print ("-|-Result:Test case Pass")
                print ("test_login_verify_user_cant_login_with_invalid_email")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Error Code: {0}".format(resp["content"]["results"]["error_message"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_4_login_verify_user_cant_login_with_empty_password(self):
        # parameters
        email = self.config.get('LOGIN', 'email')
        password = ""
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['content']['results']["error_message"], "Invalid Email or Pasword.") is None):
                print ("-|-Result:Test case Pass")
                print ("test_login_verify_user_cant_login_with_invalid_email")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp['content']['results']["error_message"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_5_login_verify_user_cant_login_with_empty_email(self):
        # Parameters
        email = ""
        password = self.config.get('LOGIN', 'password')
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['content']['results']["error_message"], "Invalid Email or Pasword.") is None):
                print ("-|-Result:Test case Pass")
                print ("test_login_verify_user_cant_login_with_invalid_email")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp['content']['results']["error_message"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_6_missing_email(self):
        # Parameters
        password = self.config.get('LOGIN', 'password')
        user = {"password": password}
        resp = Renderer_Functions.login(user)
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["content"]["results"]["error_message"], "Invalid Email or Pasword.") is None):
                print ("-|-Result:Test case Pass")
                print ("test_login_verify_user_cant_login_with_invalid_email")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["content"]["results"]["error_message"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_7_login_nonregistered_email(self):
        #Parameters
        email = ''.join(random.choice(string.ascii_letters) for ii in range(5)) + '@speechmorphing.com'
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp['content']["results"]["error_message"], "user not Found with details.") is None) :
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Reason: {0}".format(resp['content']["results"]["error_message"]))
                print ("Error Code: {0}".format(resp["status_code"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_8_login_activate_account(self):
        #Parameters
        email = ''.join(random.choice(string.ascii_letters) for ii in range(5)) + '@speechmorphing.com'
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        email = resp['results']["user"]["email_address"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp['content']["results"]["error_message"], "please activate your account.") is None) :
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp['content']["results"]["error_message"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_9_login_missing_password(self):
        # parameters
        email = self.config.get('LOGIN', 'email')
        user = {"email": email}
        resp = Renderer_Functions.login(user)
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["content"]["results"]["error_message"], "Invalid Email or Pasword.") is None):
                print("-|-Result:Test case Pass")
                print("test_login_verify_user_cant_login_with_invalid_email")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp["content"]["results"]["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_10_login_password_too_short(self):
        # parameters
        email = self.config.get('LOGIN', 'email')
        password = "abc"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["content"]["results"]["error_message"], "password too short.") is None):
                print("-|-Result:Test case Pass")
                print("test_login_verify_user_cant_login_with_invalid_email")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp["content"]["results"]["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
