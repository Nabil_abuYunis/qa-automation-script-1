import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
import string
from random import randint
import configparser
from Database import Postgres
import JSON_Request as Data_Utils
import WAV_Logs as wav

class VBTemplateSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        print(self.currentResult)

    def test_positive_1_say_temp(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "yes"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                                'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                                'is open on a holiday?</speak>'
                    smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                    request_id = "654334"
                    #request_id = "123456"
                    smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                    #file_format = "wav"

                    template_original = "no"

                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                            "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
                    resp = Renderer_Functions.say_templates(resp["cookies"][0], user)

                    print("***********VALIDATING API RESPONSE**********")
                    if resp == None:
                        print("-----No response back-----")

                    print("Response Status Code: {0}".format(resp["status_code"]))
                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp["status_code"], 200)) is None:
                        if wav.check_wav_files(resp,"",True):
                            print ("Get Voice Pass")
                            print ("resp: {0}".format(resp))
                            print ("status_code: {0}".format(resp["status_code"]))
                            print ("Filename: {0}".format(resp['results']['filename']))
                    else:
                        print("FAILURE: response code was not 200")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_positive_16_say_temp_pcmu(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "yes"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                                'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                                'is open on a holiday?</speak>'
                    smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                    request_id = "654334"
                    #request_id = "123456"
                    smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                    #file_format = "wav"

                    template_original = "no"

                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                            "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
                    resp = Renderer_Functions.say_templates(resp["cookies"][0], user)

                    print("***********VALIDATING API RESPONSE**********")
                    if resp == None:
                        print("-----No response back-----")

                    print("Response Status Code: {0}".format(resp["status_code"]))
                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp["status_code"], 200)) is None:
                        if wav.check_wav_files(resp,"",True):
                            print ("Get Voice Pass")
                            print ("resp: {0}".format(resp))
                            print ("status_code: {0}".format(resp["status_code"]))
                            print ("Filename: {0}".format(resp['results']['filename']))
                    else:
                        print("FAILURE: response code was not 200")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_8_incorrect_domain(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "no"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                                'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                                'is open on a holiday?</speak>'
                    smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                    request_id = "658945"
                    #request_id = "123456"
                    smi_domain_id = "precise"
                    #file_format = "wav"

                    template_original = "no"

                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                            "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
                    resp = Renderer_Functions.say_templates(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        print("###")
                        print(resp)
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          "Incorrect value for smi_domain_id.") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_9_incorrect_voice(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "no"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                                'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                                'is open on a holiday?</speak>'
                    smi_voice_id = "precise"
                    request_id = "658945"
                    #request_id = "123456"
                    smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                    #file_format = "wav"

                    template_original = "no"

                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                            "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
                    resp = Renderer_Functions.say_templates(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          "Incorrect value for smi_voice_id.") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))


    def test_negative_3_missing_domain_param(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "no"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                                'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                                'is open on a holiday?</speak>'
                    smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                    request_id = "123456"
                    #file_format = "wav"

                    template_original = "no"

                    user = {"smi_voice_id": smi_voice_id,"text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original, "request_id" : request_id}
                    resp = Renderer_Functions.say_templates(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          "Missing Parameter smi_domain_id") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_13_missing_req_id_param(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "no"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                                'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                                'is open on a holiday?</speak>'
                    smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                    #request_id = "123456"
                    #file_format = "wav"
                    smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                    template_original = "no"

                    user = {"smi_voice_id": smi_voice_id,"text_ssml": text_ssml, "smi_domain_id":smi_domain_id, "template_id" : template_id, "template_original":template_original}
                    resp = Renderer_Functions.say_templates(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          "Missing Parameter request_id") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_14_missing_voice_param(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "no"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                                'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                                'is open on a holiday?</speak>'
                    #smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                    request_id = "123456"
                    #file_format = "wav"
                    smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                    template_original = "no"

                    user = {"request_id": request_id, "text_ssml": text_ssml, "smi_domain_id":smi_domain_id, "template_id" : template_id, "template_original":template_original}
                    resp = Renderer_Functions.say_templates(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          "Missing Parameter smi_voice_id") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_15_missing_template_param(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "no"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                                'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                                'is open on a holiday?</speak>'
                    smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                    request_id = "123456"
                    #file_format = "wav"
                    smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                    template_original = "no"

                    user = {"request_id": request_id, "text_ssml": text_ssml, "smi_domain_id":smi_domain_id, "smi_voice_id" : smi_voice_id, "template_original":template_original}
                    resp = Renderer_Functions.say_templates(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          "Missing Parameter template_id") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))


    def test_negative_16_missing_template_orig_param(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "no"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                                'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                                'is open on a holiday?</speak>'
                    smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                    request_id = "123456"
                    #file_format = "wav"
                    smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                    template_original = "no"

                    user = {"request_id": request_id, "text_ssml": text_ssml, "smi_domain_id":smi_domain_id, "smi_voice_id" : smi_voice_id, "template_id":template_id}
                    resp = Renderer_Functions.say_templates(resp["cookies"][0], user)
                    print(resp["status_code"])
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          "Missing Parameter template_original") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_11_incorrect_template_original(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "no"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                                'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                                'is open on a holiday?</speak>'
                    smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                    request_id = "658945"
                    #request_id = "123456"
                    smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                    #file_format = "wav"

                    template_original = 123456789

                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                            "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
                    resp = Renderer_Functions.say_templates(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          "Incorrect value for template_original.") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_14_incorrect_ssml(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "no"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    text_ssml = 123456789
                    smi_voice_id = "72057594037928293"
                    request_id = "123456"
                    #file_format = "wav"
                    smi_domain_id = "72057594037927955"
                    template_original = "no"

                    user = {"smi_voice_id": smi_voice_id,"text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original, "request_id" : request_id, "smi_domain_id": smi_domain_id}
                    resp = Renderer_Functions.say_templates(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          "Incorrect value for text_ssml.") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_16_incorrect_pcmu(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "no"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    text_ssml = "To build an empire"
                    smi_voice_id = "72057594037928293"
                    request_id = "123456"
                    file_format = 123456
                    smi_domain_id = "72057594037927955"
                    template_original = "no"

                    user = {"smi_voice_id": smi_voice_id,"text_ssml": text_ssml, "template_id" : template_id, "file_format": file_format, "template_original":template_original, "request_id" : request_id, "smi_domain_id": smi_domain_id}
                    resp = Renderer_Functions.say_templates(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          "Incorrect value for format.") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_15_missing_ssml(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "no"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    smi_voice_id = "72057594037928293"
                    request_id = "123456"
                    #file_format = "wav"
                    smi_domain_id = "72057594037927955"
                    template_original = "no"

                    user = {"smi_voice_id": smi_voice_id, "template_id" : template_id, "template_original":template_original, "request_id" : request_id, "smi_domain_id": smi_domain_id}
                    resp = Renderer_Functions.say_templates(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          "Missing Parameter text_ssml.") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))


    def test_negative_10_blank_text(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "no"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    text_ssml = ''
                    smi_voice_id = '72057594037928282'
                    request_id = "654334"
                    #request_id = "123456"
                    smi_domain_id = '72057594037927948'

                    template_original = "no"

                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                            "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
                    resp = Renderer_Functions.say_templates("123456789", user)

                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], '401')) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Unauthorized Access") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["details"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] == 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_4_age_missing_login(self):
        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "654334"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        template_id = "568798633"
        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
        resp = Renderer_Functions.say_templates("NO TOKEN", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")
            
    def test_negative_11_age_wrong_login(self):
        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "654334"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        template_id = "568798633"
        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
        resp = Renderer_Functions.say_templates("123456789", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

    def test_negative_6_expired_token(self):
        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "654334"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        template_id = "568798633"
        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}

        resp = Renderer_Functions.say_templates("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

    def test_negative_5_no_permission_domain(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "no"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                                'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927954" version="1.0">Where can we go to eat that '+\
                                'is open on a holiday?</speak>'
                    smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                    request_id = "785435"
                    #request_id = "123456"
                    smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                    #file_format = "wav"

                    template_original = "no"

                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                            "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
                    resp = Renderer_Functions.say_templates(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          " you do not have permission to us this domain. ") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_6_no_permission_voice(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "no"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037927968" xml:mood="4" xml:style="72057594037927938" '+\
                                'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                                'is open on a holiday?</speak>'
                    smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                    request_id = "785435"
                    #request_id = "123456"
                    smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                    #file_format = "wav"

                    template_original = "no"

                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                            "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
                    resp = Renderer_Functions.say_templates(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          "you do not have permission to us this voice. ") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_7_no_permission_lang(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    user = {"template": "no"}
                    resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
                    resp_extract = resp2["results"][0]["list"]
                    count = resp2["results"][0]["count"]
                    # Choose a random row to test
                    random_line = randint(0, count - 1)
                    template_id = resp_extract[random_line]["template_id"]
                    print(template_id)

                    # Parameters
                    text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="200" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                                'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                                'is open on a holiday?</speak>'
                    smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                    request_id = "785435"
                    #request_id = "123456"
                    smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                    #file_format = "wav"

                    template_original = "no"

                    user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                            "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
                    resp = Renderer_Functions.say_templates(resp["cookies"][0], user)

                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], '401')) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          " you do not have permission to use this language. ") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] == 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
