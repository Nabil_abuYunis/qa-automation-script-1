import unittest
import Renderer_Functions
from Database import Postgres
import random
import string
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBovcfeedbackSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Get logged in email
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)


    def test_positive_1_OVC_Feedback(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()

        # Parameters
        cursor = Postgres.connect_to_db()
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true AND open_to_public = true")
        cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true ")
        db_info = [r for r in cursor]
        voice_id = int(random.choice(db_info)[0])

        query = "select labeling_id, lab_file_name from nvc_labels order by random() limit 1"
        cursor.execute(query)
        db_info = [r for r in cursor]
        labeling_id = db_info[0][0]
        filename = db_info[0][1]

        comment = "OVC feedback test " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        user = {"voice_id": voice_id, "labeling_id": labeling_id, "filename": filename, "comment": comment}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "ovcfeedback")
        resp_status_code = resp["status_code"]

        # Fetch the info from the Database
        query = "select * from nvc_labels_to_qa where voice_id = " + str(voice_id) + " and labeling_id = " \
                "'" + str(labeling_id) + "' and nvc_file_name ilike '" + filename + "' and comments ilike '"+ comment + "'"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["id"], db_info[0][0]) is None) \
            and (self.assertEqual(voice_id, db_info[0][0]) is None) \
            and (self.assertEqual(resp["results"]["message"], "success") is None) \
            and (self.assertEqual(filename, db_info[0][2]) is None) \
            and (self.assertEqual(labeling_id, db_info[0][1]) is None) \
            and (self.assertEqual(comment, db_info[0][4]) is None):
            print("resp: {0}".format(resp))
            print("filename: {0}".format(filename))
            print("voice_id: {0}".format(voice_id))
            print("labeling_id: {0}".format(labeling_id))
            print("comment: {0}".format(comment))
        else:
            print("Positive Test Failed")
            assert False
        print("OVC_Feedback Pass")

    def test_negative_2_OVC_Feedback_wrong_login(self):

        # Parameters
        cursor = Postgres.connect_to_db()
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true AND open_to_public = true")
        cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true ")
        db_info = [r for r in cursor]
        voice_id = int(random.choice(db_info)[0])

        query = "select labeling_id, lab_file_name from nvc_labels order by random() limit 1"
        cursor.execute(query)
        db_info = [r for r in cursor]
        labeling_id = db_info[0][0]
        filename = db_info[0][1]

        comment = "OVC feedback test " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        user = {"voice_id": voice_id, "labeling_id": labeling_id, "filename": filename, "comment": comment}
        resp = Renderer_Functions.send_request("123456789", user, "ovcfeedback")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_3_OVC_Feedback_missing_login(self):

        # Parameters
        cursor = Postgres.connect_to_db()
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true AND open_to_public = true")
        cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true ")
        db_info = [r for r in cursor]
        voice_id = int(random.choice(db_info)[0])

        query = "select labeling_id, lab_file_name from nvc_labels order by random() limit 1"
        cursor.execute(query)
        db_info = [r for r in cursor]
        labeling_id = db_info[0][0]
        filename = db_info[0][1]

        comment = "OVC feedback test " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        user = {"voice_id": voice_id, "labeling_id": labeling_id, "filename": filename, "comment": comment}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "ovcfeedback")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_4_OVC_Feedback_expired_token(self):

        # Parameters
        cursor = Postgres.connect_to_db()
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true AND open_to_public = true")
        cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true ")
        db_info = [r for r in cursor]
        voice_id = int(random.choice(db_info)[0])

        query = "select labeling_id, lab_file_name from nvc_labels order by random() limit 1"
        cursor.execute(query)
        db_info = [r for r in cursor]
        labeling_id = db_info[0][0]
        filename = db_info[0][1]

        comment = "OVC feedback test " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        user = {"voice_id": voice_id, "labeling_id": labeling_id, "filename": filename, "comment": comment}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "ovcfeedback")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_5_missing_voice_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()

        # Parameters
        cursor = Postgres.connect_to_db()

        query = "select labeling_id, lab_file_name from nvc_labels order by random() limit 1"
        cursor.execute(query)
        db_info = [r for r in cursor]
        labeling_id = db_info[0][0]
        filename = db_info[0][1]

        comment = "OVC feedback test " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        user = {"labeling_id": labeling_id, "filename": filename, "comment": comment}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "ovcfeedback")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["error_message"], "missing parameter voice_id.") is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_6_wrong_voice_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()

        # Parameters
        cursor = Postgres.connect_to_db()

        voice_id = "wrong"

        query = "select labeling_id, lab_file_name from nvc_labels order by random() limit 1"
        cursor.execute(query)
        db_info = [r for r in cursor]
        labeling_id = db_info[0][0]
        filename = db_info[0][1]

        comment = "OVC feedback test " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        user = {"voice_id": voice_id, "labeling_id": labeling_id, "filename": filename, "comment": comment}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "ovcfeedback")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
                and (self.assertEqual(resp["error_message"], "incorrect value for voice_id.") is None):
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_7_missing_labeling_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()

        # Parameters
        cursor = Postgres.connect_to_db()
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true AND open_to_public = true")
        cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true ")
        db_info = [r for r in cursor]
        voice_id = int(random.choice(db_info)[0])

        query = "select labeling_id, lab_file_name from nvc_labels order by random() limit 1"
        cursor.execute(query)
        db_info = [r for r in cursor]
        filename = db_info[0][1]

        comment = "OVC feedback test " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        user = {"voice_id": voice_id, "filename": filename, "comment": comment}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "ovcfeedback")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
                and (self.assertEqual(resp["error_message"], "missing parameter labeling_id.") is None):
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_8_wrong_labeling_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()

        # Parameters
        cursor = Postgres.connect_to_db()
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true AND open_to_public = true")
        cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true ")
        db_info = [r for r in cursor]
        voice_id = int(random.choice(db_info)[0])

        query = "select labeling_id, lab_file_name from nvc_labels order by random() limit 1"
        cursor.execute(query)
        db_info = [r for r in cursor]
        labeling_id = "wrong"
        filename = db_info[0][1]

        comment = "OVC feedback test " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        user = {"voice_id": voice_id, "labeling_id": labeling_id, "filename": filename, "comment": comment}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "ovcfeedback")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
                and (self.assertEqual(resp["error_message"], "incorrect value for labeling_id.") is None):
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_9_missing_filename(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()

        # Parameters
        cursor = Postgres.connect_to_db()
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true AND open_to_public = true")
        cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true ")
        db_info = [r for r in cursor]
        voice_id = int(random.choice(db_info)[0])

        query = "select labeling_id, lab_file_name from nvc_labels order by random() limit 1"
        cursor.execute(query)
        db_info = [r for r in cursor]
        labeling_id = db_info[0][0]

        comment = "OVC feedback test " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        user = {"voice_id": voice_id, "labeling_id": labeling_id, "comment": comment}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "ovcfeedback")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
                and (self.assertEqual(resp["error_message"], "missing parameter filename.") is None):
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_10_wrong_filename(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()

        # Parameters
        cursor = Postgres.connect_to_db()
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true AND open_to_public = true")
        cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true ")
        db_info = [r for r in cursor]
        voice_id = int(random.choice(db_info)[0])

        query = "select labeling_id, lab_file_name from nvc_labels order by random() limit 1"
        cursor.execute(query)
        db_info = [r for r in cursor]
        labeling_id = db_info[0][0]
        filename = 123456789

        comment = "OVC feedback test " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        user = {"voice_id": voice_id, "labeling_id": labeling_id, "filename": filename, "comment": comment}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "ovcfeedback")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
                and (self.assertEqual(resp["error_message"], "incorrect value for filename.") is None):
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_11_missing_comment(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()

        # Parameters
        cursor = Postgres.connect_to_db()
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true AND open_to_public = true")
        cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true ")
        db_info = [r for r in cursor]
        voice_id = int(random.choice(db_info)[0])

        query = "select labeling_id, lab_file_name from nvc_labels order by random() limit 1"
        cursor.execute(query)
        db_info = [r for r in cursor]
        labeling_id = db_info[0][0]
        filename = db_info[0][1]

        user = {"voice_id": voice_id, "labeling_id": labeling_id, "filename": filename}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "ovcfeedback")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
                and (self.assertEqual(resp["error_message"], "missing parameter comment.") is None):
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    def test_negative_12_wrong_comment(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()

        # Parameters
        cursor = Postgres.connect_to_db()
        # cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true AND open_to_public = true")
        cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true ")
        db_info = [r for r in cursor]
        voice_id = int(random.choice(db_info)[0])

        query = "select labeling_id, lab_file_name from nvc_labels order by random() limit 1"
        cursor.execute(query)
        db_info = [r for r in cursor]
        labeling_id = db_info[0][0]
        filename = db_info[0][1]

        comment = 123456789

        user = {"voice_id": voice_id, "labeling_id": labeling_id, "filename": filename, "comment": comment}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "ovcfeedback")
        resp_status_code = resp["status_code"]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
                and (self.assertEqual(resp["error_message"], "incorrect value for comment.") is None):
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
