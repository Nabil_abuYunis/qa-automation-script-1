import urllib2
import unittest
import flask
import json
import Renderer_Functions as Renderer_Functions


class VBLoginSuites(unittest.TestCase):

    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Start run test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Completed running test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"
        print self.currentResult

    def test_positive_1_login_verify_user_can_login_with_valid_username_password(self):
        # parameters
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "-|-Result:Test case Pass"
                    print "resp: {0}".format(resp)
                    print "User: {0}".format(resp["content"]["Results"]["User"]["email_address"])
                    print "Token: {0}".format(resp["cookies"])
                    print "Status code: {0}".format(resp["status_code"])

            except urllib2.URLError as e:   # if response code is 200 but the fetched email is
                                            # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "-|-Result:Test case Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])
            print "Error Code: {0}".format(resp["error_code"])

    def test_negative_2_login_verify_user_cant_login_with_invalid_email(self):
        # Parameters
        email = "hung@speechmorphing"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        resp_json1 = json.loads(resp["content"]["Results"]["Error Message"])

        if resp["status_code"] == 200 and resp["content"]["Results"]["Error Code"] == 400:
            try:
                if (self.assertEqual(resp["content"]["Results"]["Error Code"], 400)) is None \
                        and (self.assertEqual(resp_json1["status_code"], "InvalidEmailOrPassword") is None) \
                        and (self.assertEqual(resp_json1["details"], "User not Found with details.") is None):
                    print "-|-Result:Test case Pass"
                    print "test_login_verify_user_cant_login_with_invalid_email"
                    print "resp: {0}".format(resp)
                    print "Status Code: {0}".format(resp_json1["status_code"])
                    print "Reason: {0}".format(resp_json1["details"])
                    print "Error Code: {0}".format(resp["content"]["Results"]["Error Code"])

            except urllib2.URLError as e:
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 401:
            print "Negative Test Failed"

    def test_negative_3_login_verify_user_cant_login_with_invalid_password(self):
        # Parameters
        email = "hung@speechmorphing.com"
        password = "abc456"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        resp_json1 = json.loads(resp["content"]["Results"]["Error Message"])

        if resp["status_code"] == 200 and resp["content"]["Results"]["Error Code"] == 400:
            try:
                if (self.assertEqual(resp["content"]["Results"]["Error Code"], 400)) is None \
                        and (self.assertEqual(resp_json1["status_code"], "InvalidEmailOrPassword") is None) \
                        and (self.assertEqual(resp_json1["details"], "Incorrect Password.") is None):
                    print "-|-Result:Test case Pass"
                    print "test_login_verify_user_cant_login_with_invalid_email"
                    print "resp: {0}".format(resp)
                    print "Status Code: {0}".format(resp_json1["status_code"])
                    print "Reason: {0}".format(resp_json1["details"])
                    print "Error Code: {0}".format(resp["content"]["Results"]["Error Code"])

            except urllib2.URLError as e:
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 401:
            print "Negative Test Failed"

    def test_negative_4_login_verify_user_cant_login_with_empty_password(self):
        # Parameters
        email = "hung@speechmorphing.com"
        password = ""
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200 and resp["status_code"] == 400:
            try:
                if (self.assertEqual(resp["status_code"], 400)) is None \
                        and (self.assertEqual(resp["error_code"], 1) is None) \
                        and (self.assertEqual(resp["details"], " The password cannot be blank.") is None):
                    print "-|-Result:Test case Pass"
                    print "test_login_verify_user_cant_login_with_invalid_email"
                    print "resp: {0}".format(resp)
                    print "Status Code: {0}".format(resp["status_code"])
                    print "Reason: {0}".format(resp["details"])
                    print "Error Code: {0}".format(resp["error_code"])

            except urllib2.URLError as e:
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 400:
            print "Negative Test Failed"

    def test_negative_5_login_verify_user_cant_login_with_empty_email(self):
        # Parameters
        email = ""
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200 and resp["status_code"] == 400:
            try:
                if (self.assertEqual(resp["status_code"], 400)) is None \
                        and (self.assertEqual(resp["error_code"], 1) is None) \
                        and (self.assertEqual(resp["details"], "The email cannot be blank.") is None):
                    print "-|-Result:Test case Pass"
                    print "test_login_verify_user_cant_login_with_invalid_email"
                    print "resp: {0}".format(resp)
                    print "Status Code: {0}".format(resp["status_code"])
                    print "Reason: {0}".format(resp["details"])
                    print "Error Code: {0}".format(resp["error_code"])

            except urllib2.URLError as e:
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 400:
            print "Negative Test Failed"

if __name__ == '__main__':
    print "-------------Test Result----------------\n"
    testResult = unittest.main(verbosity=1)
