import urllib2
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from random import randint


class VBVersionSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Start run test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Completed running test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"
        print self.currentResult

    def test_positive_1_version(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    user = {}
                    resp = Renderer_Functions.version(resp["cookies"][0], user)

                    # # Fetch the info from Database
                    # cursor = Renderer_Functions.connect_to_db()
                    # cursor.execute(
                    #     "select style_id, style_name as name, color, supported from style  ORDER BY style_id")
                    #
                    # db_info = [r for r in cursor]
                    # db_info = sorted(db_info, key=lambda k: k[0])
                    #
                    # # Reorder the API received rows
                    # resp_status_code = resp["status_code"]
                    # resp = sorted(resp["Results"], key=lambda k: k['Style_Id'])
                    #
                    # # Choose a random row to test
                    # random_line = randint(0, len(db_info) - 1)

                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp["status_code"], 200)) is None \
                            and (self.assertEqual(resp["details"], 130) is None):
                        print "Version Pass"
                        print "resp: {0}".format(resp)
                        print "status_code: {0}".format(resp["status_code"])
                        print "details: {0}".format(resp["details"])

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

if __name__ == '__main__':
    print "-------------Test Result----------------\n"
    testResult = unittest.main(verbosity=1)
