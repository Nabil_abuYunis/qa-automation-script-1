
Purpose of the API scripts files
These scripts are created in order to test the API functionalities for the rendering applicaiton It is possible to run each script file alone, but in order to make the procces easier, we created a file "Run_All.py" in order to run all the tests together.

Run_All.py - run all the script files
Run_All.py - this file runs all the test cases according to three parameters: You may enter 3 parameters, if they are not entered then a default parameters are taken, these parameters could be also changed by editing this file. you could run this file both in "CMD" and "Pycharm" program.
1. test_type - in order to pick the positive, negative or all tests: � ������������� ����
	Positive: test_positive ��������������� ��� 
	Negative: test_negative ��������������� ��� 
	All tests: test � 
2. test_file - in order to choose which files to test: ���������� ���� ����
	All tests: Tests.py ��������������� ��� 
	Specific tests: for example - ge_Te* � 
3. test_dir - the folder that contains the test scripts

*****Python 3 Update*****

API scripts now use Python 3.

To run all API scripts, run command "python ypthon_file.py" to run test cases.
Currently, Run_All.py is not working.

To specify login information, target server, target database, or other destinations, go to api_config.ini file to set variables.

As per Smorphing API document, here are the relevant API endpoints:
	- Age_Test.py
	- Domain_Tests.py
	- Filename_Tests.py
	- Gender_Tests.py
	- Gesture_Tests.py
	- Gettext_Tests.py
	- Getvoices_Tests.py
	- Keyword_Tests.py
	- Lingo_Tests.py
	- Language_Tests.py
	- Login_Tests.py
	- Logout_Tests.py
	- Mood_Tests.py
	- Signup_Tests.py
	- SVP_Tests.py
	- Say_Tests.py
	- Templates_Tests.py
	- Style_Tests.py
	
Non-relevant/outdated API endpoints:
	- Feedback_Tests.py
	- SaveText_Tests.py
	- Version_Tests.py
	- textlists_Tests.py
	- Share_Tests.py

Notes:
  - Logger not implemented yet. Will be fixed eventually.