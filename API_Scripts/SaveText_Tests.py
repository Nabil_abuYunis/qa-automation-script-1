import unittest
import Renderer_Functions as Renderer_Functions
import string
from Database import Postgres
import random
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBSaveTextSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Get logged in email
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print("\n--------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_SaveText(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        text_ssml = "Hello  i am hung update2345567567"
        normalization_domain_id = 1
        # name = "First text"
        name = ''.join(random.choice(string.ascii_letters) for ii in range(6))

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        # template = ["yes", "no"]
        # template_original = random.choice(template)
        template_original = "no"

        user = {"text_ssml": text_ssml, "normalization_domain_id": normalization_domain_id, "name": name,
                "template_original": template_original, "template_id": 0, "smi_domain_id": smi_domain_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "savetext")

        # Get text id for the saved text from the DB
        query = "select * from renderer_text where renderer_text_id = " + str(resp["results"]["text_id"])
        cursor.execute(query)
        notes = [r for r in cursor]

        # here we will check if the voice detail is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and (self.assertEqual(resp["results"]["status"], "success")) is None \
                and (self.assertEqual(resp["results"]["text_id"], str(notes[0][0]))) is None \
                and (self.assertEqual(name, notes[0][1])) is None \
                and (self.assertEqual(text_ssml, notes[0][2])) is None \
                and (self.assertEqual(random_project, notes[0][4])) is None \
                and (self.assertEqual(smi_domain_id, notes[0][8])) is None:
            print("resp: {0}".format(resp))
            print("status_code: {0}".format(resp["status_code"]))
        else:
            print("Positive Test Failed")
            assert False

    print("SaveText Pass")

    # def test_negative_2_SaveText_incorrect_normalization_domain_id(self):
    #     # Login and get token
    #     resp = Renderer_Functions.initiate_login()
    #     email = resp['content']['results']['user']['email_address']
    #
    #     # Parameters
    #     text_ssml = "Hello  i am hung update2345567567"
    #     normalization_domain_id = "y"
    #     # name = "First text"
    #     name = ''.join(random.choice(string.ascii_letters) for ii in range(6))
    #
    #     cursor = Postgres.connect_to_db()
    #     cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
    #     db_info = [r for r in cursor]
    #     smi_domain_id = int(random.choice(db_info)[0])
    #     language_id = 6
    #     allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
    #     random_project = random.choice(allowed_projects)
    #     template = ["yes", "no"]
    #     template_original = random.choice(template)
    #     template_original = "no"
    #
    #     user = {"text_ssml": text_ssml, "normalization_domain_id": normalization_domain_id, "name": name,
    #             "template_id": 0, "template_original": template_original, "smi_domain_id": smi_domain_id, "project_id": random_project}
    #     resp = Renderer_Functions.send_request(resp["cookies"][0], user, "savetext")
    #
    #     # here we will check if the voice detail is passed or failed
    #     if resp["status_code"] == 200 \
    #         and (self.assertEqual(resp["results"]["error_message"], "Incorrect value for normalization_domain_id.") is None):
    #             print("-|-Result:Test case Pass")
    #             print("resp: {0}".format(resp))
    #             print("Status Code: {0}".format(resp["status_code"]))
    #     # Check error type
    #     else:
    #         print("Negative Test Failed")
    #         assert False

    def test_negative_2_SaveText_incorrect_text_ssml(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        text_ssml = 1
        normalization_domain_id = 1
        # name = "First text"
        name = ''.join(random.choice(string.ascii_letters) for ii in range(6))

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        # template = ["yes", "no"]
        # template_original = random.choice(template)
        template_original = "no"

        user = {"text_ssml": text_ssml, "normalization_domain_id": normalization_domain_id, "name": name,
                "template_id": 0, "template_original": template_original, "smi_domain_id": smi_domain_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "savetext")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["results"]["error_message"],
                                          "incorrect value for text_ssml.The parameter text_ssml cannot be empty.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_3_SaveText_missing_name(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        text_ssml = "Hello  i am hung update2345567567"
        normalization_domain_id = 1

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        # template = ["yes", "no"]
        # template_original = random.choice(template)
        template_original = "no"

        user = {"text_ssml": text_ssml, "normalization_domain_id": normalization_domain_id,
                "template_id": 0, "template_original": template_original, "smi_domain_id": smi_domain_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "savetext")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["results"]["error_message"], "missing parameter name.The domain parameter name cannot be empty.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    # def test_negative_4_SaveText_missing_normalization_domain_id(self):
    #     # Login and get token
    #     resp = Renderer_Functions.initiate_login()
    #     email = resp['content']['results']['user']['email_address']
    #
    #     # Parameters
    #     text_ssml = "Hello  i am hung update2345567567"
    #     # name = "First text"
    #     name = ''.join(random.choice(string.ascii_letters) for ii in range(6))
    #
    #     cursor = Postgres.connect_to_db()
    #     cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
    #     db_info = [r for r in cursor]
    #     smi_domain_id = int(random.choice(db_info)[0])
    #     language_id = 6
    #     allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
    #     random_project = random.choice(allowed_projects)
    #     template = ["yes", "no"]
    #     template_original = random.choice(template)
    #     template_original = "no"
    #
    #     user = {"text_ssml": text_ssml, "name": name,
    #             "template_id": 0, "template_original": template_original, "smi_domain_id": smi_domain_id, "project_id": random_project}
    #     resp = Renderer_Functions.send_request(resp["cookies"][0], user, "savetext")
    #
    #     # here we will check if the voice detail is passed or failed
    #     if resp["status_code"] == 200 \
    #         and (self.assertEqual(resp["results"]["error_message"], "Missing Parameter normalization_domain_id.") is None):
    #             print("-|-Result:Test case Pass")
    #             print("resp: {0}".format(resp))
    #             print("Status Code: {0}".format(resp["status_code"]))
    #     # Check error type
    #     else:
    #         print("Negative Test Failed")
    #         assert False

    def test_negative_4_SaveText_missing_text_ssml(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        normalization_domain_id = 1
        # name = "First text"
        name = ''.join(random.choice(string.ascii_letters) for ii in range(6))

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        # template = ["yes", "no"]
        # template_original = random.choice(template)
        template_original = "no"

        user = {"normalization_domain_id": normalization_domain_id, "name": name,
                "template_id": 0, "template_original": template_original, "smi_domain_id": smi_domain_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "savetext")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["results"]["error_message"],
                                          "missing parameter text_ssml.The parameter text_ssml cannot be empty.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_5_wrong_login(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        text_ssml = "Hello  i am hung update2345567567"
        normalization_domain_id = 1
        # name = "First text"
        name = ''.join(random.choice(string.ascii_letters) for ii in range(6))

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        # template = ["yes", "no"]
        # template_original = random.choice(template)
        template_original = "no"

        user = {"text_ssml": text_ssml, "normalization_domain_id": normalization_domain_id, "name": name,
                "template_id": 0, "template_original": template_original, "smi_domain_id": smi_domain_id, "project_id": random_project}
        resp = Renderer_Functions.send_request("123456789", user, "savetext")
        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_6_missing_login(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        text_ssml = "Hello  i am hung update2345567567"
        normalization_domain_id = 1
        # name = "First text"
        name = ''.join(random.choice(string.ascii_letters) for ii in range(6))

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        # template = ["yes", "no"]
        # template_original = random.choice(template)
        template_original = "no"

        user = {"text_ssml": text_ssml, "normalization_domain_id": normalization_domain_id, "name": name,
                "template_id": 0, "template_original": template_original, "smi_domain_id": smi_domain_id, "project_id": random_project}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "savetext")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_7_SaveText_incorrect_template_original(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        text_ssml = "Hello  i am hung update2345567567"
        normalization_domain_id = 1
        # name = "First text"
        name = ''.join(random.choice(string.ascii_letters) for ii in range(6))

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        # template = ["yes", "no"]
        # template_original = random.choice(template)
        template_original = 1

        user = {"text_ssml": text_ssml, "normalization_domain_id": normalization_domain_id, "name": name,
                "template_id": 0, "template_original": template_original, "smi_domain_id": smi_domain_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "savetext")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["results"]["error_message"], "incorrect value for template_original.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_8_SaveText_missing_template_original(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        text_ssml = "Hello  i am hung update2345567567"
        normalization_domain_id = 1
        # name = "First text"
        name = ''.join(random.choice(string.ascii_letters) for ii in range(6))

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        # template = ["yes", "no"]
        # template_original = random.choice(template)
        template_original = "no"

        user = {"text_ssml": text_ssml, "normalization_domain_id": normalization_domain_id, "name": name,
                "template_id": 0, "smi_domain_id": smi_domain_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "savetext")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["results"]["error_message"], "missing parameter template_original.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_9_SaveText_incorrect_template_id(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        text_ssml = "Hello  i am hung update2345567567"
        normalization_domain_id = 1
        # name = "First text"
        name = ''.join(random.choice(string.ascii_letters) for ii in range(6))

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        # template = ["yes", "no"]
        # template_original = random.choice(template)
        template_original = "no"

        user = {"text_ssml": text_ssml, "normalization_domain_id": normalization_domain_id, "name": name,
                "template_id": "abc", "template_original": template_original, "smi_domain_id": smi_domain_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "savetext")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["results"]["error_message"], "incorrect value for template_id.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_10_SaveText_missing_template_id(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        text_ssml = "Hello  i am hung update2345567567"
        normalization_domain_id = 1
        # name = "First text"
        name = ''.join(random.choice(string.ascii_letters) for ii in range(6))

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        # template = ["yes", "no"]
        # template_original = random.choice(template)
        template_original = "no"

        user = {"text_ssml": text_ssml, "normalization_domain_id": normalization_domain_id, "name": name,
                "template_original": template_original, "smi_domain_id": smi_domain_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "savetext")

        # here we will check if the voice detail is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["results"]["error_message"], "missing parameter template_id.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_22_SaveText_existName(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        text_ssml = "Hello  i am hung update2345567567"
        normalization_domain_id = 1
        # name = "First text"
        name = ''.join(random.choice(string.ascii_letters) for ii in range(6))

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        # template = ["yes", "no"]
        # template_original = random.choice(template)
        # Get text id for the saved text from the DB

        template_original = "true"
        open_to_public = False
        query_templates = "SELECT template_id,smi_domain_id FROM template WHERE  template_id IN (SELECT DISTINCT template_id FROM renderer_text where is_template_org = True)"
        cursor.execute(query_templates)
        db_info = [r for r in cursor]
        index = random.randint(0, db_info.__len__())
        template_id = db_info[index][0]

        query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE is_template_org AND template_id=" + str(
            template_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        template_name = db_info[0][0]
        text_ssml = db_info[0][1]


        user = {"text_ssml": text_ssml, "name": name,"template_id": template_id, "open_to_public": open_to_public
                ,"text_id": 0, "template_original": template_original, "smi_domain_id": smi_domain_id, "project_id": random_project}

        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "savetext")

        resp2 = Renderer_Functions.initiate_login()
        resp2 = Renderer_Functions.send_request(resp2["cookies"][0], user, "savetext")

        if resp2["status_code"] == 200 \
            and (self.assertEqual(resp2["results"]["error_message"],
                                          "the name already exists in the project and domain. ") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp2))
                print("Status Code: {0}".format(resp2["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False
        #the name already exist in the project and domain.



    def test_negative_23_SaveText_Public(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        text_ssml = "Hello  i am hung update2345567567"
        normalization_domain_id = 1
        # name = "First text"
        name = ''.join(random.choice(string.ascii_letters) for ii in range(6))

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        # template = ["yes", "no"]
        # template_original = random.choice(template)
        # Get text id for the saved text from the DB

        template_original = "true"
        open_to_public = True
        query_templates = "SELECT template_id,smi_domain_id FROM template WHERE  template_id IN (SELECT DISTINCT template_id FROM renderer_text where is_template_org = True)"
        cursor.execute(query_templates)
        db_info = [r for r in cursor]
        index = random.randint(0, db_info.__len__())
        template_id = db_info[index][0]

        query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE is_template_org AND template_id=" + str(
            template_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        template_name = db_info[0][0]
        text_ssml = db_info[0][1]

        #only changed the open_to_public field

        user = {"text_ssml": text_ssml, "name": name,"template_id": template_id, "open_to_public": open_to_public
                ,"text_id": 0, "template_original": template_original, "smi_domain_id": smi_domain_id, "project_id": random_project}

        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "savetext")
        resp2 = Renderer_Functions.initiate_login()
        resp2 = Renderer_Functions.send_request(resp2["cookies"][0], user, "savetext")

        # here we will check if the voice detail is passed or failed
        if resp2["status_code"] == 200 \
            and (self.assertEqual(resp2["results"]["error_message"],
                                          "please choose another name to make it public. this name is already made open to public") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp2))
                print("Status Code: {0}".format(resp2["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False


    def test_negative_24_SaveText_permission(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()

        email = resp['content']['results']['user']['email_address']

        # Parameters
        text_ssml = "Hello  i am hung update2345567567"
        normalization_domain_id = 1
        # name = "First text"
        name = ''.join(random.choice(string.ascii_letters) for ii in range(6))

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        # template = ["yes", "no"]
        # template_original = random.choice(template)
        # Get text id for the saved text from the DB

        template_original = "NULL"
        open_to_public = False

        query_templates = "SELECT template_id,smi_domain_id FROM template WHERE  template_id IN (SELECT DISTINCT template_id FROM renderer_text where is_template_org = True)"
        cursor.execute(query_templates)
        db_info = [r for r in cursor]
        index = random.randint(0, db_info.__len__())
        template_id = db_info[index][0]

        query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE is_template_org AND template_id=" + str(
            template_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        template_name = db_info[0][0]
        text_ssml = db_info[0][1]

        #only changed the open_to_public field

        user = {"text_ssml": text_ssml, "name": name,"template_id": template_id, "open_to_public": open_to_public
                ,"text_id": 0, "template_original": template_original, "smi_domain_id": smi_domain_id, "project_id": random_project}

        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "savetext")
        #change the user for another permission
        resp2 = Renderer_Functions.initiate_login()
        resp2 = Renderer_Functions.send_request(resp2["cookies"][0], user, "savetext")

        # here we will check if the voice detail is passed or failed
        if resp2["status_code"] == 200 \
            and (self.assertEqual(resp2["results"]["error_message"],
                                          "you do not have permission to update. please change the name and then save.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp2))
                print("Status Code: {0}".format(resp2["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_25_SaveText_original_template(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()

        email = resp['content']['results']['user']['email_address']

        # Parameters
        text_ssml = "Hello  i am hung update2345567567"
        normalization_domain_id = 1
        # name = "First text"
        name = ''.join(random.choice(string.ascii_letters) for ii in range(6))

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        # template = ["yes", "no"]
        # template_original = random.choice(template)
        # Get text id for the saved text from the DB

        template_original = "NULL"
        open_to_public = False

        query_templates = "SELECT template_id,smi_domain_id FROM template WHERE  template_id IN (SELECT DISTINCT template_id FROM renderer_text where is_template_org = True)"
        cursor.execute(query_templates)
        db_info = [r for r in cursor]
        index = random.randint(0, db_info.__len__())
        template_id = db_info[index][0]

        query_renderertext = "SELECT name,text_ssml,open_to_public FROM renderer_text WHERE is_template_org AND template_id=" + str(
            template_id)
        cursor.execute(query_renderertext)
        db_info = [r for r in cursor]
        template_name = db_info[0][0]
        text_ssml = db_info[0][1]

        #only changed the open_to_public field

        user = {"text_ssml": text_ssml, "name": template_name,"template_id": 0, "open_to_public": open_to_public
                ,"text_id": 0, "template_original": template_original, "smi_domain_id": smi_domain_id, "project_id": random_project}

        #resp = Renderer_Functions.send_request(resp["cookies"][0], user, "savetext")
        #change the user for another permission
        resp2 = Renderer_Functions.initiate_login()
        resp2 = Renderer_Functions.send_request(resp2["cookies"][0], user, "savetext")

        # here we will check if the voice detail is passed or failed
        if resp2["status_code"] == 200 \
            and (self.assertEqual(resp2["results"]["error_message"],
                                          "you cannot use original template name. please choose another name") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp2))
                print("Status Code: {0}".format(resp2["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
