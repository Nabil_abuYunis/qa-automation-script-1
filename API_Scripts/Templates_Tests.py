import unittest
import Renderer_Functions
from random import randint
from Database import Postgres
import JSON_Request as Data_Utils
import random
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBTemplateSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Get logged in email
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_say_temp(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        # get template_id
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # user = {"template": "yes", "project_id": random_project}
        # resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        # resp_extract = resp2["results"][0]["list"]
        # count = resp2["results"][0]["count"]
        # # Choose a random row to test
        # random_line = randint(0, count - 1)
        # template_id = resp_extract[random_line]["template_id"]

        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
        #             'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
        #             'is open on a holiday?</speak>'
        text_ssml = 'testing new again'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        # query = "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true"
        # query = "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true"
        # cursor.execute(query)
        # db_info = [r for r in cursor]
        # smi_domain_id = int(random.choice(db_info)[0])
        # allowed_domains = Renderer_Functions.get_allowed_domains_in_allowed_projects_in_allowed_accounts(email, language_id)
        # smi_domain_id = random.choice(allowed_domains)
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        #file_format = "wav"

        choice = randint(0, 1)

        if choice == 0:
            template_original = "no"
        else:
            template_original = "yes"

        query = "select * from renderer_text where is_template_org = true and text_ssml ilike '%say-as interpret-as=\"sub-field%'"
        cursor.execute(query)
        templates_with_fields = [r for r in cursor]
        template_ids = []
        for i in templates_with_fields:
            if i[2].count("sub-field") >= 1:
                template_ids.append(i[0])
        template_id = random.choice(template_ids)

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original" : template_original, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and self.assertGreater(resp["results"]["filenumber"], 0) is None \
                and self.assertIn(".wav", resp["results"]["filename"]) is None \
                and self.assertIn(self.config['SERVER']['URL'], resp["results"]["filename"]) is None:
            # if wav.check_wav_files(resp,"",True):
                print ("Templates Pass")
                print ("resp: {0}".format(resp))
                print ("status_code: {0}".format(resp["status_code"]))
                print ("Filename: {0}".format(resp['results']['filename']))
        else:
            print("Positive Test Failed")
            assert False

    print("Templates Pass")

    def test_positive_2_say_temp_optional_file_format(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # get template_id
        user = {"template": "yes", "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        # template_id = resp_extract[random_line]["template_id"]

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="6" xml:domain="1099511627778" xml:accent="default"' \
                    ' xml:rate="100" xml:volume="100" xml:pitch="100" xml:style="1099511627778" xml:mood="10">Do you want the transaction' \
                    ' history for your<say-as interpret-as="sub-field" detail="9">' \
                    '  checking</say-as>  account or your<say-as interpret-as="sub-field" detail="14">  savings</say-as>  account ?</speak>'

        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
        #            'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
        #             'is open on a holiday?</speak>'
        # text_ssml = 'testing a new template input by API'

        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])

        # Get template_ids which doesn't contain sub-fields
        query2 = "select distinct template_id from template_words where template_field_type_id=0"
        cursor.execute(query2)
        db_info = [r for r in cursor]
        template_id = random.choice(db_info)[0]
        template_id = "4398046511759"

        request_id = "654334"
        # request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        # query = "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true"
        # query = "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true"
        # cursor.execute(query)
        # db_info = [r for r in cursor]
        # smi_domain_id = int(random.choice(db_info)[0])
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        all_file_formats = ["wav", "pcma", "pcmu"]
        file_format = random.choice(all_file_formats)

        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id": template_id, "template_original": template_original,
                "format": file_format, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and self.assertGreater(resp["results"]["filenumber"], 0) is None \
                and self.assertIn(self.config['SERVER']['URL'], resp["results"]["filename"]) is None:
               # and self.assertIn("." + file_format, resp["results"]["filename"]) is None:
            # if wav.check_wav_files(resp,"",True):
            print("Templates Pass")
            print("resp: {0}".format(resp))
            print("status_code: {0}".format(resp["status_code"]))
            print("Filename: {0}".format(resp['results']['filename']))
        else:
            print("Positive Test Failed")
            assert False

        print("Templates Pass")

    def test_positive_3_say_temp_optional_file_type(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # get template_id
        user = {"template": "yes", "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]

        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" ' + \
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that ' + \
                    'is open on a holiday?</speak>'
        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
        #             'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
        #             'is open on a holiday?</speak>'
        # text_ssml = 'testing a new template input by API'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])

        # Get template_ids which doesn't contain sub-fields
        query2 = "select distinct renderer_text_id,text_ssml from renderer_text where text_ssml NOT ilike '%sub-field%'"
        # query2 = "select distinct template_id from template_words where template_field_type_id=0"
        cursor.execute(query2)
        db_info = [r for r in cursor]
        index = randint(0,db_info.__len__())
        template_id = db_info[index][0]
        text_ssml = db_info[index][1]

        request_id = "654334"
        # request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        # query = "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true"
        # query = "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true"
        # cursor.execute(query)
        # db_info = [r for r in cursor]
        # smi_domain_id = int(random.choice(db_info)[0])
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        all_url_types = ["http", "smb"]
        #  url_type is NO LONGER USED
        url_type = random.choice(all_url_types)
        url_type= "http"
        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id": template_id, "template_original": template_original,
                "url_type": url_type, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and self.assertGreater(resp["results"]["filenumber"], 0) is None \
                and self.assertIn(url_type, resp["results"]["filename"]) is None \
                and self.assertIn(self.config['SERVER']['URL'], resp["results"]["filename"]) is None:
            # if wav.check_wav_files(resp,"",True):
            print("Templates Pass")
            print("resp: {0}".format(resp))
            print("status_code: {0}".format(resp["status_code"]))
            print("Filename: {0}".format(resp['results']['filename']))
        else:
            print("Positive Test Failed")
            assert False

        print("Templates Pass")

    def test_negative_4_incorrect_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"template": "no", "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "658945"
        #request_id = "123456"
        smi_domain_id = "precise"
        #file_format = "wav"

        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "incorrect value for smi_domain_id.") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_5_incorrect_voice(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"template": "no", "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = "precise"
        request_id = "658945"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        #file_format = "wav"

        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "incorrect value for smi_voice_id.") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_6_missing_domain_param(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"template": "no", "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "123456"
        #file_format = "wav"

        template_original = "no"

        user = {"smi_voice_id": smi_voice_id,"text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original, "request_id" : request_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "missing parameter smi_domain_id") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_7_missing_req_id_param(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"template": "no", "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        #request_id = "123456"
        #file_format = "wav"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        template_original = "no"

        user = {"smi_voice_id": smi_voice_id,"text_ssml": text_ssml, "smi_domain_id":smi_domain_id, "template_id" : template_id, "template_original":template_original, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "missing parameter request_id") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_8_missing_voice_param(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"template": "no", "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        #smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "123456"
        #file_format = "wav"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        template_original = "no"

        user = {"request_id": request_id, "text_ssml": text_ssml, "smi_domain_id":smi_domain_id, "template_id" : template_id, "template_original":template_original, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "missing parameter smi_voice_id") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_9_missing_template_param(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # user = {"template": "no"}
        # resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
        # resp_extract = resp2["results"][0]["list"]
        # count = resp2["results"][0]["count"]
        # # Choose a random row to test
        # random_line = randint(0, count - 1)
        # template_id = resp_extract[random_line]["template_id"]
        # print(template_id)

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "123456"
        #file_format = "wav"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        template_original = "no"

        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"request_id": request_id, "text_ssml": text_ssml, "smi_domain_id":smi_domain_id, "smi_voice_id" : smi_voice_id, "template_original":template_original, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "missing parameter template_id") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_10_missing_template_orig_param(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"template": "no", "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "123456"
        #file_format = "wav"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        # template_original = "no"
        user = {"send_unsupported": True, "project_id": random_project}

        user = {"request_id": request_id, "text_ssml": text_ssml, "smi_domain_id":smi_domain_id, "smi_voice_id" : smi_voice_id, "template_id":template_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "missing parameter template_original") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_11_incorrect_template_original(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"template": "no", "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "658945"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        #file_format = "wav"

        template_original = 123456789

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "incorrect value for template_original.") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_12_incorrect_ssml(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"template": "no", "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)

        # Set template_id value to non-zero
        template_id = str(randint(1,10))

        # Parameters
        text_ssml = 123456789
        smi_voice_id = "72057594037928293"
        request_id = "123456"
        #file_format = "wav"
        smi_domain_id = "72057594037927955"
        template_original = "no"

        user = {"smi_voice_id": smi_voice_id,"text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original, "request_id" : request_id, "smi_domain_id": smi_domain_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "incorrect value for text_ssml.") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_13_incorrect_file_format(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"template": "no", "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]

        # Parameters
        text_ssml = "To build an empire"
        # smi_voice_id = "72057594037928293"
        cursor = Postgres.connect_to_db()
        # query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "123456"
        # file_format = 123456
        format = 123456
        # smi_domain_id = "72057594037927955"
        # query = "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        template_original = "no"

        user = {"smi_voice_id": smi_voice_id,"text_ssml": text_ssml, "template_id" : template_id, "file_format": format,
                "template_original":template_original, "request_id" : request_id, "smi_domain_id": smi_domain_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "incorrect value for format.") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_14_missing_ssml(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"template": "no", "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)

        # Set template_id value to non-zero
        template_id = str(randint(1, 10))
        # Parameters
        smi_voice_id = "72057594037928293"
        request_id = "123456"
        #file_format = "wav"
        smi_domain_id = "72057594037927955"
        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "template_id" : template_id, "template_original":template_original, "request_id" : request_id, "smi_domain_id": smi_domain_id, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "missing parameter text_ssml.") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    # if this test failed later, then comment this test
    # Mia said, text_ssml can be empty for saytemplate/file
    def test_negative_15_blank_text(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"template": "no", "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]

        # Parameters
        text_ssml = ''
        # smi_voice_id = '72057594037928282'
        # request_id = "654334"
        # #request_id = "123456"
        # smi_domain_id = '72057594037927948'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        # request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        # query = "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true"
        # query = "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true"
        # cursor.execute(query)
        # db_info = [r for r in cursor]
        # smi_domain_id = int(random.choice(db_info)[0])
        allowed_domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        smi_domain_id = random.choice(allowed_domains)
        # file_format = "wav"

        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id[0],
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["results"]["error_message"], " text_ssml cannot be empty.") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_16_template_missing_login(self):

        email = self.config["LOGIN"]["email"]
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "654334"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        template_id = "568798633"
        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original, "project_id": random_project}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "templates")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False
            
    def test_negative_17_template_wrong_login(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "654334"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        template_id = "568798633"
        template_original = "no"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original, "project_id": random_project}
        resp = Renderer_Functions.send_request("123456789", user, "templates")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_18_expired_token(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "654334"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        template_id = "568798633"
        template_original = "no"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original, "project_id": random_project}

        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "templates")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_19_no_permission_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"template": "no", "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927954" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        text_ssml= '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="6" xml:domain="1099511627778" ' +\
                   'xml:accent="default" xml:rate="100" xml:volume="100" xml:pitch="100" xml:style="1099511627778" xml:mood="10"' +\
                   '>Do you want the transaction history for your<say-as interpret-as="sub-field" detail="9">  checking</say-as>'+\
                   '  account or your<say-as interpret-as="sub-field" detail="14">  savings</say-as>  account ?</speak>'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "785435"
        cursor = Postgres.connect_to_db()
        # qurey = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        #request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")

        # Get domain_ids that user doesn't have permission to access
        query = "SELECT smi_domain_id FROM smi_domain WHERE smi_domain_owner_project_id <> '"+random_project+"' " \
                "AND smi_domain_id NOT IN (SELECT smi_domain_id FROM smi_domain_permissions WHERE project_id ILIKE '"+random_project+"')"

        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        #file_format = "wav"

        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], " you do not have permission to use this domain "+ str(smi_domain_id)+". ") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    def test_negative_20_no_permission_voice(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"template": "no", "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037927968" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        # get voice that does not have permission
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = false AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = false"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "785435"
        #request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        # query = "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        #file_format = "wav"

        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"]," you do not have permission to use this domain " + str(smi_domain_id) + ".  you do not have permission to use this voice " + str(smi_voice_id) + ". ") is None):
                print( "-|-Result:Test case Pass")
                print( "resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print( "Negative Test Failed")
            assert False

    # The language is determined by the voice, and only by the voice
    # def test_negative_21_no_permission_lang(self):
    #
    #     # Login and get token
    #     resp = Renderer_Functions.initiate_login()
    #     email = resp['content']['results']['user']['email_address']
    #
    #     language_id = 6
    #     allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
    #     random_project = random.choice(allowed_projects)
    #     user = {"template": "no", "project_id": random_project}
    #     resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
    #     resp_extract = resp2["results"][0]["list"]
    #     count = resp2["results"][0]["count"]
    #     # Choose a random row to test
    #     random_line = randint(0, count - 1)
    #     template_id = resp_extract[random_line]["template_id"]
    #
    #     # Parameters
    #     language = 200
    #     text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="' + str(language) + '" xml:age="3" xml:voice="1099511628199" xml:mood="4" xml:style="72057594037927938" '+\
    #                 'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="1099511627865" version="1.0">Where can we go to eat that '+\
    #                 'is open on a holiday?</speak>'
    #     # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
    #     cursor = Postgres.connect_to_db()
    #     cursor.execute("select Distinct smi_voice_id from smi_voice where supported = true ")
    #     db_info = [r for r in cursor]
    #     smi_voice_id = int(random.choice(db_info)[0])
    #     request_id = "785435"
    #     #request_id = "123456"
    #     # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
    #     cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
    #     db_info = [r for r in cursor]
    #     smi_domain_id = int(random.choice(db_info)[0])
    #     #file_format = "wav"
    #
    #     template_original = "no"
    #
    #     user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
    #             "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original, "project_id": random_project}
    #     resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")
    #
    #     if resp["status_code"] != 200 \
    #         and (self.assertEqual(resp["status_code"], '401')) is None \
    #         and (self.assertEqual(resp["results"]["error_message"], " you do not have permission to use this language. ") is None):
    #             print( "-|-Result:Test case Pass")
    #             print( "resp: {0}".format(resp))
    #             print( "Status Code: {0}".format(resp["status_code"]))
    #             print( "Reason: {0}".format(resp["results"]["error_message"]))
    #
    #     # Check error type
    #     else:
    #         print( "Negative Test Failed")
    #         assert False

    def test_negative_22_say_temp_mismatch(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        # get template_id
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"template": "yes", "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        #template_id = resp_extract[random_line]["template_id"]

        plain_text_ssml = 'testing new again'
        # text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(
        #     1) + '"' \
        #                  ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="6" xml:domain="' + str(
            1) + '"' \
            ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" xml:style="1099511627778" \
            xml:mood="10" >' + plain_text_ssml + '<say-as interpret-as="sub-field" detail="9">  checking</say-as>\
            account or your<say-as interpret-as="sub-field" detail="14">  savings</say-as>  account ?</speak> '

        text_ssml= '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="6" xml:domain="1099511627778" ' +\
                   'xml:accent="default" xml:rate="100" xml:volume="100" xml:pitch="100" xml:style="1099511627778" xml:mood="10"' +\
                   '>testing new again<say-as interpret-as="sub-field" detail="9">  checking</say-as>'+\
                   '  account or your<say-as interpret-as="sub-field" detail="14">  savings</say-as>  account ?</speak>'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()

        # Get template_ids with sub-fields, and send an text_ssml request without sub fields
        query1 = "select distinct template_id from template_words where template_field_type_id!=0"
        cursor.execute(query1)
        template_ids = [r for r in cursor]
        template_id = random.choice(template_ids[0])


        # query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        # query = "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true"
        # query = "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true"
        # cursor.execute(query)
        # db_info = [r for r in cursor]
        # smi_domain_id = int(random.choice(db_info)[0])
        # allowed_domains = Renderer_Functions.get_allowed_domains_in_allowed_projects_in_allowed_accounts(email, language_id)
        # smi_domain_id = random.choice(allowed_domains)
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        # file_format = "wav"

        choice = randint(0, 1)

        if choice == 0:
            template_original = "no"
        else:
            template_original = "yes"

        # query = "select * from renderer_text where is_template_org = false"
        # cursor.execute(query)
        # templates_with_fields = [r for r in cursor]
        # template_ids = []
        # for i in templates_with_fields:
        #     if i[2].count("sub-field") == 0:
        #         template_ids.append(i[0])
        # template_id = random.choice(template_ids)

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id": template_id, "template_original": template_original,
                "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], " Template Fields Mismatch") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print( "Status Code: {0}".format(resp["status_code"]))
                print( "Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    print("Templates Pass")

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)