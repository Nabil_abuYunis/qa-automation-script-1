import unittest
import Renderer_Functions
from Database import Postgres
import random
import string
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBtemplatesaddSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Get logged in email
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)


    def test_positive_1_templates_delete(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login_admin()
        cookies = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']

        # -------------------------------------------------
        # First add a template in order to remove it later
        # Parameters
        plain_text_ssml = "testing a new input for template " + ''.join(random.choice(string.ascii_letters) for ii in range(6))
        plain_text_ssml ="sample text."

        cursor = Postgres.connect_to_db()
        cursor.execute("select Distinct smi_domain_id from smi_domain where supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        domain_id = int(random.choice(db_info)[0])

        template_name = "testing template name " + ''.join(random.choice(string.ascii_letters) for ii in range(6))

        publicValues = [True, False]
        is_public = random.choice(publicValues)

        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        project_id = random.choice(allowed_projects)

        text_ssml = '<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="en_US" xml:domain="' + str(domain_id) + '"' \
                         ' xml:accent="default" xml:rate="100" xml:pitch="100" xml:volume="100" xml:say-as="default" >' + plain_text_ssml + '</speak> '

        words = [{"word_number": 1, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "sample", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "s", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "a", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "m", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "p", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 5, "phoneme": "l", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 6, "phoneme": "e", "stress_type": 0, "pitch_type": 0}
                ]
            },
            {
                "word_number": 2, "sub_field_id": 0, "field_type_id": 0, "pos_code": "0", "boundary_tone_id": 0,
                "phrase_break_id": 0, "focus": 0, "compressed_f0": True, "text": "text", "sensitivity_next_word": False,
                "sensitivity_gender": False, "mood_id": 0,"prosody_speed": "100", "prosody_volume": "100","prosody_pitch": "100", "phonemes": [
                    {"phoneme_number": 1, "phoneme": "t", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 2, "phoneme": "e", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 3, "phoneme": "x", "stress_type": 0, "pitch_type": 0},
                    {"phoneme_number": 4, "phoneme": "t", "stress_type": 0, "pitch_type": 0}
                ]
            }]

        user = {"text_ssml": text_ssml, "project_id": project_id, "domain_id": domain_id, "template_name": template_name,
                "is_public": is_public, "language_id": language_id, "words": words}
        resp = Renderer_Functions.send_request(cookies, user, "templatesadd")

        # -------------------------------------------------
        # now remove the added template

        # Fetch the info from the Database
        cursor = Postgres.connect_to_db()
        query = "select * from renderer_text where name ilike '" + template_name + "'"
        cursor.execute(query)
        db_info = [r for r in cursor]

        template_id = db_info[0][6]
        user = {"template_id": template_id}
        resp = Renderer_Functions.send_request(cookies, user, "templatesdelete")
        resp_status_code = resp["status_code"]

        # Check again that the new added template is deleted
        query = "select is_deleted from renderer_text where name ilike '" + template_name + "'"
        cursor.execute(query)
        db_info = [r for r in cursor]

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
            and (self.assertEqual(resp["results"]["success_message"], "template is deleted") is None) \
            and (self.assertEqual(db_info[0][0], True) is None) :
            print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False
        print("Templates Delete Pass")

    def test_negative_2_templates_delete_wrong_login(self):
        # Parameters
        email = self.config["LOGINADMIN"]["email"]

        # Get available template ids
        cursor = Postgres.connect_to_db()
        query = "SELECT template_id FROM template"
        cursor.execute(query)
        template_ids = [r for r in cursor]
        template_id = random.choice(template_ids)

        # Send request
        user = {"template_id": template_id[0]}
        resp = Renderer_Functions.send_request("123456789", user, "templatesdelete")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_3_templates_delete_missing_login(self):
        # Parameters
        email = self.config["LOGINADMIN"]["email"]

        # Get available template ids
        cursor = Postgres.connect_to_db()
        query = "SELECT template_id FROM template"
        cursor.execute(query)
        template_ids = [r for r in cursor]
        template_id = random.choice(template_ids)

        # Send request
        user = {"template_id": template_id[0]}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "templatesdelete")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_4_templates_add_expired_token(self):
        # Parameters
        email = self.config["LOGINADMIN"]["email"]

        # Get available template ids
        cursor = Postgres.connect_to_db()
        query = "SELECT template_id FROM template"
        cursor.execute(query)
        template_ids = [r for r in cursor]
        template_id = random.choice(template_ids)

        # Send request
        user = {"template_id": template_id[0]}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "templatesdelete")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))
        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_5_templates_delete_wrong_template_id(self):

        resp = Renderer_Functions.initiate_login_admin()

        # Parameters
        template_id = ''.join(random.choice(string.ascii_letters) for ii in range(6))
        # Send request
        user = {"template_id": template_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesdelete")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and (self.assertEqual(resp["error_message"], "incorrect value for template_id.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_6_templates_delete_missing_template_id(self):

        resp = Renderer_Functions.initiate_login_admin()
        # Parameters
        user = {}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesdelete")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and (self.assertEqual(resp["error_message"], "missing parameter template_id.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
