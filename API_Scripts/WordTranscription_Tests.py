import unittest
import Renderer_Functions
from Database import Postgres
import random

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBWordTranscriptionSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_word_transcription(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        cursor = Postgres.connect_to_db()
        # Get random smi_domain_id
        cursor.execute(
            "select distinct smi_domain_id from smi_domain_word_transcriptions")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        user = {"smi_domain_id": smi_domain_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "wordtranscription")

        # Get DB transcriptions
        query = "WITH RECURSIVE parents (c_id, p_id, level) as ( SELECT smi_domain_id, parent_id, " \
                "0 FROM smi_domain  WHERE smi_domain_id = " + str(smi_domain_id) + " and marked_for_deletion " \
                "is NULl  UNION ALL SELECT smi_domain_id, parent_id, level + 1 FROM smi_domain " \
                "INNER JOIN parents ON (smi_domain_id = p_id)) SELECT smi_domain_id, domain_name," \
                " word, transcription FROM  smi_domain INNER JOIN (SELECT smi_domain_id, word, " \
                "transcription, level,  rank() OVER (PARTITION BY word ORDER BY level)  FROM " \
                "smi_domain_word_transcriptions INNER JOIN parents ON (smi_domain_id = c_id)  ) " \
                "innerQuery USING (smi_domain_id)  WHERE rank = 1 ORDER BY 3;"
        cursor.execute(query)
        db_info = [r for r in cursor]

        count = resp['count']

        # If they are not the same amount then fail
        if resp["results"].__len__() != db_info.__len__():
            assert False

        for x in range(count):
            if (self.assertEqual(resp["status_code"], 200)) is None \
                and (self.assertEqual(resp['results'][x]["transcription"], db_info[x][3]) is None) \
                and (self.assertEqual(resp['results'][x]["smi_domain_id"], db_info[x][0]) is None) \
                and (self.assertEqual(resp['results'][x]["domain_name"], db_info[x][1]) is None) \
                and (self.assertEqual(resp['results'][x]["word"], db_info[x][2]) is None):
                print(resp['results'][x]["transcription"], db_info[x][3])
                print(resp['results'][x]["smi_domain_id"], db_info[x][0])
                print(resp['results'][x]["domain_name"], db_info[x][1])
                print(resp['results'][x]["word"], db_info[x][2])
            else:
                print("Positive Test Failed")
                assert False

        print("Word Transcription Pass")

    def test_negative_2_incorrect_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        user = {"smi_domain_id": "mistletoe"}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "wordtranscription")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "incorrect value for smi_domain_id.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_3_missing_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        user = {}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "wordtranscription")

        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "missing parameter smi_domain_id.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_6_word_replacement_wrong_login(self):
        # parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.send_request("123456789", user, "wordtranscription")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_7_word_replacement_missing_login(self):
        # parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "wordtranscription")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_8_expired_token(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "wordtranscription")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)