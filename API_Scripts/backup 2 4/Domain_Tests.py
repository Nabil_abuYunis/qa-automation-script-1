import urllib.error as urllibe
import unittest
import Renderer_Functions
from random import randint
import configparser
from Database import Postgres

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBDomainSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print( "-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Completed running test case: {0}".format(str(self.id())))
        print( "-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print( self.currentResult)

    def test_positive_1_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        # Get Random Language id
        cursor = Postgres.connect_to_db()
        # select only the language_id's that have domains
        cursor.execute('SELECT DISTINCT language_id FROM smi_domain')
        db_info = [r for r in cursor]
        random_line = randint(0, cursor.rowcount-1)

        language_id = db_info[random_line][0]

        # Send to Domain
        user = {"language_id": language_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")

        # Get logged in email
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        email = config["LOGIN"]["email"]

        # Fetch the info from the domain table
        cursor = Postgres.connect_to_db()
        cursor.execute("WITH webSupported as (SELECT DISTINCT smi_domain_id FROM canned_clip_website WHERE supported)SELECT smi_domain_id,domain_name as name, language_id, parent_id as parent_smi_domain_id, normalization_domain_id, style_id, mood_id, speed, pitch, volume, open_to_public, supported, smi_domain_owner_email, string_agg(keyword, ', ') as keywords, string_agg(user_email, ', ') as email, websupported.smi_domain_id is not NULL web_supported   FROM smi_domain LEFT OUTER JOIN smi_domain_permissions USING (smi_domain_id) LEFT OUTER JOIN smi_domain_keywords USING (smi_domain_id) LEFT OUTER JOIN websupported using (smi_domain_id) WHERE (open_to_public OR smi_domain_owner_email = '" + email + "'  OR user_email ILIKE '" + email + "') AND CASE WHEN (length('') = 0  OR trim(domain_name) ~* '()' OR smi_domain_id IN (SELECT smi_domain_id FROM smi_domain_keywords  WHERE  keyword ~* '^()$')) THEN TRUE ELSE FALSE END GROUP BY smi_domain_id, domain_name, language_id, parent_id, normalization_domain_id, style_id, mood_id, speed, pitch, volume, open_to_public, smi_domain_owner_email, websupported.smi_domain_id   ORDER BY (websupported.smi_domain_id is NOT NULL) DESC, domain_name")

        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])
        db_info = [x for x in db_info if x[2] == language_id]

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["smi_domain_id"])

        # here we will check if the request is passed or failed
        count = resp.__len__()
        for x in range(count):
            print(x)
            print (resp[x]["smi_domain_id"], str(db_info[x][0]))
            print(resp[x]["name"], str(db_info[x][1]))
            print(resp[x]["mood_id"], str(db_info[x][9]))
            print(resp[x]["open_to_public"], str(db_info[x][10]))
            print(resp[x]["pitch"], str(db_info[x][5]))
            print(resp[x]["smi_domain_owner_email"], str(db_info[x][12]))
            print(resp[x]["supported"], str(db_info[x][11]))
            print(resp[x]["volume"], str(db_info[x][9]))
            print(resp[x]["style_id"], str(db_info[x][5]))
            print(resp[x]["speed"], str(db_info[x][7]))
            print(resp[x]["normalization_domain_id"], str(db_info[x][4]))
            print(resp[x]["language_id"], str(db_info[x][2]))
            if (self.assertEqual(resp_status_code, 200) is None
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["smi_domain_id"],
                                                                     str(db_info[x][0])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["name"],
                                                                     str(db_info[x][1])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["mood_id"],
                                                                     str(db_info[x][9])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["open_to_public"],
                                                                     str(db_info[x][10])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["pitch"],
                                                                     str(db_info[x][5])))
                    and (
                    Renderer_Functions.compare_two_values_equal(resp[x]["smi_domain_owner_email"],
                                                                str(db_info[x][12])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["supported"],
                                                                     str(db_info[x][11])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["volume"],
                                                                     str(db_info[x][9])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["style_id"],
                                                                     str(db_info[x][5])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["speed"],
                                                                     str(db_info[x][7])))
                    and (
                    Renderer_Functions.compare_two_values_equal(resp[x]["normalization_domain_id"],
                                                                    str(db_info[x][4])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["language_id"],
                                                                str(db_info[x][2])))):
                print ("Domain Pass")
                print ("resp: {0}".format(resp))

    def test_positive_2_open_to_public(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        send_unsupported = False
        open_to_public = False
        # Send to Domain
        user = {"language_id": language_id , "send_unsupported": send_unsupported, "open_to_public": open_to_public}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")

        # Fetch the info from the Age table
        cursor = Postgres.connect_to_db()
        cursor.execute("SELECT smi_domain_id, domain_name, parent_id, style_id, speed, pitch, volume, normalization_domain_id, open_to_public, mood_id, supported FROM smi_domain WHERE language_id=" + str(language_id) + " AND supported=" + str(send_unsupported) + " AND open_to_public=" + str(open_to_public) + ";")

        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # -------------------
        # # Get logged in email
        # config = configparser.ConfigParser()
        # config.sections()
        # config.read('api_config.ini')
        # email = config["LOGIN"]["email"]
        #
        # cursor.execute(
        #     "WITH webSupported as (SELECT DISTINCT smi_domain_id FROM canned_clip_website WHERE supported)SELECT smi_domain_id,domain_name as name, language_id, parent_id as parent_smi_domain_id, normalization_domain_id, style_id, mood_id, speed, pitch, volume, open_to_public, supported, smi_domain_owner_email, string_agg(keyword, ', ') as keywords, string_agg(user_email, ', ') as email, websupported.smi_domain_id is not NULL web_supported   FROM smi_domain LEFT OUTER JOIN smi_domain_permissions USING (smi_domain_id) LEFT OUTER JOIN smi_domain_keywords USING (smi_domain_id) LEFT OUTER JOIN websupported using (smi_domain_id) WHERE (open_to_public OR smi_domain_owner_email = '" + email + "'  OR user_email ILIKE '" + email + "') AND CASE WHEN (length('') = 0  OR trim(domain_name) ~* '()' OR smi_domain_id IN (SELECT smi_domain_id FROM smi_domain_keywords  WHERE  keyword ~* '^()$')) THEN TRUE ELSE FALSE END GROUP BY smi_domain_id, domain_name, language_id, parent_id, normalization_domain_id, style_id, mood_id, speed, pitch, volume, open_to_public, smi_domain_owner_email, websupported.smi_domain_id   ORDER BY (websupported.smi_domain_id is NOT NULL) DESC, domain_name")
        # db_info = [r for r in cursor]
        # db_info = sorted(db_info, key=lambda k: k[0])
        # -----------------------

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["smi_domain_id"])

        # here we will check if the request is passed or failed
        count = resp.__len__()
        for x in range(count):
            if x is 35:
                print ("mibo")
            print(x)
            print(resp[x]["smi_domain_id"], str(db_info[x][0]))
            print(resp[x]["name"], str(db_info[x][1]))
            print(resp[x]["mood_id"], str(db_info[x][9]))
            print(resp[x]["open_to_public"], str(db_info[x][8]))
            print(resp[x]["pitch"], str(db_info[x][5]))
            # print(resp[x]["smi_domain_owner_email"], str(db_info[x][12]))
            print(resp[x]["supported"], str(db_info[x][10]))
            print(resp[x]["volume"], str(db_info[x][6]))
            print(resp[x]["style_id"], str(db_info[x][3]))
            print(resp[x]["speed"], str(db_info[x][4]))
            print(resp[x]["normalization_domain_id"], str(db_info[x][7]))
            print(resp[x]["language_id"], str(db_info[x][2]))
            if (self.assertEqual(resp_status_code, 200) is None
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["smi_domain_id"],
                                                                     str(db_info[x][0])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["name"],
                                                                     str(db_info[x][1])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["mood_id"],
                                                                     str(db_info[x][9])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["open_to_public"],
                                                                     str(db_info[x][8])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["pitch"],
                                                                     str(db_info[x][5])))
                    # and (
                    #         Renderer_Functions.compare_two_values_equal(resp[x]["smi_domain_owner_email"],
                    #                                                     str(db_info[x][12])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["supported"],
                                                                     str(db_info[x][10])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["volume"],
                                                                     str(db_info[x][6])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["style_id"],
                                                                     str(db_info[x][3])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["speed"],
                                                                     str(db_info[x][4])))
                    and (
                            Renderer_Functions.compare_two_values_equal(resp[x]["normalization_domain_id"],
                                                                        str(db_info[x][7])))
                    and (Renderer_Functions.compare_two_values_equal(resp[x]["language_id"],
                                                                     str(db_info[x][2])))):
                print("Domain Pass")
                print("resp: {0}".format(resp))

    def _negative_3_domain_incorrect_language_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 99999
        user = {"language_id": language_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")
        print(resp["status_code"])
        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["count"], 0) is None):
                        # and (self.assertEqual(resp['results']["error message"],
                        #                       "Incorrect value for language_id.") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["count"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def _negative_4_domain_missing_parameters(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Missing Parameter language_id.") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def _negative_5_domain_missing_language_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "domain")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Missing Parameter language_id.") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def _negative_6_domain_wrong_login(self):
        # Parameters
        user = {"language_id": 2, "send_unsupported": True}
        resp = Renderer_Functions.send_request("123456789", user, "domain")
        print(resp)
        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

    def _negative_7_domain_missing_login(self):
        # Parameters
        user = {"language_id": 2, "send_unsupported": True}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "domain")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

    def _negative_8_expired_token(self):
        # Parameters
        user = {"language_id": 6, "send_unsupported": True}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "domain")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")
            

if __name__ == '__main__':
    print( "-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)