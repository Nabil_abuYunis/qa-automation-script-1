import os
import logging
import json
from shutil import copyfile
import Logger as Loggit
import wave
import contextlib
import configparser
import time

config = configparser.ConfigParser()
config.sections()
config.read('api_config.ini')

def check_wav_files(resp,folder_name,dl,*args):
    if len(args) >0 :
        yadda = args[2]
        mean = what_mood(args[1])
    
    print("**********Checking for .wave file**********\n")
    '''
    if dl:
        destination = config['LOCATION']['local'] + folder_name + "\\"
        #destination = config['LOCATION']['local']+ yadda +"_"+folder_name +\
        #              "\\" + mean + "\\"
        if not os.path.exists(destination):
            os.makedirs(destination)
    '''

    location = config['LOCATION']['wav']
    
    #folder = os.listdir(location)
    if 'error' in resp.keys():
        if eh.check_known_error(resp['error']):
            print("Known Error: {0}".format(resp['error']))
            print("*****Proceeding with sanity test*****");
        else:
            print("Logging found error...")
            print(resp['error'])
            
        return False
    else:
        arr = resp["results"]["filename"].split('/')
        file = arr[len(arr)-1]
        file_location = location + "\\" + file
        print(file_location)
        if os.path.exists(file_location):
            print("Found wav file: " + file_location)
            log_wav_stats(file_location)
            return True
        else:
            print("File was not found???")
            return False

def log_wav_stats(file_location):
    with contextlib.closing(wave.open(file_location,'r')) as f:
        frames = f.getnframes()
        rate = f.getframerate()
        duration = frames / float(rate)
        print("WAV file duration: {0}".format(duration) + " secs")
    statinfo = os.stat(file_location).st_size
    print("WAV file size: {0}".format(statinfo) + " bytes")


def what_mood(mood):
    if mood is "4":
        return "Apologetic"
    elif mood is "5":
        return "Angry"
    elif mood is "7":
        return "Cheerful"
    elif mood is "10":
        return "Neutral"
    elif mood is "11":
        return "Stern"

    
