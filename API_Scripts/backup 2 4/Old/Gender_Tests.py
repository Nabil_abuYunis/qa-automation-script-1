import urllib2
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from random import randint


class VBGenderSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Start run test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print "\n---------------------------------------------------------------------------------------------"
        print "Completed running test case: {0}".format(str(self.id()))
        print "-----------------------------------------------------------------------------------------------\n"
        print self.currentResult

    def test_positive_1_gender(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    user = {"send_unsupported": True}
                    resp = Renderer_Functions.gender(resp["cookies"][0], user)

                    # Fetch the info from Database
                    cursor = Renderer_Functions.connect_to_db()
                    cursor.execute("WITH webSupported as (SELECT DISTINCT gender_id FROM smi_voice where smi_voice_id IN (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)) select gender_id, name, supported, (websupported.gender_id is not NULL OR gender_id = 0) web_supported from gender LEFT OUTER JOIN websupported using (gender_id) ORDER BY (websupported.gender_id is not NULL OR gender_id = 0) DESC, gender.gender_id ")

                    db_info = [r for r in cursor]
                    db_info = sorted(db_info, key=lambda k: k[0])

                    # Reorder the API received rows
                    resp_status_code = resp["status_code"]
                    resp = sorted(resp["Results"], key=lambda k: k['Gender_Id'])

                    # Choose a random row to test
                    random_line = randint(0, len(db_info) - 1)

                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp_status_code, 200)) is None \
                            and (self.assertEqual(resp[random_line]["Gender_Id"], db_info[random_line][0]) is None) \
                            and (self.assertEqual(resp[random_line]["Supported"], db_info[random_line][3]) is None) \
                            and (self.assertEqual(resp[random_line]["Name"], db_info[random_line][1]) is None):
                        print "Gender Pass"
                        print "resp: {0}".format(resp)
                        print "Gender_Id: {0}".format(resp[random_line]["Gender_Id"])
                        print "Supported: {0}".format(resp[random_line]["Supported"])
                        print "Name: {0}".format(resp[random_line]["Name"])

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_2_gender_missing_sendUnsupported(self):
        # valid username and password
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["Results"]["User"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print "Login Pass"
                    print "Token: {0}".format(resp["cookies"])

                    # Parameters
                    user = {}
                    resp = Renderer_Functions.gender(resp["cookies"][0], user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 400)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Missing Parameter sendUnsupported.") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

            except urllib2.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print "-|-Result:Test case Failed"
                print "Reason: {0}".format(e.reason)

        # Check error type
        elif resp["status_code"] != 200:
            print "Login Failed"
            print "Status Code: {0}".format(resp["status_code"])
            print "Reason: {0}".format(resp["details"])

    def test_negative_3_gender_wrong_login(self):
                    # Parameters
                    user = {"send_unsupported": True}
                    resp = Renderer_Functions.gender("123456789", user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 401)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Unauthorized Access") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

    def test_negative_4_gender_missing_login(self):
                    # Parameters
                    user = {"send_unsupported": True}
                    resp = Renderer_Functions.gender("NO TOKEN", user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] != 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 401)) is None \
                                    and (self.assertEqual(resp["details"],
                                                          "Unauthorized Access") is None):
                                print "-|-Result:Test case Pass"
                                print "resp: {0}".format(resp)
                                print "Status Code: {0}".format(resp["status_code"])
                                print "Reason: {0}".format(resp["details"])

                        except urllib2.URLError as e:
                            print "-|-Result:Test case Failed"
                            print "Reason: {0}".format(e.reason)

                    # Check error type
                    elif resp["status_code"] == 200:
                        print "Negative Test Failed"

if __name__ == '__main__':
    print "-------------Test Result----------------\n"
    testResult = unittest.main(verbosity=1)
