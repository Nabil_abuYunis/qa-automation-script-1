import urllib2
import json
import requests
import pypyodbc
import pyodbc
import psycopg2
#import MySQLdb as mdb
import psycopg2 as pgdb
import psycopg2.extras
import os
import sys

# baseUrl = "http://50.197.139.145:8280"
# baseUrl = "http://ec2-52-53-250-125.us-west-1.compute.amazonaws.com:8080/"
# baseUrl = "http://10.1.10.119:8080"
baseUrl = "http://10.1.10.138:8080"
dbUrl = "10.1.10.138"
# baseUrl = "ec2-54-67-28-43.us-west-1.compute.amazonaws.com"
# baseUrl = "http://54.83.141.67"
# baseUrl = "https://smorph-say.speechmorphing.com:8443"
# baseUrl = "http://smorph-dev.speechmorphing.com:9090" #feedback
boundary = ""


def login(user):
        user_json = json.dumps(user)

        # create request
        # url = baseUrl + "/renderer/login"
        url = baseUrl + "/smorphing/1.0/login"
        request = urllib2.Request(url)

        # add header to request
        headers = {"Content-Type": "application/json"}
        for key, value in headers.items():
            request.add_header(key, value)

        # t = api_request(user_json, request, url, headers)

        # post json data
        response = requests.post(url, user_json, headers)
        content_json = json.loads(response.content)
        print "response.status_code {0}".format(response.status_code)

        if response.status_code == 200:
            try:
                resp = {}
                resp["status_code"] = response.status_code
                resp["content"] = content_json
                resp["cookies"] = response.cookies.values()
                return resp
            except:
                resp = {}
                resp["status_code"] = response.status_code
                resp["error_code"] = content_json["Results"]["Error Code"]
                resp["details"] = content_json["Results"]["Error Message"]
                return resp


        '''
        if response.status_code == 200:
            resp = {}
            resp["status_code"] = response.status_code
            resp["content"] = content_json
            resp["cookies"] = response.cookies.values()
            return resp

        else:
            resp = {}
            resp["status_code"] = response.status_code
            resp["error_code"] = content_json["Results"]["Error Code"]
            resp["details"] = content_json["Results"]["Error Message"]
            return resp
        '''

def signup(user):

    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/vb/signup"
    url = baseUrl +  "/smorphing/1.0/signup"
    request = urllib2.Request(url)

    # add header to request
    headers = {"Content-Type": "application/json"}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def voice_details(token, user):

    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/renderer/voicedetails"
    url = baseUrl + "/smorphing/1.0/voicedetails"

    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def get_voices(token, user):

    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/renderer/voices"
    url = baseUrl + "/smorphing/1.0/voices"
    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def domain(token, user):

    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/renderer/domain"
    url = baseUrl + "/smorphing/1.0/domain"
    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def filename(token, user):

    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/renderer/filename"
    url = baseUrl + "/smorphing/1.0/filename"

    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def gettext(token, user):

    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/renderer/text"
    url = baseUrl + "/smorphing/1.0/text"
    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def savetext(token, user):

    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/renderer/savetext"
    url = baseUrl + "/smorphing/1.0/savetext"

    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def share(token, user):

    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/renderer/share"
    url = baseUrl + "/smorphing/1.0/share"
    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def feedback(token, user):

    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/vb/feedback"
    url = baseUrl + "/smorphing/1.0/feedback"

    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def say(token, user):
    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/smorph/say"
    url = baseUrl + "/smorphing/1.0/say/file"
    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t

"""
def user_group(token, user):

    user_json = json.dumps(user)

    # create request
    url = baseUrl + "/vb/usergroup"
    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t
"""

def gesture(token, user):

    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/renderer/gesture"
    url = baseUrl + "/smorphing/1.0/gesture"
    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def mood(token, user):

    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/renderer/mood"
    url = baseUrl + "/smorphing/1.0/mood"
    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def style(token, user):

    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/renderer/style"
    url = baseUrl + "/smorphing/1.0/style"

    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def gender(token, user):

    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/renderer/gender"
    url = baseUrl + "/smorphing/1.0/gender"
    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def language(token, user):

    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/renderer/language"
    url = baseUrl + "/smorphing/1.0/language"
    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def age(token, user):
    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/renderer/age"
    url = baseUrl + "/smorphing/1.0/age"
    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def textlists(token, user):
    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/renderer/textlists"
    url = baseUrl + "/smorphing/1.0/textlists"
    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def logout(token, user):
    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/renderer/logout"
    url = baseUrl + "/smorphing/1.0/logout"
    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def version(token, user):
    user_json = json.dumps(user)

    # create request
    # url = baseUrl + "/vb/getMyDetails"
    url = baseUrl + "/smorphing/1.0/getmydetails"
    request = urllib2.Request(url)

    # add header to request, also add or not a token within
    if token == "NO TOKEN":
        headers = {"Content-Type": "application/json"}
    else:
        headers = {"Content-Type": "application/json", "Cookie": "XSRF-TOKEN={0}".format(token)}
    for key, value in headers.items():
        request.add_header(key, value)

    response = requests.get(url, headers)
    resp = {}
    resp["status_code"] = response.status_code
    resp["details"] = json.loads(response.content)
    print (resp["details"])
    return resp


def api_request(user_json, request, url, headers):
    try:
        response = urllib2.urlopen(request, user_json)
        resp = json.loads(response.read())
        # this below "try" code is only for signup tests
        try:
            if resp["Status Code"] == 400:
                return resp
        except:
            resp["status_code"] = response.code
            # Added this due to new issue with the new API paths, need to check if it doesn't interfere
            # in other tests
            if resp["status_code"] == 200:
                response = requests.post(url, user_json, headers)
                resp = {}
                resp["status_code"] = response.status_code
                if response.status_code != 404:
                    content_json = json.loads(response.content)
                    try:
                        resp["details"] = content_json["results"]["error message"]
                        print (resp["details"])
                    except:  # the tests that uses sendUnsupported have the error in differnt path
                        resp["details"] = content_json["error message"]
                        print (resp["details"])
                return resp
            # return resp
    except:  # if the answer above is different than 200 then use the second method in order to get the reason
        response = requests.post(url, user_json, headers)
        resp = {}
        resp["status_code"] = response.status_code
        if response.status_code != 404:
            content_json = json.loads(response.content)
            try:
                resp["details"] = content_json["results"]["error message"]
                print (resp["details"])
            except:  # the tests that uses sendUnsupported have the error in differnt path
                resp["details"] = content_json["error message"]
                print (resp["details"])
        return resp


def check_error_messages(argument):
    switcher = {
        400: "Bad Request",
        401: "Unauthorized",
        402: "Payment Required",
        403: "Forbidden",
        404: "Not Found",
        405: "Method Not Allowed",
        406: "Not Acceptable",
        407: "Proxy Authentication Required",
        408: "Request Timeout",
        409: "Conflict",
        410: "Gone",
        411: "Length Required",
        412: "Precondition Failed",
        413: "Payload Too Large",
        414: "URI Too Long",
        415: "Unsupported Media Type",
        416: "Range Not Satisfiable",
        417: "Expectation Failed",
        418: "I'm a teapot",
        421: "Misdirected Request",
        422: "Unprocessable Entity",
        423: "Locked",
        424: "Failed Dependency",
        426: "Upgrade Required",
        428: "Precondition Required",
        429: "Too Many Requests",
        431: "Request Header Fields Too Large",
        451: "Unavailable For Legal Reasons",
    }
    return switcher.get(argument, "Unidentified Error")


def connect_to_db():

    '''
    connection = pyodbc.connect('Driver={SQL Server};'
                                  'SERVER=Ss-werver=smorphdb.crlxwxg8ztgz.uest-1.rds.amazonaws.com;'
                                  'port=5432;'
                                  'Database=voicebank_Says;'
                                  'uid=vbuser;pwd=gfdsa12345!!')
    '''
    '''
    connection = pypyodbc.connect(driver='{SQL Server}', server="Ss-werver=smorphdb.crlxwxg8ztgz.uest-1.rds.amazonaws.com", host="localhost", port="5432" , database="voicebank_Says",
                          trusted_connection="yes", user="vbuser", password="gfdsa12345!!")
    '''

    '''
    connection = pyodbc.connect('DRIVER={SQL Server};SERVER=Ss-werver=smorphdb.crlxwxg8ztgz.uest-1.rds.amazonaws.com;PORT:5432;DATABASE=voicebank_Says;UID=vbuser;PWD=gfdsa12345!!')
    '''

    '''
    connection_string = "host='Ss-werver=smorphdb.crlxwxg8ztgz.uest-1.rds.amazonaws.com' port='5432' dbname='voicebank_Says' user='vbuser' password='gfdsa12345!!'"
    connection = psycopg2.connect(connection_string)
    '''
    '''
    connection = psycopg2.connect(database="voicebank_Says", user="vbuser", password="gfdsa12345!!", host="host='Ss-werver=smorphdb.crlxwxg8ztgz.uest-1.rds.amazonaws.com", port="5432")
    print "Opened database successfully"

    print "connbefore {0}".format(connection)
    '''

    # # self.con = mdb.connect(config.DB_HOSTNAME,config.DB_USERNAME, config.DB_PASSWORD, config.DB_NAME)
    # connection = pgdb.connect("dbname='voicebank_Says' user='vbuser' host='Ss-werver=smorphdb.crlxwxg8ztgz.uest-1.rds.amazonaws.com' port='5432' password='gfdsa12345!!'")

    connection = pgdb.connect(
        "dbname='voicebank' user='vbuser' host=" + dbUrl + " port='5432' password='gfdsa12345!!'")
    cursor = connection.cursor()

    return cursor

    # closing connection


def compare_two_values_equal(side1, side2):
    # Check first that side 2 with is from the DB is not None, if it is then replace it with 0

    side2 = '0' if side2 == 'None' else str(side2)

    # Compare
    if str(side1) == str(side2):
        return True
    else:
        return False
