import urllib.error as urllibe
import unittest
import Renderer_Functions
from random import randint
from Database import Postgres
import JSON_Request as Data_Utils
import random

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBTemplateSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_say_temp(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        # get template_id
        user = {"template": "yes"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]

        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
        #             'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
        #             'is open on a holiday?</speak>'
        text_ssml = 'testing a new template input by API'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        cursor.execute(
            "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        #file_format = "wav"

        choice = randint(0, 1)

        if choice == 0:
            template_original = "no"
        else:
            template_original = "yes"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and self.assertGreater(resp["results"]["filenumber"], 0) is None \
                and self.assertIn(".wav", resp["results"]["filename"]) is None:
            # if wav.check_wav_files(resp,"",True):
                print ("Templates Pass")
                print ("resp: {0}".format(resp))
                print ("status_code: {0}".format(resp["status_code"]))
                print ("Filename: {0}".format(resp['results']['filename']))
        else:
            print("FAILURE: response code was not 200")

    def test_positive_2_say_temp_optional_file_format(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        # get template_id
        user = {"template": "yes"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]
        print(template_id)

        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
        #             'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
        #             'is open on a holiday?</speak>'
        text_ssml = 'testing a new template input by API'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        cursor.execute(
            "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        # request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        all_file_formats = ["wav", "pcma", "pcmu"]
        file_format = random.choice(all_file_formats)

        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id": template_id, "template_original": template_original,
                "format": file_format}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        print("***********VALIDATING API RESPONSE**********")
        if resp == None:
            print("-----No response back-----")

        print("Response Status Code: {0}".format(resp["status_code"]))
        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and self.assertGreater(resp["results"]["filenumber"], 0) is None \
                and self.assertIn("." + file_format, resp["results"]["filename"]) is None:
            # if wav.check_wav_files(resp,"",True):
            print("Templates Pass")
            print("resp: {0}".format(resp))
            print("status_code: {0}".format(resp["status_code"]))
            print("Filename: {0}".format(resp['results']['filename']))
        else:
            print("FAILURE: response code was not 200")

    def test_positive_2_say_temp_optional_file_type(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        # get template_id
        user = {"template": "yes"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]
        print(template_id)

        # text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
        #             'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
        #             'is open on a holiday?</speak>'
        text_ssml = 'testing a new template input by API'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        cursor.execute(
            "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        # request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        all_url_types = ["http", "smb"]
        url_type = random.choice(all_url_types)

        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id": template_id, "template_original": template_original,
                "url_type": url_type}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        print("***********VALIDATING API RESPONSE**********")
        if resp == None:
            print("-----No response back-----")

        print("Response Status Code: {0}".format(resp["status_code"]))
        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and self.assertGreater(resp["results"]["filenumber"], 0) is None \
                and self.assertIn(url_type, resp["results"]["filename"]) is None:
            # if wav.check_wav_files(resp,"",True):
            print("Templates Pass")
            print("resp: {0}".format(resp))
            print("status_code: {0}".format(resp["status_code"]))
            print("Filename: {0}".format(resp['results']['filename']))
        else:
            print("FAILURE: response code was not 200")

    def test_negative_4_incorrect_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        user = {"template": "no"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]
        print(template_id)

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "658945"
        #request_id = "123456"
        smi_domain_id = "precise"
        #file_format = "wav"

        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200:
            print("###")
            print(resp)
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Incorrect value for smi_domain_id.") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_5_incorrect_voice(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        user = {"template": "no"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]
        print(template_id)

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = "precise"
        request_id = "658945"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        #file_format = "wav"

        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Incorrect value for smi_voice_id.") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_6_missing_domain_param(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        user = {"template": "no"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]
        print(template_id)

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "123456"
        #file_format = "wav"

        template_original = "no"

        user = {"smi_voice_id": smi_voice_id,"text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original, "request_id" : request_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Missing Parameter smi_domain_id") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_7_missing_req_id_param(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        user = {"template": "no"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]
        print(template_id)

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        #request_id = "123456"
        #file_format = "wav"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        template_original = "no"

        user = {"smi_voice_id": smi_voice_id,"text_ssml": text_ssml, "smi_domain_id":smi_domain_id, "template_id" : template_id, "template_original":template_original}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Missing Parameter request_id") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_8_missing_voice_param(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        user = {"template": "no"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]
        print(template_id)

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        #smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "123456"
        #file_format = "wav"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        template_original = "no"

        user = {"request_id": request_id, "text_ssml": text_ssml, "smi_domain_id":smi_domain_id, "template_id" : template_id, "template_original":template_original}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Missing Parameter smi_voice_id") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_9_missing_template_param(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # user = {"template": "no"}
        # resp2 = Renderer_Functions.filename(resp["cookies"][0], user)
        # resp_extract = resp2["results"][0]["list"]
        # count = resp2["results"][0]["count"]
        # # Choose a random row to test
        # random_line = randint(0, count - 1)
        # template_id = resp_extract[random_line]["template_id"]
        # print(template_id)

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "123456"
        #file_format = "wav"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        template_original = "no"

        user = {"request_id": request_id, "text_ssml": text_ssml, "smi_domain_id":smi_domain_id, "smi_voice_id" : smi_voice_id, "template_original":template_original}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Missing Parameter template_id") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_10_missing_template_orig_param(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        user = {"template": "no"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]
        print(template_id)

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "123456"
        #file_format = "wav"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        # template_original = "no"

        user = {"request_id": request_id, "text_ssml": text_ssml, "smi_domain_id":smi_domain_id, "smi_voice_id" : smi_voice_id, "template_id":template_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Missing Parameter template_original") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_11_incorrect_template_original(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        user = {"template": "no"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]
        print(template_id)

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "658945"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        #file_format = "wav"

        template_original = 123456789

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Incorrect value for template_original.") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_12_incorrect_ssml(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        user = {"template": "no"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]
        print(template_id)

        # Parameters
        text_ssml = 123456789
        smi_voice_id = "72057594037928293"
        request_id = "123456"
        #file_format = "wav"
        smi_domain_id = "72057594037927955"
        template_original = "no"

        user = {"smi_voice_id": smi_voice_id,"text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original, "request_id" : request_id, "smi_domain_id": smi_domain_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Incorrect value for text_ssml.") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_13_incorrect_file_format(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        user = {"template": "no"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]
        print(template_id)

        # Parameters
        text_ssml = "To build an empire"
        # smi_voice_id = "72057594037928293"
        cursor = Postgres.connect_to_db()
        cursor.execute(
            "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "123456"
        # file_format = 123456
        format = 123456
        # smi_domain_id = "72057594037927955"
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        template_original = "no"

        user = {"smi_voice_id": smi_voice_id,"text_ssml": text_ssml, "template_id" : template_id, "file_format": format, "template_original":template_original, "request_id" : request_id, "smi_domain_id": smi_domain_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Incorrect value for format.") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_14_missing_ssml(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        user = {"template": "no"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]
        print(template_id)

        # Parameters
        smi_voice_id = "72057594037928293"
        request_id = "123456"
        #file_format = "wav"
        smi_domain_id = "72057594037927955"
        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "template_id" : template_id, "template_original":template_original, "request_id" : request_id, "smi_domain_id": smi_domain_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "Missing Parameter text_ssml.") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_15_blank_text(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        user = {"template": "no"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]
        print(template_id)

        # Parameters
        text_ssml = ''
        # smi_voice_id = '72057594037928282'
        # request_id = "654334"
        # #request_id = "123456"
        # smi_domain_id = '72057594037927948'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        cursor.execute(
            "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        # request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        # file_format = "wav"

        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

    def test_negative_16_template_missing_login(self):
        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "654334"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        template_id = "568798633"
        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "templates")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")
            
    def test_negative_17_template_wrong_login(self):
        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "654334"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        template_id = "568798633"
        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
        resp = Renderer_Functions.send_request("123456789", user, "templates")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

    def test_negative_18_expired_token(self):
        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "654334"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        template_id = "568798633"
        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}

        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "templates")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

    def test_negative_19_no_permission_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        user = {"template": "no"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]
        print(template_id)

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927954" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "785435"
        cursor = Postgres.connect_to_db()
        cursor.execute(
            "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        #request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = false AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        #file_format = "wav"

        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              " you do not have permission to us this domain. ") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_20_no_permission_voice(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        user = {"template": "no"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]
        print(template_id)

        # Parameters
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037927968" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        # get voice that does not have permission
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        cursor.execute(
            "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = false AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "785435"
        #request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        #file_format = "wav"

        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["error_message"],
                                              "you do not have permission to us this voice. ") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_21_no_permission_lang(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        user = {"template": "no"}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        template_id = resp_extract[random_line]["template_id"]

        # Parameters
        language = 200
        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="' + str(language) + '" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        request_id = "785435"
        #request_id = "123456"
        smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        #file_format = "wav"

        template_original = "no"

        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "template_id" : template_id, "template_original":template_original}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templates")

        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["error_message"],
                                              " you do not have permission to use this language. ") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["error_message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)