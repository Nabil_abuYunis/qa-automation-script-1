import unittest
import Renderer_Functions as Renderer_Functions

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBFeedbackSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print( "-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Completed running test case: {0}".format(str(self.id())))
        print( "-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print( self.currentResult)

    def test_positive_1_feedback(self):
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # Parameters
        user_id = "112233445566"
        reply_email = "bogy@google.com"
        text = "testing only"
        # user = {"voice_owner_id": user_id, "email": reply_email, "text": text}
        # user = {"voice_owner_id": user_id, "reply_to_email": reply_email, "message": text}
        user = {"voice_owner_id": user_id, "reply_email": reply_email, "text": text}
        # resp = Renderer_Functions.feedback(resp["cookies"][0], user)
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "feedback")
        # here we will check if the voice detail is passed or failed
        resp_status_code = resp["status_code"]


        # Check error type
        # else:
        #     print( "Login Failed")
        #     print( "Status Code: {0}".format(resp["status_code"]))
        #     print( "Reason: {0}".format(resp["details"]))


if __name__ == '__main__':
    print( "-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
