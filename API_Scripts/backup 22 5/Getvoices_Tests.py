import unittest
import Renderer_Functions
from Database import Postgres
import random
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBGetvoicesSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    # without any optional parameters, we should get all the voices
    def _positive_1_getvoices_all(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']
        # parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"send_unsupported": True, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "voices")

        # Take the account id of the chosen project
        account_project_split = random_project.split('-')
        # Fetch the info from the voices table
        cursor = Postgres.connect_to_db()

        query = "select * from smi_voice where smi_voice_owner_account_id ilike '" + account_project_split[0] + "'"
        cursor.execute(query)
        # cursor.execute(
        #     "WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id) WHERE (open_to_public  OR smi_voice_owner_email = '" + email + "' OR user_email ILIKE '" + email + "') AND CASE WHEN (length('') = 0 OR trim(name) ~* '()' OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  WHERE keyword ~* '^()$')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, websupported.smi_voice_id  ORDER BY  (websupported.smi_voice_id is not NULL) DESC, name")

        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k['smi_voice_id'])

        # here we will check if the request is passed or failed
        count = resp.__len__()
        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        for x in range(count):
            print(x)
            print(resp[x]["smi_voice_id"], str(db_info[x][0]))
            print(resp[x]["name"], db_info[x][1])
            if (self.assertEqual(resp_status_code, 200)) is None \
                and (self.assertEqual(resp[x]["smi_voice_id"], str(db_info[x][0])) is None) \
                and (self.assertEqual(resp[x]["name"], db_info[x][1]) is None):
                print("Get Voice Pass")
                print("resp: {0}".format(resp))
                print("status_code: {0}".format(resp_status_code))
            else:
                print("Positive Test Failed")
                assert False

    print("Get voices Pass")

    def _positive_2_getvoices_optional_gender_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        email = resp['content']['results']['user']['email_address']

        # Get random gender_id
        cursor = Postgres.connect_to_db()
        # queryGender = "select distinct gender_id from smi_voice Where supported = True AND open_to_public = True"
        queryGender = "select distinct gender_id from smi_voice Where supported = True"
        cursor.execute(queryGender)
        db_infoGender = [r for r in cursor]
        gender_id = random.choice(db_infoGender)
        gender_id = gender_id[0]
        # gender_id = 2
        send_unsupported = True
        language_id = 6

        # parameters
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"gender_id": gender_id, "send_unsupported": send_unsupported, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "voices")

        # Take the account id of the chosen project
        account_project_split = random_project.split('-')
        query = "select * from smi_voice where smi_voice_owner_account_id ilike '" + account_project_split[0] + "'"
        cursor.execute(query)
        # queryAllVoices = "WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id) WHERE (open_to_public  OR smi_voice_owner_email = '" + email + "' OR user_email ILIKE '" + email + "') AND CASE WHEN (length('') = 0 OR trim(name) ~* '()' OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  WHERE keyword ~* '^()$')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, websupported.smi_voice_id  ORDER BY  (websupported.smi_voice_id is not NULL) DESC, name"
        # cursor.execute(queryAllVoices)
        db_infoAll = [r for r in cursor]
        queryGender = "select distinct smi_voice_id from smi_voice where gender_id = " + str (gender_id)
        cursor.execute(queryGender)
        db_infoGender = [r for r in cursor]
        db_info = []
        if gender_id != 0:
            # db_info = [i for i in db_infoGender[0] if i in db_infoAll[0]]
            for i in db_infoGender:
                for j in db_infoAll:
                    # print ("i[0]: ", i[0], ", j[0]: ", j[0])
                    if i[0] == j[0]:
                        db_info.append(j)

        # db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

       # Reorder the API received rows
        resp_status_code = resp["status_code"]
        count = resp['count']
        resp = sorted(resp["results"], key=lambda k: k['smi_voice_id'])

        # here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        for x in range(count):
            #print(resp['results'][random_line]["smi_voice_id"] + "-" + str(x[0]))
            # if resp['results'][random_line]["smi_voice_id"] == str(x[0]):
            #     print("Found")
            print (resp[x]["smi_voice_id"], db_info[x][0])
            print (resp[x]["name"], db_info[x][1])
            if (self.assertEqual(resp_status_code, 200)) is None \
                and (self.assertEqual(resp[x]["smi_voice_id"], str(db_info[x][0])) is None) \
                and (self.assertEqual(resp[x]["name"], db_info[x][1]) is None):
                print("Get Voice Pass")
                print("resp: {0}".format(resp))
                print("status_code: {0}".format(resp_status_code))
            else:
                print("Positive Test Failed")
                assert False

    print("Get voices Pass")

    def _positive_3_getvoices_optional_age_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # parameters
        # Get random age_id
        cursor = Postgres.connect_to_db()
        # queryAge = "select distinct age_id from smi_voice Where supported = True AND open_to_public = True"
        queryAge = "select distinct age_id from smi_voice Where supported = True"
        cursor.execute(queryAge)
        db_infoAge = [r for r in cursor]
        age_id = random.choice(db_infoAge)
        age_id = age_id[0]
        # age_id = 5
        send_unsupported = True
        language_id = 6

        # parameters
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"age_id": age_id, "send_unsupported": send_unsupported, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "voices")
        resp_status_code = resp["status_code"]
        count = resp['count']

        # cursor = Postgres.connect_to_db()
        # cursor.execute("WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id) WHERE (open_to_public  OR smi_voice_owner_email = '" + email + "' OR user_email ILIKE '" + email + "') AND CASE WHEN (length('') = 0 OR trim(name) ~* '()' OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  WHERE keyword ~* '^()$')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, websupported.smi_voice_id  ORDER BY  (websupported.smi_voice_id is not NULL) DESC, name")
        # db_info = [r for r in cursor]
        # db_info = sorted(db_info, key=lambda k: k[0])

        # Take the account id of the chosen project
        account_project_split = random_project.split('-')
        query = "select * from smi_voice where smi_voice_owner_account_id ilike '" + account_project_split[0] + "'"
        cursor.execute(query)

        # cursor = Postgres.connect_to_db()
        # queryAllVoices = "WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id) WHERE (open_to_public  OR smi_voice_owner_email = '" + email + "' OR user_email ILIKE '" + email + "') AND CASE WHEN (length('') = 0 OR trim(name) ~* '()' OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  WHERE keyword ~* '^()$')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, websupported.smi_voice_id  ORDER BY  (websupported.smi_voice_id is not NULL) DESC, name"
        # cursor.execute(queryAllVoices)
        db_infoAll = [r for r in cursor]
        queryAge = "select distinct smi_voice_id from smi_voice where age_id = " + str(age_id)
        cursor.execute(queryAge)
        db_infoAge = [r for r in cursor]
        db_info = []
        if age_id != 0:
            # db_info = [i for i in db_infoGender[0] if i in db_infoAll[0]]
            for i in db_infoAge:
                for j in db_infoAll:
                    # print ("i[0]: ", i[0], ", j[0]: ", j[0])
                    if i[0] == j[0]:
                        db_info.append(j)

        db_info = sorted(db_info, key=lambda k: k[0])
        resp = sorted(resp["results"], key=lambda k: k['smi_voice_id'])

        # here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        for x in range(count):
            print(resp[x]["smi_voice_id"], db_info[x][0])
            print(resp[x]["name"], db_info[x][1])
            if (self.assertEqual(resp_status_code, 200)) is None \
                    and (self.assertEqual(resp[x]["smi_voice_id"], str(db_info[x][0])) is None) \
                    and (self.assertEqual(resp[x]["name"], db_info[x][1]) is None):
                print("Get Voice Pass")
                print("resp: {0}".format(resp))
                print("status_code: {0}".format(resp_status_code))
            else:
                print("Positive Test Failed")
                assert False

    print("Get voices Pass")

    def _positive_4_getvoices_optional_language_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # parameters
        # Get random language_id
        cursor = Postgres.connect_to_db()
        # queryLanguage = "select distinct language_id from smi_voice Where supported = True AND open_to_public = True"
        queryLanguage = "select distinct language_id from smi_voice Where supported = True"
        cursor.execute(queryLanguage)
        db_infoLanguage = [r for r in cursor]
        language_id = random.choice(db_infoLanguage)
        language_id = language_id[0]
        # language_id = 6
        send_unsupported = True

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"language_id": language_id, "send_unsupported": send_unsupported, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "voices")
        resp_status_code = resp["status_code"]
        count = resp['count']

        # Take the account id of the chosen project
        account_project_split = random_project.split('-')
        query = "select * from smi_voice where smi_voice_owner_account_id ilike '" + account_project_split[0] + "'"
        cursor.execute(query)

        # cursor = Postgres.connect_to_db()
        # cursor.execute("WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id) WHERE (open_to_public  OR smi_voice_owner_email = '" + email + "' OR user_email ILIKE '" + email + "') AND CASE WHEN (length('') = 0 OR trim(name) ~* '()' OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  WHERE keyword ~* '^()$')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, websupported.smi_voice_id  ORDER BY  (websupported.smi_voice_id is not NULL) DESC, name")
        # db_info = [r for r in cursor]
        # db_info = sorted(db_info, key=lambda k: k[0])
        # cursor = Postgres.connect_to_db()
        # queryAllVoices = "WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id) WHERE (open_to_public  OR smi_voice_owner_email = '" + email + "' OR user_email ILIKE '" + email + "') AND CASE WHEN (length('') = 0 OR trim(name) ~* '()' OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  WHERE keyword ~* '^()$')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, websupported.smi_voice_id  ORDER BY  (websupported.smi_voice_id is not NULL) DESC, name"
        # cursor.execute(queryAllVoices)
        db_infoAll = [r for r in cursor]
        queryLanguage = "select distinct smi_voice_id from smi_voice where language_id = " + str(language_id)
        cursor.execute(queryLanguage)
        db_infoLanguage = [r for r in cursor]
        db_info = []
        if language_id != 0:
            # db_info = [i for i in db_infoGender[0] if i in db_infoAll[0]]
            for i in db_infoLanguage:
                for j in db_infoAll:
                    # print ("i[0]: ", i[0], ", j[0]: ", j[0])
                    if i[0] == j[0]:
                        db_info.append(j)

        db_info = sorted(db_info, key=lambda k: k[0])
        resp = sorted(resp["results"], key=lambda k: k['smi_voice_id'])

        # here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        for x in range(count):
            print(resp[x]["smi_voice_id"], db_info[x][0])
            print(resp[x]["name"], db_info[x][1])
            if (self.assertEqual(resp_status_code, 200)) is None \
                    and (self.assertEqual(resp[x]["smi_voice_id"], str(db_info[x][0])) is None) \
                    and (self.assertEqual(resp[x]["name"], db_info[x][1]) is None):
                print("Get Voice Pass")
                print("resp: {0}".format(resp))
                print("status_code: {0}".format(resp_status_code))
            else:
                print("Positive Test Failed")
                assert False

    print("Get voices Pass")

    def _positive_5_getvoices_optional_voicename(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # parameters
        # Get random voicename
        cursor = Postgres.connect_to_db()
        # queryvoicename = "select distinct name from smi_voice Where supported = True AND open_to_public = True"
        queryvoicename = "select distinct name from smi_voice Where supported = True"
        cursor.execute(queryvoicename)
        db_infoVoicename = [r for r in cursor]
        name = random.choice(db_infoVoicename)
        name = name[0]
        send_unsupported = True
        language_id = 6

        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"voicename": name, "send_unsupported": send_unsupported, "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "voices")
        resp_status_code = resp["status_code"]
        count = resp['count']

        # Take the account id of the chosen project
        account_project_split = random_project.split('-')
        query = "select * from smi_voice where smi_voice_owner_account_id ilike '" + account_project_split[0] + "'"
        cursor.execute(query)

        # cursor = Postgres.connect_to_db()
        # cursor.execute("WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id) WHERE (open_to_public  OR smi_voice_owner_email = '" + email + "' OR user_email ILIKE '" + email + "') AND CASE WHEN (length('') = 0 OR trim(name) ~* '()' OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  WHERE keyword ~* '^()$')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, websupported.smi_voice_id  ORDER BY  (websupported.smi_voice_id is not NULL) DESC, name")
        # db_info = [r for r in cursor]
        # db_info = sorted(db_info, key=lambda k: k[0])
        # cursor = Postgres.connect_to_db()
        # queryAllVoices = "WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id) WHERE (open_to_public  OR smi_voice_owner_email = '" + email + "' OR user_email ILIKE '" + email + "') AND CASE WHEN (length('') = 0 OR trim(name) ~* '()' OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  WHERE keyword ~* '^()$')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, websupported.smi_voice_id  ORDER BY  (websupported.smi_voice_id is not NULL) DESC, name"
        # cursor.execute(queryAllVoices)
        db_infoAll = [r for r in cursor]
        queryvoice = "select distinct smi_voice_id from smi_voice where name iLike '" + str(name) + "'"
        cursor.execute(queryvoice)
        queryvoice = [r for r in cursor]
        db_info = []
        # db_info = [i for i in db_infoGender[0] if i in db_infoAll[0]]
        for i in queryvoice:
            for j in db_infoAll:
                # print ("i[0]: ", i[0], ", j[0]: ", j[0])
                if i[0] == j[0]:
                    db_info.append(j)

        db_info = sorted(db_info, key=lambda k: k[0])
        resp = sorted(resp["results"], key=lambda k: k['smi_voice_id'])

        # here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        for x in range(count):
            print(resp[x]["smi_voice_id"], db_info[x][0])
            print(resp[x]["name"], db_info[x][1])
            if (self.assertEqual(resp_status_code, 200)) is None \
                    and (self.assertEqual(resp[x]["smi_voice_id"], str(db_info[x][0])) is None) \
                    and (self.assertEqual(resp[x]["name"], db_info[x][1]) is None):
                print("Get Voice Pass")
                print("resp: {0}".format(resp))
                print("status_code: {0}".format(resp_status_code))
            else:
                print("Positive Test Failed")
                assert False

    print("Get voices Pass")

    def test_positive_6_getvoices_optional_keywords(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # parameters
        # Get random voicename
        cursor = Postgres.connect_to_db()
        permitted_accounts = Renderer_Functions.get_allowed_accounts(email)
        random_account = random.choice(permitted_accounts)

        # querykeywords = "select distinct keyword from smi_voice_keywords where smi_voice_id in (select smi_voice_id from smi_voice where supported = True and open_to_public = True)"
        querykeywords = "select name from smi_voice"
        # querykeywords = "WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website  " \
        #                  "WHERE supported) SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id" \
        #                  " is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  " \
        #                  "LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN " \
        #                  "smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id)" \
        #                  " WHERE (open_to_public  OR smi_voice_owner_account_id = '" + str(random_account[0]) + "'  OR account_id " \
        #                  "ILIKE '" + str(random_account[0]) + "' ) AND  supported = true  AND CASE WHEN (length('') = 0 OR " \
        #                  "trim(name) ~* ('') OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  " \
        #                  "WHERE keyword ~* '')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, " \
        #                  "websupported.smi_voice_id  ORDER BY   supported DESC, name"
        #
        # querykeywords1 = "WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website  WHERE supported) " \
        #                 "SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not " \
        #                 "NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER " \
        #                 "JOIN smi_voice_permissions USING (smi_voice_id)   LEFT OUTER JOIN smi_voice_keywords " \
        #                 "USING (smi_voice_id)   LEFT OUTER JOIN websupported using (smi_voice_id) WHERE " \
        #                 "(open_to_public  OR smi_voice_owner_account_id = '" + str(random_account[0]) + "'  OR " \
        #                 "account_id ILIKE '" + str(random_account[0]) + "' )  AND  supported = true  AND CASE WHEN (length('') = 0 " \
        #                 "OR trim(name) ~* ('') OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords " \
        #                 " WHERE keyword ~* ''))  THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported," \
        #                 " websupported.smi_voice_id  ORDER BY   supported DESC, name;"


        cursor.execute(querykeywords)
        db_infokeywords = [r for r in cursor]

        # cursor.execute(querykeywords1)
        # db_infokeywords1 = [r for r in cursor]

        # Take all words that have more than 3 letters to use as keywords
        keywords = list()
        for line in db_infokeywords:
            for word in line[0].split(" "):
                if len(word) > 3 and word.isalpha():
                    keywords.append(word)

        # Choose random keyword
        keywords = list(set(keywords))
        random_keyword = random.choice(keywords)

        if not db_infokeywords:
            print("no keyword found, skipping")
        else:
            send_unsupported = True
            language_id = 6
            allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
            random_project = random.choice(allowed_projects)
            user = {"keyword": [random_keyword], "keyword_length" : 1  ,"send_unsupported": send_unsupported, "project_id": random_project}
            # user = {"keyword": ["koko"], "send_unsupported": send_unsupported, "project_id": random_project}

            resp = Renderer_Functions.send_request(resp["cookies"][0], user, "voices")
            resp_status_code = resp["status_code"]
            count = resp['count']
            resp = sorted(resp["results"], key=lambda k: k['smi_voice_id'])

            # cursor = Postgres.connect_to_db()
            # cursor.execute("WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id) WHERE (open_to_public  OR smi_voice_owner_email = '" + email + "' OR user_email ILIKE '" + email + "') AND CASE WHEN (length('') = 0 OR trim(name) ~* '()' OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  WHERE keyword ~* '^()$')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, websupported.smi_voice_id  ORDER BY  (websupported.smi_voice_id is not NULL) DESC, name")
            # db_info = [r for r in cursor]
            # db_info = sorted(db_info, key=lambda k: k[0])
            # cursor = Postgres.connect_to_db()

            # # Get account id owner of the voice that have the chosen keywords
            # account_id_query = "select smi_voice_owner_account_id from smi_voice where smi_voice_id = " + str(keyword[0])
            # cursor.execute(account_id_query)
            # account_ids = [r for r in cursor]
            # account_id = random.choice(account_ids)
            #
            # # Get voices that we have permission to
            # # queryAllVoices = "WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id) WHERE (open_to_public  OR smi_voice_owner_email = '" + email + "' OR user_email ILIKE '" + email + "') AND CASE WHEN (length('') = 0 OR trim(name) ~* '()' OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  WHERE keyword ~* '^()$')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, websupported.smi_voice_id  ORDER BY  (websupported.smi_voice_id is not NULL) DESC, name"
            # queryAllVoices = "WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website  " \
            #                  "WHERE supported) SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id" \
            #                  " is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  " \
            #                  "LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN " \
            #                  "smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id)" \
            #                  " WHERE (open_to_public  OR smi_voice_owner_account_id = '" + account_id[0] + "'  OR account_id " \
            #                  "ILIKE '" + account_id[0] + "' ) AND  supported = true  AND CASE WHEN (length('') = 0 OR " \
            #                  "trim(name) ~* ('') OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  " \
            #                  "WHERE keyword ~* '')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, " \
            #                  "websupported.smi_voice_id  ORDER BY   supported DESC, name"
            #
            # cursor.execute(queryAllVoices)
            # db_infoAll = [r for r in cursor]
            # queryvoice = "select distinct smi_voice_id from smi_voice_keywords where keyword iLike '" + str(keyword[1]) + "'"
            # cursor.execute(queryvoice)
            # queryvoice = [r for r in cursor]
            # db_info = []
            # # db_info = [i for i in db_infoGender[0] if i in db_infoAll[0]]
            # for i in queryvoice:
            #     for j in db_infoAll:
            #         # print ("i[0]: ", i[0], ", j[0]: ", j[0])
            #         if i[0] == j[0]:
            #             db_info.append(j)

            # Get voice names that include the chosen keyword from the DB
            # query = "select * from smi_voice"
            # cursor.execute(query)
            # queryvoice = [r for r in cursor]

            # db_info = sorted(db_info, key=lambda k: k[0])

            # Check that the keyword exists in the voices names
            for x in range(count):
                if (self.assertEqual(resp_status_code, 200)) is None \
                        and (self.assertIn(random_keyword.lower(), resp[x]["name"].lower()) is None):
                    print("Get Voice Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp_status_code))
                else:
                    print("Positive Test Failed")
                    assert False

        print("Get voices Pass")

    def _negative_7_getvoices_missing_sendUnsupported(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "voices")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "Missing Parameter send_unsupported.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def _negative_8_getvoices_wrong_login(self):
        email = self.config["LOGIN"]["email"]

        # parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"send_unsupported": True, "project_id": random_project}
        resp = Renderer_Functions.send_request("123456789", user, "voices")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def _negative_9_getvoices_missing_login(self):
        email = self.config["LOGIN"]["email"]

        # parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"send_unsupported": True, "project_id": random_project}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "voices")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def _negative_10_incorrect_sendUnsupported(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        user = {"send_unsupported" : "rules", "project_id": random_project}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "voices")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "Incorrect value for send_unsupported.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def _negative_11_expired_token(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"send_unsupported": True, "project_id": random_project}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "voices")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)