import unittest
import Renderer_Functions
from Database import Postgres
import random
import configparser
import string

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBinviteUsersSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Read config file
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)


    def test_positive_1_inviteUsers(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookie = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']

        # Get account id
        # user = {}
        # resp = Renderer_Functions.send_request(cookie, user, "accountlist")
        # # Get account that have projects within from sql instead of API
        cursor = Postgres.connect_to_db()
        # email = self.config.get('LOGIN', 'email')
        # cursor.execute("SELECT distinct(account_id) FROM user_account_permissions where user_email like 'admin@speechmorphing.com' "
        #                "or user_email like '" + email + "'" + " in (select distinct account_id from project) "
        #                 "intersect (select distinct account_id from project)")
        # db_info = [r for r in cursor]
        # # random_line = randint(0, resp["count"] - 1)
        # # account_id = resp["results"][random_line]["account_id"]
        # random_account = random.choice(db_info)
        # random_account = 'C4398046511112'
        # account_id = random_account

        #------------------
        # permitted_projects = Renderer_Functions.get_allowed_projects_in_allowed_accounts(email)
        # random_project = random.choice(permitted_projects)
        # account = random_project[0].split("-")[0]
        #
        # # email = ''.join(random.choice(string.ascii_letters) for ii in range(6)) + '@speechmorphing.com'
        # emails = ["nabil@speechmorphing.com", "hung@speechmorphing.com", "dillon@speechmorphing.com",
        #           "ashraf@speechmorphing.com", "mahmud@speechmorphing.com"]
        # email = random.choice(emails)

        #------------------

        # Parameters
        permitted_accounts = Renderer_Functions.get_allowed_accounts(email)
        random_account = random.choice(permitted_accounts)
        email = ''.join(random.choice(string.ascii_letters) for ii in range(6)) + '@speechmorphing.com'
        first_name = ''.join(random.choice(string.ascii_letters) for ii in range(6))
        last_name = ''.join(random.choice(string.ascii_letters) for ii in range(6))
        permissions_vals = [11, 0, 13]
        account_permission = random.choice(permissions_vals)
        email_permission_array = [{"email": email, "account_permission": account_permission,  "first_name": first_name, "last_name": last_name}]

        user = {"account_id": random_account[0], "email_permission_array": email_permission_array}
        user1 = {"account_id": "C4398046511107",
                "email_permission_array": [
                {"email": "bharati@speechmorphing.com", "account_permission": 13, "first_name": "Bharati", "last_name": "Adkar"}]
        }

        resp = Renderer_Functions.send_request(cookie, user, "inviteUsers")
        resp1 = Renderer_Functions.send_request(cookie, user1, "inviteUsers")

        # Fetch projects from the Database for the same random account id
        cursor.execute( "select project_name from project where account_id = '" + random_account[0] + "'")
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # here we will check if the request is passed or failed
        count = resp.__len__()

        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        for x in range(count):
            if (self.assertEqual(resp["status_code"], 200)) is None \
                    and (self.assertEqual(resp["results"][x]["project_name"], db_info[x][0]) is None) :
                print ("Project List Pass")
                print("resp: {0}".format(resp))
                print("account_name: {0}".format(resp["results"][x]["project_name"]))
            else:
                print("Positive Test Failed")
                assert False

    print("Invite users Pass")

    def test_negative_2_inviteUsers_wrong_login(self):
        email = self.config["LOGIN"]["email"]

        # Parameters
        permitted_accounts = Renderer_Functions.get_allowed_accounts(email)
        random_account = random.choice(permitted_accounts)
        email = ''.join(random.choice(string.ascii_letters) for ii in range(6)) + '@speechmorphing.com'
        first_name = ''.join(random.choice(string.ascii_letters) for ii in range(6))
        last_name = ''.join(random.choice(string.ascii_letters) for ii in range(6))
        permissions_vals = [11, 0, 13]
        account_permission = random.choice(permissions_vals)
        email_permission_array = [{"email": email, "account_permission": account_permission, "first_name": first_name,
                                   "last_name": last_name}]

        user = {"account_id": random_account[0], "email_permission_array": email_permission_array}

        # user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("123456789", user, "inviteUsers")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_3_inviteUsers_missing_login(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "inviteUsers")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_4_inviteUsers_expired_token(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "inviteUsers")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
