import unittest
import Renderer_Functions
from Database import Postgres
import random

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBJumpSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_jump(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']

        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        #request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        #file_format = "wav"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        token = resp["cookies"][0]
        resp = Renderer_Functions.send_request(cookies, user, "say")

        #Parameters
        filenumber = resp['results']['filenumber']
        direction = "forward"
        units = "words"
        length_of_jump = 10

        user = {"filenumber": filenumber, "direction": direction, "units": units,
                "length_of_jump": length_of_jump}
        resp = Renderer_Functions.send_request(cookies, user, "jump")

        if (self.assertEqual(resp["status_code"], 200)) is None \
           and (self.assertEqual(resp['results']['success_message'], "success")) is None:
             print("Jump Pass")
             print("resp: {0}".format(resp))
        else :
            print("Positive Test Failed")
            assert False

        print("Jump Pass")

    def test_negative_2_missing_filenumber(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        #filenumber = resp['results']['filenumber']
        direction = "forward"
        units = "words"
        length_of_jump = 10

        user = {"direction": direction, "units": units,
                "length_of_jump": length_of_jump}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "jump")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "Missing Parameter filenumber.") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else :
            print ("Negative Test Failed")
            assert False

    def test_negative_3_missing_direction(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']

        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        #request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        #file_format = "wav"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        token = resp["cookies"][0]
        resp = Renderer_Functions.send_request(cookies, user, "say")

        #Parameters
        filenumber = resp['results']['filenumber']
        direction = "forward"
        units = "words"
        length_of_jump = 10

        user = {"filenumber": filenumber, "units": units,
                "length_of_jump": length_of_jump}
        resp = Renderer_Functions.send_request(cookies, user, "jump")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "Missing Parameter direction.") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else :
            print ("Negative Test Failed")
            assert False

    def test_negative_4_missing_units(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']

        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        #request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        #file_format = "wav"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        token = resp["cookies"][0]
        resp = Renderer_Functions.send_request(cookies, user, "say")
        #Parameters
        filenumber = resp['results']['filenumber']
        direction = "forward"
        units = "words"
        length_of_jump = 10

        user = {"filenumber": filenumber, "direction": direction,
                "length_of_jump": length_of_jump}
        resp = Renderer_Functions.send_request(cookies, user, "jump")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "Missing Parameter units.") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else :
            print ("Negative Test Failed")
            assert False

    def test_negative_5_missing_length_of_jump(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']

        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        #request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        #file_format = "wav"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        token = resp["cookies"][0]
        resp = Renderer_Functions.send_request(cookies, user, "say")
        #Parameters
        filenumber = resp['results']['filenumber']
        direction = "forward"
        units = "words"
        length_of_jump = 10

        user = {"filenumber": filenumber, "direction": direction,
                "units": units}
        resp = Renderer_Functions.send_request(cookies, user, "jump")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "Missing Parameter length_of_jump.") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else :
            print ("Negative Test Failed")
            assert False

    def test_negative_6_incorrect_filenumber(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        filenumber = "pzsjdfkisa"
        direction = "forward"
        units = "words"
        length_of_jump = 10

        user = {"filenumber": filenumber, "direction": direction, "units": units,
                "length_of_jump": length_of_jump}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "jump")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "Incorrect value for filenumber.") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else :
            print ("Negative Test Failed")
            assert False

    def test_negative_7_incorrect_direction(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']

        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        #request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        #file_format = "wav"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        token = resp["cookies"][0]
        resp = Renderer_Functions.send_request(cookies, user, "say")
        #Parameters
        filenumber = resp['results']['filenumber']
        direction = 123456789
        units = "words"
        length_of_jump = 10

        user = {"filenumber": filenumber, "direction": direction,
                "units": units, "length_of_jump": length_of_jump}
        resp = Renderer_Functions.send_request(cookies, user, "jump")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "Incorrect value for direction.") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else :
            print ("Negative Test Failed")
            assert False

    def test_negative_8_incorrect_units(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']

        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        #request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        #file_format = "wav"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        token = resp["cookies"][0]
        resp = Renderer_Functions.send_request(cookies, user, "say")
        #Parameters
        filenumber = resp['results']['filenumber']
        direction = "forward"
        units = 123456789
        length_of_jump = 10

        user = {"filenumber": filenumber, "direction": direction,
                "units": units,"length_of_jump":length_of_jump}
        resp = Renderer_Functions.send_request(cookies, user, "jump")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "Incorrect value for units.") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else :
            print ("Negative Test Failed")
            assert False

    def test_negative_9_incorrect_length_of_jump(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']

        text_ssml = '<?xml version="1.0" encoding="UTF-8"?><speak xml:lang="0" xml:age="3" xml:voice="72057594037928282" xml:mood="4" xml:style="72057594037927938" '+\
                    'xml:volume="100" xml:pitch="100" xml:rate="100" xml:accent="default" xml:domain="72057594037927948" version="1.0">Where can we go to eat that '+\
                    'is open on a holiday?</speak>'
        # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
        cursor = Postgres.connect_to_db()
        # query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true"
        query = "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_voice_id = int(random.choice(db_info)[0])
        request_id = "654334"
        #request_id = "123456"
        # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        #file_format = "wav"
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        user = {"smi_voice_id": smi_voice_id, "request_id": request_id, "smi_domain_id": smi_domain_id,
                "text_ssml": text_ssml, "project_id": random_project}
        token = resp["cookies"][0]
        resp = Renderer_Functions.send_request(cookies, user, "say")
        #Parameters
        filenumber = resp['results']['filenumber']
        direction = "forward"
        units = "words"
        length_of_jump = "nbnfbidsask"

        user = {"filenumber": filenumber, "direction": direction,
                "units": units,"length_of_jump":length_of_jump}
        resp = Renderer_Functions.send_request(cookies, user, "jump")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], "Incorrect value for length_of_jump.") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["results"]["error_message"]))

        # Check error type
        else :
            print ("Negative Test Failed")
            assert False

    def test_negative_10_wrong_login(self):
        # Parameters
        filenumber = "71234569"
        direction = "forward"
        units = "words"
        length_of_jump = 10

        user = {"filenumber": filenumber, "direction": direction,
                "units": units, "length_of_jump": length_of_jump}
        resp = Renderer_Functions.send_request("123456789", user, "jump")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else :
            print ("Negative Test Failed")
            assert False

    def test_negative_11_missing_login(self):
        # Parameters
        filenumber = "71234569"
        direction = "forward"
        units = "words"
        length_of_jump = 10

        user = {"filenumber": filenumber, "direction": direction,
                "units": units, "length_of_jump": length_of_jump}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "jump")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else :
            print ("Negative Test Failed")
            assert False

    def test_negative_12_expired_token(self):
        # Parameters
        filenumber = "71234569"
        direction = "forward"
        units = "words"
        length_of_jump = 10

        user = {"filenumber": filenumber, "direction": direction,
                "units": units, "length_of_jump": length_of_jump}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "jump")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else :
            print ("Negative Test Failed")
            assert False
                    

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)