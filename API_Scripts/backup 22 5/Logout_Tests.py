import unittest
import Renderer_Functions

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBLogoutSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_logout(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "logout")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and (self.assertEqual(resp["results"]["success_message"], "logout success") is None) :
                # and (self.assertEqual(content_resp["details"], "") is None):
            print("Logout Pass")
            print("resp: {0}".format(resp))
            print("status_code: {0}".format(resp["status_code"]))
            print("details: {0}".format(resp["results"]["success_message"]))
        else:
            print("Positive Test Failed")
            assert False

    print("Account List Pass")

    def test_negative_2_logout_wrong_login(self):
        # Parameters
        user = {}
        resp = Renderer_Functions.send_request("123456789", user, "logout")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_3_logout_missing_login(self):
        # Parameters
        user = {}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "logout")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_4_expired_token(self):
        # Parameters
        user = {}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "logout")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)