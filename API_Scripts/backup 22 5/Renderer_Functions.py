import urllib.request as urllibr
import urllib.error
import json
import requests
from socket import timeout
import re
import configparser
import logging
import SMI_API as SMI_API
import urllib.error as urllibe
from Database import Postgres

# baseUrl = "http://50.197.139.145:8280"
# baseUrl = "http://ec2-52-53-250-125.us-west-1.compute.amazonaws.com:8080/"
# baseUrl = "http://10.1.10.131:8080"
# dbUrl = "10.1.10.131"
# baseUrl = "http://10.1.10.34:8080"
# dbUrl = "10.1.10.34"
#baseUrl = "http://smorph-demo-1608781781.us-east-1.elb.amazonaws.com"
#dbUrl = "smorph-demo-gpu.clx3qujijubm.us-east-1.rds.amazonaws.com"
# baseUrl = "ec2-54-67-28-43.us-west-1.compute.amazonaws.com"
# baseUrl = "http://smorph-demo-1608781781.us-east-1.elb.amazonaws.com/RenderingApp/"
# baseUrl = "https://smorph-say.speechmorphing.com:8443"
# baseUrl = "http://smorph-dev.speechmorphing.com:9090" #feedback
boundary = ""

config = configparser.ConfigParser()
config.sections()
config.read('api_config.ini')


def login(user):
    user_json = json.dumps(user).encode("utf-8")

    # create request
    print(config["SERVER"]["url"])
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "login")
    request = urllibr.Request(url)

    # add header to request
    headers = {"Content-Type": "application/json"}
    for key, value in headers.items():
        request.add_header(key, value)

    # t = api_request(user_json, request, url, headers)

    # post json data
    response = requests.post(url, user_json, headers)
    if response is None:
        print("*****No response from API*****")
    else:
        # print(response.content.decode("utf-8","strict"))
        # content_json = json.loads(response.content.decode("utf-8","strict"))
        content_json = response.json()
        print ("response.status_code {0}".format(response.status_code))

    print(response.status_code)

    if response.status_code == 200:
        try:
            resp = {}
            resp["status_code"] = response.status_code
            resp["content"] = content_json
            # print(content_json)
            resp["cookies"] = response.cookies.values()
            return resp
        except:
            resp = {}
            resp["status_code"] = response.status_code
            resp["error_code"] = content_json["results"]["error code"]
            resp["details"] = content_json["results"]["error message"]
            return resp
    else:
        resp = {}
        resp["status_code"] = response.status_code
        # print(content_json["results"])
        resp["details"] = content_json["results"]["error message"]
        return resp

        '''
        if response.status_code == 200:
            resp = {}
            resp["status_code"] = response.status_code
            resp["content"] = content_json
            resp["cookies"] = response.cookies.values()
            return resp

        else:
            resp = {}
            resp["status_code"] = response.status_code
            resp["error_code"] = content_json["Results"]["Error Code"]
            resp["details"] = content_json["Results"]["Error Message"]
            return resp
        '''

def initiate_login():

    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    # Get valid username and password
    email = config["LOGIN"]["email"]
    password = config["LOGIN"]["password"]
    user = {"email": email, "password": password}
    resp = login(user)

    if resp["status_code"] == 200:
        try:
            email_address = resp["content"]["results"]["user"]["email_address"]
            assert resp["status_code"] == 200
            assert email_address == email
            # if (self.assertEqual(resp["status_code"], 200)) is None \
            #         and (self.assertEqual(email_address, email) is None):
            print("Login Pass")
            print("Token: {0}".format(resp["cookies"]))
            return resp

        except urllibe.URLError as e:  # if response code is 200 but the fetched email is
            # different than the requested
            print("-|-Result:Test case Failed")
            print("Reason: {0}".format(e.reason))

    # Check error type
    elif resp["status_code"] != 200:
        print("Login Failed")
        print("Status Code: {0}".format(resp["status_code"]))
        print("Reason: {0}".format(resp["details"]))

def initiate_login_admin():

    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    # Get valid username and password
    email = config["LOGINADMIN"]["email"]
    password = config["LOGINADMIN"]["password"]
    user = {"email": email, "password": password}
    resp = login(user)

    if resp["status_code"] == 200:
        try:
            email_address = resp["content"]["results"]["user"]["email_address"]
            assert resp["status_code"] == 200
            assert email_address == email
            # if (self.assertEqual(resp["status_code"], 200)) is None \
            #         and (self.assertEqual(email_address, email) is None):
            print("Login Pass")
            print("Token: {0}".format(resp["cookies"]))
            return resp

        except urllibe.URLError as e:  # if response code is 200 but the fetched email is
            # different than the requested
            print("-|-Result:Test case Failed")
            print("Reason: {0}".format(e.reason))

    # Check error type
    elif resp["status_code"] != 200:
        print("Login Failed")
        print("Status Code: {0}".format(resp["status_code"]))
        print("Reason: {0}".format(resp["details"]))

def signup(user):

    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "signup")
    request = urllibr.Request(url)
    
    # add header to requests
    headers = {"Content-Type": "application/json"}
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def send_request(token, user, service):

    # if service is "projectsList":
    #     user_json = json.dumps(user)
    # else:
    user_json = json.dumps(user).encode("utf-8")

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], service)
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)
    
    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def svp(token):
    user_json = {}

    # create request
    url = SMI_API.endpoint_switch(config["SERVER"]["url"], "svp")
    request = urllibr.Request(url)

    # add header to request, also add or not a token within
    headers = SMI_API.generate_header(token)

    for key, value in headers.items():
        request.add_header(key, value)

    # function to send request and get response
    t = api_request(user_json, request, url, headers)
    return t


def api_request(user_json, request, url, headers):
    # logger = init_logger()
    try:
        print("Getting response...")
        response = urllibr.urlopen(request, user_json, timeout=30)
        print("*******************")
        geronimo = response.read().decode("utf-8",'strict')
        print(geronimo)
        print("*******************")
        try:
            resp = json.loads(geronimo)
        except:
            print("Parsing Error: JSON failed to load")
            # logger.error("Parsing Error: JSON failed to load")
        #resp = null
        print("*******************")
        # this below "try" code is only for signup tests
        try:
            if resp["status code"] == 400:
                return resp
            
        except:
                
            resp["status_code"] = response.code
            #print('HTTPError: {}'.format(response.code))
            return resp

        
    except requests.exceptions.ConnectionError as disconnect:
        # logger.error ('Server Down', disconnect)
        print(('Server Down', disconnect))
        resp = None
        return resp

    except (urllib.error.HTTPError, urllib.error.URLError) as error:
        # logger.error ('Data not retrieved because of: %s', error)
        print('Data not retrieved because of: %s', error)
        resp = {}
        code = str(error).split(" ")
        regex = re.compile('[^0-9]')
        print(code)
        sc = regex.sub('', code[len(code)- 2])
        resp["status_code"] = sc
        resp["details"] = 'Unauthorized Access'
        return resp

    except timeout:
        # logger.error ('socket timed out - URL')
        print ('socket timed out - URL')
        resp = None
        return resp

    except:  # if the answer above is different than 200 then use the second method in order to get the reason
        response = requests.post(url, user_json, headers)
        print(response)
        resp = {}
        resp["status_code"] = response.status_code
        print("Response Code: " + str(resp["status_code"]))
        if response.status_code != 404:
            content_json = json.loads(response.content)
            
            try:
                resp["details"] = content_json["results"]["error_message"]
                # logger.error (resp["details"])
                print(resp["details"])
            except:  # the tests that uses sendUnsupported have the error in differnt path
                resp["details"] = content_json["Error"]
                # logger.error(resp["details"])
                print(resp["details"])
        return resp

def compare_two_values_equal(side1, side2):
    # Check first that side 2 with is from the DB is not None, if it is then replace it with 0

    side2 = '0' if side2 == 'None' else str(side2)

    # Compare
    if str(side1) == str(side2):
        return True
    else:
        return False

def init_logger(file_name):
    # logger = logging.getLogger(name)
    # logger.setLevel(logging.DEBUG)
    #
    # logs_folder = 'logs'
    # if not os.path.isdir(path):
    #     os.makedirs(path)
    #
    # logging_name = 'logs/results.log'
    #
    # # create file handler which logs debug messages
    # fh = logging.FileHandler(logging_name)
    # fh.setLevel(logging.DEBUG)
    #
    # # create console handler with a higher log level
    # ch = logging.StreamHandler()
    # ch.setLevel(logging.DEBUG)
    #
    # # create formatter and add it to the handlers
    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # fh.setFormatter(formatter)
    # ch.setFormatter(formatter)
    #
    # # add the handlers to the logger
    # logger.addHandler(fh)
    # logger.addHandler(ch)
    #
    # return logger
    # create logger for logging purposes

    lgr = logging.getLogger(file_name)
    lgr.setLevel(logging.DEBUG)
    # add a file handler
    fh = logging.FileHandler('logs\\' + file_name)
    fh.setLevel(logging.DEBUG)
    # create a formatter and set the formatter for the handler.
    frmt = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    fh.setFormatter(frmt)
    # add the Handler to the logger
    lgr.addHandler(fh)
    return lgr

# Get allowed accounts
def get_allowed_accounts(email):
    cursor = Postgres.connect_to_db()

    query = "SELECT account_id FROM user_account_permissions " \
            + "INNER JOIN account USING(account_id) " \
            + "INNER JOIN user_permissions ap USING(user_permission_id) " \
            + "WHERE(user_email ILIKE '" + email + "')"


    #-----------------------------

    # query = "SELECT   distinct A.account_name, A.account_id, A.active, A.account_owner_user_email, B.user_permission_id " \
    # "FROM account A, user_account_permissions B   where A. account_id = B.account_id   and A.account_owner_user_email " \
    # "= B.user_email AND B.user_email ILIKE trim ('" + email + "')  ORDER BY account_name "

    cursor.execute(query)
    db_info = [r for r in cursor]

    return db_info

def get_allowed_projects_in_allowed_accounts(email):
    cursor = Postgres.connect_to_db()
    allowed_accounts = get_allowed_accounts(email)
    allowed_accounts_list = [i[0] for i in allowed_accounts]
    str = "("
    for i in allowed_accounts_list:
        # str += "'" + i.replace("'", "''") + "', "
        str += "'" + i + "', "
    str = str[:-2]
    str += ")"
    
    query = "SELECT project_id FROM user_project_permissions upp " \
            + "INNER JOIN user_permissions pp ON(upp.user_permission_id = pp.user_permission_id) " \
            + "INNER JOIN project p USING(project_id) " \
            + "INNER JOIN user_account_permissions uap USING(account_id, user_email) " \
            + "INNER JOIN user_permissions ap ON(uap.user_permission_id = ap.user_permission_id) " \
            + "INNER JOIN account a USING(account_id) " \
            + "WHERE(upp.user_email ILIKE '" + email + "') and account_id in " + str

    cursor.execute(query)
    db_info = [r for r in cursor]
    return db_info

def get_allowed_domains_in_allowed_projects_in_allowed_accounts(email, language_id):
    cursor = Postgres.connect_to_db()

    # Get allowed accounts
    allowed_projects = get_allowed_projects_in_allowed_accounts(email)
    allowed_projects_list = [i[0] for i in allowed_projects]
    str = "("
    for i in allowed_projects_list:
        str += "'" + i + "', "
    str = str[:-2]
    str += ")"

    # Get allowed projects
    query = "SELECT smi_domain_id FROM user_project_permissions upp " \
            + "INNER JOIN user_permissions pp ON(upp.user_permission_id = pp.user_permission_id) " \
            + "INNER JOIN project p USING(project_id) " \
            + "INNER JOIN user_account_permissions uap USING(account_id, user_email) " \
            + "INNER JOIN user_permissions ap ON(uap.user_permission_id = ap.user_permission_id) " \
            + "INNER JOIN account a USING(account_id) INNER JOIN smi_domain ON(project_id=smi_domain_owner_project_id) " \
            + "WHERE(upp.user_email ILIKE '" + email + "') AND supported and language_id = " + repr(language_id) + " and project_id in " + str

    cursor.execute(query)
    db_info = [r for r in cursor]
    return db_info

def get_allowed_domains_in_specific_project(email, language_id, project_id):

    cursor = Postgres.connect_to_db()

    # Get allowed projects
    query = "SELECT smi_domain_id FROM user_project_permissions upp " \
            + "INNER JOIN user_permissions pp ON(upp.user_permission_id = pp.user_permission_id) " \
            + "INNER JOIN project p USING(project_id) " \
            + "INNER JOIN user_account_permissions uap USING(account_id, user_email) " \
            + "INNER JOIN user_permissions ap ON(uap.user_permission_id = ap.user_permission_id) " \
            + "INNER JOIN account a USING(account_id) INNER JOIN smi_domain ON(project_id=smi_domain_owner_project_id) " \
            + "WHERE(upp.user_email ILIKE '" + email + "') AND supported and language_id = " + repr(language_id) + " and project_id ilike '" + project_id + "'"

    cursor.execute(query)
    db_info = [r for r in cursor]
    return db_info

def get_allowed_projects_that_contains_allowed_domains(email, language_id):
    # Get allowed domains
    allowed_domains = get_allowed_domains_in_allowed_projects_in_allowed_accounts(email, language_id)
    str = "("
    for i in allowed_domains:
        str += "'" + repr(i[0]) + "', "
    str = str[:-2]
    str += ")"

    # Get the projects from the allowed domains
    cursor = Postgres.connect_to_db()
    query = "select smi_domain_owner_project_id from smi_domain where smi_domain_id in " + str
    cursor.execute(query)
    db_info = [r for r in cursor]
    db_info_list = [i[0] for i in db_info]

    # Check that the projects are allowed for the user
    allowed_projects = get_allowed_projects_in_allowed_accounts(email)
    allowed_projects_list = [i[0] for i in allowed_projects]
    final_project_list = set(db_info_list).intersection(allowed_projects_list)
    return list(final_project_list)

def get_permitted_voices_for_account_from_DB(email, language_id):

     # Get allowed accounts
    allowed_accounts = get_allowed_accounts(email);
    allowed_accounts_list = [i[0] for i in allowed_accounts]
    # random_account = random.choice(allowed_accounts)

    # Get voices that have permissions
    cursor = Postgres.connect_to_db()
    query = "SELECT smi_voice_id voice_key, 'tokenRead' token,  supported, queue_name, language_id::text, " \
    + "RTRIM(CASE WHEN open_to_public THEN 'ALL, ' ||  CASE WHEN TRIM(smi_voice_owner_account_id) = '' THEN 'SMI'  ELSE smi_voice_owner_account_id END || ', ' " \
    + "ELSE  CASE WHEN TRIM(smi_voice_owner_account_id) = '' THEN 'SMI'  " \
    + "ELSE smi_voice_owner_account_id END END ||  COALESCE(string_agg(account_id, ', '), ''), ', ') " \
    + "permissions_value FROM smi_voice LEFT OUTER JOIN smi_voice_permissions " \
    + "USING(smi_voice_id) WHERE smi_voice_id IN(select smi_voice_id from smi_voice WHERE supported is true and language_id=" + str(language_id) + " )  " \
    + "AND supported GROUP BY smi_voice_id, open_to_public";
    cursor.execute(query)
    # db_info = [r for r in cursor if r[5] in allowed_accounts]
    db_info = [r for r in cursor]

    # Get allowed voices in allowed accounts
    allowed_voices_in_allowed_accounts = []
    for allowed_voice in db_info:
        if allowed_voice[5] in allowed_accounts_list:
            allowed_voices_in_allowed_accounts.append(allowed_voice[0])

    return allowed_voices_in_allowed_accounts

    # # Get voices for the chosen account
    # permitted_voices_ids = []
    # for voice in db_info:
    #     if voice[5] is random_account[0]:
    #         permitted_voices_ids.append(voice[0])
    #
    # return permitted_voices_ids