import unittest
import Renderer_Functions
from Database import Postgres
import random
import configparser
import string

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBsetUserPermissionSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Read config file
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)


    def test_positive_1_setUserPermission(self):

        # Login and get token
        # resp = Renderer_Functions.initiate_login()
        resp = Renderer_Functions.initiate_login_admin()
        cookie = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']

        # Get account id
        # user = {}
        # resp = Renderer_Functions.send_request(cookie, user, "accountlist")
        # # Get account that have projects within from sql instead of API
        cursor = Postgres.connect_to_db()
        # email = self.config.get('LOGIN', 'email')
        # cursor.execute("SELECT distinct(account_id) FROM user_account_permissions where user_email like 'admin@speechmorphing.com' "
        #                "or user_email like '" + email + "'" + " in (select distinct account_id from project) "
        #                 "intersect (select distinct account_id from project)")
        # db_info = [r for r in cursor]
        # # random_line = randint(0, resp["count"] - 1)
        # # account_id = resp["results"][random_line]["account_id"]
        # random_account = random.choice(db_info)
        # random_account = 'S4398046511105'
        # account_id = random_account

        # Parameters
        permitted_projects = Renderer_Functions.get_allowed_projects_in_allowed_accounts(email)
        random_project = random.choice(permitted_projects)
        account = random_project[0].split("-")[0]

        # email = ''.join(random.choice(string.ascii_letters) for ii in range(6)) + '@speechmorphing.com'
        emails = ["nabil@speechmorphing.com", "hung@speechmorphing.com", "dillon@speechmorphing.com",
                  "ashraf@speechmorphing.com", "mahmud@speechmorphing.com"]
        email = random.choice(emails)
        permissions_vals = [11, 0, 13]
        account_permission = random.choice(permissions_vals)
        project_permissions = [{"project_id": random_project[0], "permission": account_permission}]
        email_permission_array = [{"user_email": email, "account_permission": account_permission, "project_permissions": project_permissions}]
        user = {"account_id": account, "email_permission_array": email_permission_array}
        # user = {'account_id': 'S4398046511105', 'email_permission_array': [
        #     {'email': 'YnIXQx@speechmorphing.com', 'account_permission': '13',
        #      'project_permissions': [{'project_id': 'S4398046511105-4398046511106', 'permission': '13'}]}]}
        # user = {
        #     "account_id": "C4398046511107",
        #     "email_permission_array": [{"user_email": "bharati@speechmorphing.com", "account_permission": 13,
        #                                 "project_permissions": [
        #                                     {"project_id": "C4398046511107-4398046511117", "permission": 12},
        #                                     {"project_id": "C4398046511107-4398046511108", "permission": 13}]}]
        # }

        resp = Renderer_Functions.send_request(cookie, user, "setUserPermission")

        # Fetch projects from the Database for the same random account id
        query = "select * from user_project_permissions where user_email ilike '" + email + "' and project_id ilike '" + random_project[0] + "'"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertIn("permission added for email", resp["results"]["success_message"]) is None) \
            and (self.assertIn(email, resp["results"]["success_message"]) is None) \
            and (self.assertIn(random_project[0], resp["results"]["success_message"]) is None) \
            and (self.assertIn(account, resp["results"]["success_message"]) is None) \
            and (self.assertEqual(db_info[0][0], email) is None) \
            and (self.assertEqual(db_info[0][1], random_project[0]) is None) \
            and (self.assertEqual(db_info[0][2], account_permission) is None):
                print("resp: {0}".format(resp))
        else:
            print("Positive Test Failed")
            assert False

    print("Set user permission Pass")

    def test_negative_2_setUserPermission_wrong_login(self):
        # Parameters
        email = self.config["LOGINADMIN"]["email"]
        permitted_projects = Renderer_Functions.get_allowed_projects_in_allowed_accounts(email)
        random_project = random.choice(permitted_projects)
        account = random_project[0].split("-")[0]
        emails = ["nabil@speechmorphing.com", "hung@speechmorphing.com", "dillon@speechmorphing.com",
                  "ashraf@speechmorphing.com", "mahmud@speechmorphing.com"]
        email = random.choice(emails)
        permissions_vals = [11, 0, 13]
        account_permission = random.choice(permissions_vals)
        project_permissions = [{"project_id": random_project[0], "permission": account_permission}]
        email_permission_array = [
            {"user_email": email, "account_permission": account_permission, "project_permissions": project_permissions}]
        user = {"account_id": account, "email_permission_array": email_permission_array}

        # user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("123456789", user, "setUserPermission")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], '200')) is None \
                and (self.assertEqual(resp["details"],
                                      "Unauthorized Access") is None):
            print ("-|-Result:Test case Pass")
            print ("resp: {0}".format(resp))
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_3_setUserPermission_missing_login(self):
        # Parameters
        email = self.config["LOGINADMIN"]["email"]
        permitted_projects = Renderer_Functions.get_allowed_projects_in_allowed_accounts(email)
        random_project = random.choice(permitted_projects)
        account = random_project[0].split("-")[0]
        emails = ["nabil@speechmorphing.com", "hung@speechmorphing.com", "dillon@speechmorphing.com",
                  "ashraf@speechmorphing.com", "mahmud@speechmorphing.com"]
        email = random.choice(emails)
        permissions_vals = [11, 0, 13]
        account_permission = random.choice(permissions_vals)
        project_permissions = [{"project_id": random_project[0], "permission": account_permission}]
        email_permission_array = [
            {"user_email": email, "account_permission": account_permission, "project_permissions": project_permissions}]
        user = {"account_id": account, "email_permission_array": email_permission_array}

        # user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "setUserPermission")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_4_setUserPermission_expired_token(self):
        # Parameters
        email = self.config["LOGINADMIN"]["email"]
        permitted_projects = Renderer_Functions.get_allowed_projects_in_allowed_accounts(email)
        random_project = random.choice(permitted_projects)
        account = random_project[0].split("-")[0]
        emails = ["nabil@speechmorphing.com", "hung@speechmorphing.com", "dillon@speechmorphing.com",
                  "ashraf@speechmorphing.com", "mahmud@speechmorphing.com"]
        email = random.choice(emails)
        permissions_vals = [11, 0, 13]
        account_permission = random.choice(permissions_vals)
        project_permissions = [{"project_id": random_project[0], "permission": account_permission}]
        email_permission_array = [
            {"user_email": email, "account_permission": account_permission, "project_permissions": project_permissions}]
        user = {"account_id": account, "email_permission_array": email_permission_array}

        # user = {"send_unsupported": True}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "setUserPermission")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_5_no_permission(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookie = resp["cookies"][0]
        email = resp['content']['results']['user']['email_address']

        # Parameters
        permitted_projects = Renderer_Functions.get_allowed_projects_in_allowed_accounts(email)
        random_project = random.choice(permitted_projects)
        account = random_project[0].split("-")[0]

        email = ''.join(random.choice(string.ascii_letters) for ii in range(6)) + '@speechmorphing.com'
        permissions_vals = [11, 0, 13]
        account_permission = random.choice(permissions_vals)
        project_permissions = [{"project_id": random_project[0], "permission": account_permission}]
        email_permission_array = [{"user_email": email, "account_permission": account_permission, "project_permissions": project_permissions}]
        user = {"account_id": account, "email_permission_array": email_permission_array}

        resp = Renderer_Functions.send_request(cookie, user, "setUserPermission")
        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp["results"]["error_message"], " user with " + email + " is not in UMS") is None) :
                print("resp: {0}".format(resp))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
