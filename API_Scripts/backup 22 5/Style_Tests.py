import unittest
import Renderer_Functions
from Database import Postgres
import random

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBStyleSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_style(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        cursor = Postgres.connect_to_db()
        # Choose domain that has styles
        query = "select smi_domain_id FROM smi_domain_allowed_styles where style_id in (select style_id FROM style where supported is TRUE) " \
                + "INTERSECT " \
                + "select DISTINCT smi_domain_id from smi_domain where open_to_public is TRUE AND language_id=10" \
                + " ORDER BY smi_domain_id"

        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_domain_id = random.choice(db_info)[0]

        user = {"smi_domain_id": smi_domain_id}

        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "style")
        count = resp['count']
        # Fetch the info from Database
        query = "WITH RECURSIVE parents(c_id, p_id, level) as (SELECT smi_domain_id, parent_id, 0 FROM " \
                "smi_domain WHERE smi_domain_id = " + str(smi_domain_id) + " and marked_for_deletion is " \
                "NULL UNION ALL  SELECT smi_domain_id, parent_id, level + 1  FROM smi_domain  " \
                "INNER JOIN parents ON (smi_domain_id = p_id)) SELECT smi_domain_id, domain_name, " \
                "innerQuery.style_id, style_name, color FROM smi_domain " \
                "INNER JOIN(SELECT smi_domain_id, als.style_id, level, rank() OVER(PARTITION " \
                "BY als.style_id ORDER BY level), style_name, color FROM smi_domain_allowed_styles als " \
                "INNER JOIN parents ON(smi_domain_id=c_id) INNER JOIN(SELECT style_id, style_name, color " \
                "FROM style WHERE supported) styles ON(als.style_id = styles.style_id) ) " \
                "innerQuery USING(smi_domain_id) WHERE rank = 1 ORDER BY 4 DESC, 2;"

        cursor.execute(query)

        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[2])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        # Convert the values in mood_id from str to int first
        for sub in resp["results"]:
            sub['style_id'] = int(sub['style_id'])
        resp = sorted(resp["results"], key=lambda k: k['style_id'])

        # here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        for x in range(count):
            print (resp[x]["style_name"], db_info[x][3])
            print (resp[x]["color"], db_info[x][4])
            print (resp[x]["style_id"], db_info[x][2])
            print (resp[x]["smi_domain_id"], db_info[x][0])
            if (self.assertEqual(resp_status_code, 200)) is None \
                    and (self.assertEqual(resp[x]["style_name"], db_info[x][3]) is None) \
                    and (self.assertEqual(resp[x]["color"], db_info[x][4]) is None) \
                    and (self.assertEqual(resp[x]["smi_domain_id"] , db_info[x][0]) is None) \
                    and (self.assertEqual(resp[x]["style_id"], db_info[x][2]) is None):
                print("Style Pass")
                print("resp: {0}".format(resp))
                print("Style_Id: {0}".format(resp[x]["style_id"]))
                print("Color: {0}".format(resp[x]["color"]))
                print("Name: {0}".format(resp[x]["style_name"]))
            else:
                print("Positive Test Failed")
                assert False

    print("Style Pass")

    def test_negative_2_style_missing_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "style")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "Missing Parameter smi_domain_id.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_3_incorrect_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {"smi_domain_id" : "teleport"}
        #user = {"smi_domain_id" : 12345678912345678}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "style")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
            and (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["error_message"], "Incorrect value for smi_domain_id.") is None):
                print("-|-Result:Test case Pass")
                print("resp: {0}".format(resp))
                print("Status Code: {0}".format(resp["status_code"]))
                print("Reason: {0}".format(resp['results']["error_message"]))

        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_4_style_wrong_login(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.send_request("123456789", user, "style")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_5_style_missing_login(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "style")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_6_expired_token(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "style")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)