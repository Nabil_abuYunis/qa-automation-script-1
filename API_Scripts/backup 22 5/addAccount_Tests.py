import unittest
import Renderer_Functions
from Database import Postgres
import random
import configparser
import string

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBaddAccountSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Read config file
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)


    def test_positive_1_addAccount(self):

        # Login and get token
        # resp = Renderer_Functions.initiate_login()
        resp = Renderer_Functions.initiate_login_admin()
        cookie = resp["cookies"][0]

        # # Get account id
        # # user = {}
        # # resp = Renderer_Functions.send_request(cookie, user, "accountlist")
        # # # Get account that have projects within from sql instead of API
        cursor = Postgres.connect_to_db()
        # # email = self.config.get('LOGIN', 'email')
        # # cursor.execute("SELECT distinct(account_id) FROM user_account_permissions where user_email like 'admin@speechmorphing.com' "
        # #                "or user_email like '" + email + "'" + " in (select distinct account_id from project) "
        # #                 "intersect (select distinct account_id from project)")
        # # db_info = [r for r in cursor]
        # # # random_line = randint(0, resp["count"] - 1)
        # # # account_id = resp["results"][random_line]["account_id"]
        # # random_account = random.choice(db_info)
        # random_account = 'S4398046511105'
        #
        # # Parameters
        # account_id = random_account

        newAccountName = ''.join(random.choice(string.ascii_letters) for ii in range(5))
        newAccountName = 'testAC_' + newAccountName
        account_types = [1, 2, 3]
        account_type_id = random.choice(account_types)

        user = {"account_name": newAccountName, "account_type_id": account_type_id}
        resp = Renderer_Functions.send_request(cookie, user, "addAccount")

        # USE THE QUERIES TO FETCH THE DB INFO IN ORDER TO COMPARE THE RESULTS
        query = "select * from account where account_name ilike '" + newAccountName + \
                "' and account_type_id = " + str(account_type_id)
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and (self.assertEqual(resp["results"][0]["account_name"], db_info[0][2]) is None) \
                and (self.assertEqual(resp["results"][0]["account_id"], db_info[0][0]) is None):
            print ("Add account Pass")
            print("resp: {0}".format(resp))
            print("account_name: {0}".format(resp["results"][0]["account_name"]))
            print("account_name: {0}".format(resp["results"][0]["account_id"]))
        else :
            print("Positive Test Failed")
            assert False
        print("Add account Pass")

    def test_negative_2_addAccount_wrong_login(self):
        # Parameters
        newAccountName = ''.join(random.choice(string.ascii_letters) for ii in range(5))
        newAccountName = 'testAC_' + newAccountName
        account_types = [1, 2, 3]
        account_type_id = random.choice(account_types)
        user = {"account_name": newAccountName, "account_type_id": account_type_id}
        resp = Renderer_Functions.send_request("123456789", user, "addAccount")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_3_addAccount_missing_login(self):
        # Parameters
        newAccountName = ''.join(random.choice(string.ascii_letters) for ii in range(5))
        newAccountName = 'testAC_' + newAccountName
        account_types = [1, 2, 3]
        account_type_id = random.choice(account_types)
        user = {"account_name": newAccountName, "account_type_id": account_type_id}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "addAccount")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_4_addAccount_expired_token(self):
        # Parameters
        newAccountName = ''.join(random.choice(string.ascii_letters) for ii in range(5))
        newAccountName = 'testAC_' + newAccountName
        account_types = [1, 2, 3]
        account_type_id = random.choice(account_types)
        user = {"account_name": newAccountName, "account_type_id": account_type_id}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "addAccount")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200 \
            and (self.assertEqual(resp["status_code"], '401')) is None \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))

        # Check error type
        else:
            print ("Negative Test Failed")
            assert False

    def test_negative_5_addAccount_no_permission(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookie = resp["cookies"][0]

        # # Get account id
        # # user = {}
        # # resp = Renderer_Functions.send_request(cookie, user, "accountlist")
        # # # Get account that have projects within from sql instead of API
        cursor = Postgres.connect_to_db()
        # # email = self.config.get('LOGIN', 'email')
        # # cursor.execute("SELECT distinct(account_id) FROM user_account_permissions where user_email like 'admin@speechmorphing.com' "
        # #                "or user_email like '" + email + "'" + " in (select distinct account_id from project) "
        # #                 "intersect (select distinct account_id from project)")
        # # db_info = [r for r in cursor]
        # # # random_line = randint(0, resp["count"] - 1)
        # # # account_id = resp["results"][random_line]["account_id"]
        # # random_account = random.choice(db_info)
        # random_account = 'S4398046511105'
        #
        # # Parameters
        # account_id = random_account

        newAccountName = ''.join(random.choice(string.ascii_letters) for ii in range(5))
        newAccountName = 'testAC_' + newAccountName
        account_types = [1, 2, 3]
        account_type_id = random.choice(account_types)

        user = {"account_name": newAccountName, "account_type_id": account_type_id}
        resp = Renderer_Functions.send_request(cookie, user, "addAccount")

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp["status_code"], 200)) is None \
                and (self.assertEqual(resp["results"]["error_message"], "you do not have permission to add account.") is None) :
            print ("Test Pass")
            print("resp: {0}".format(resp))
            print("error_message: {0}".format(resp["results"]["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
