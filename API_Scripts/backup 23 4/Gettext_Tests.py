import urllib.error as urllibe
import unittest
import Renderer_Functions
from random import randint
from Database import Postgres
import random

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBGettextSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_getext(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # parameters
        # fetch filename id from filename functions
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        template = "no"
        user = {"template": template, "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        count = resp2["results"][0]["count"]
        # Choose a random row to test
        random_line = randint(0, count - 1)
        filename_id = resp_extract[random_line]["filename_id"]

        # parameters
        user = {"filename_id": filename_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "text")
        # Fetch the info from Database
        cursor = Postgres.connect_to_db()
        cursor.execute("SELECT  renderer_text_id, name, text_ssml,open_to_public,template_id, is_template_org, text_owner_email, smi_domain_id  FROM renderer_text where renderer_text_id = " + filename_id + "")

        db_info = [r for r in cursor]
        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = resp["results"]

        is_temp_org = ""
        if db_info[0][5] == True:
            is_temp_org = "yes"
        else:
            is_temp_org = 'NULL'

        temp_id = ""
        if db_info[0][4] is None:
            temp_id = str(0)
        else:
            temp_id = str(db_info[0][4])

        perm = ""
        if db_info[0][3] == True:
            perm = 'true'
        else:
            perm = 'false'

        # here we will check if the voice detail is passed or failed
        if (self.assertEqual(resp_status_code, 200)) is None \
                and (self.assertEqual(resp["text_id"], str(db_info[0][0]))) is None \
                and (self.assertEqual(resp["text_ssml"],db_info[0][2])) is None \
                and (self.assertEqual(resp["name"], db_info[0][1])) is None \
                and (self.assertEqual(resp["template_id"], temp_id)) is None \
                and (self.assertEqual(resp["permission"], perm)) is None \
                and (self.assertEqual(resp["template_original"], is_temp_org)) is None:
            print("Get Voice Pass")
            print("resp: {0}".format(resp))
            print("status_code: {0}".format(resp_status_code))
            print("Text_Id: {0}".format(resp["text_id"]))
            print("Text_Ssml: {0}".format(resp["text_ssml"]))
            print("Name: {0}".format(resp["name"]))
            print("Permission: {0}".format(resp["permission"]))

    def test_negative_2_gettext_wrong_login(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # parameters
        # fetch filename id from filename functions
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        template = "NO"
        user = {"normalization_domain_id": 1, "template": template, "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]['list']
        filename_id = resp_extract[0]["filename_id"]

        # parameters
        user = {"filename_id": filename_id}
        resp = Renderer_Functions.send_request("123456789", user, "text")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_3_gettext_missing_login(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        email = resp['content']['results']['user']['email_address']

        # parameters
        # fetch filename id from filename functions
        language_id = 6
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)
        template = "NO"
        user = {"normalization_domain_id": 1, "template": template, "project_id": random_project}
        resp2 = Renderer_Functions.send_request(resp["cookies"][0], user, "filenames")
        resp_extract = resp2["results"][0]["list"]
        filename_id = resp_extract[0]["filename_id"]

        # parameters
        user = {"filename_id": filename_id}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "text")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_4_gettext_incorrect_value(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # parameters
        user = {"filename_id": "chef boyardee"}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "text")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Incorrect value for filename_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_5_gettext_missing_param(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # parameters
        user = {}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "text")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Missing Parameter filename_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_6_expired_token(self):
        # Parameters
        user = {"filename_id": "765489612"}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "text")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)