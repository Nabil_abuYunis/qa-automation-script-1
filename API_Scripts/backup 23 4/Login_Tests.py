import urllib.error as urllibe
import unittest
import json
import Renderer_Functions
import configparser
import random
import string

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBLoginSuites(unittest.TestCase):

    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}
    # Read config file
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print (self.currentResult)

    def test_positive_1_login_verify_user_can_login_with_valid_username_password(self):
        # parameters
        email = self.config.get('LOGIN', 'email')
        password = self.config.get('LOGIN', 'password')
        user = {"email": email, "password" : password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print ("-|-Result:Test case Pass")
                    # print ("resp: {0}".format(resp))
                    print ("User: {0}".format(resp["content"]["results"]["user"]["email_address"]))
                    print ("Token: {0}".format(resp["cookies"]))
                    print ("Status code: {0}".format(resp["status_code"]))

            except urllibe.URLError as e:   # if response code is 200 but the fetched email is
                                            # different than the requested
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("-|-Result:Test case Failed")
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))
            print ("Error Code: {0}".format(resp["error_code"]))

    def test_negative_2_login_verify_user_cant_login_with_invalid_email(self):
        # Parameters
        email = "hung@speechmorphing"
        password = self.config.get('LOGIN', 'password')
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        resp_json1 = json.loads(resp['content']["results"]["error message"])

        if resp['status_code'] == 200:
            try:
                if (self.assertEqual(resp_json1["status_code"], "InvalidEmailOrPassword") is None) \
                        and (self.assertEqual(resp_json1["details"], "email_address: invalid email address.") is None): #?
                    print ("-|-Result:Test case Pass")
                    print ("test_login_verify_user_cant_login_with_invalid_email")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp_json1["status_code"]))
                    print ("Reason: {0}".format(resp_json1["details"]))
                    print ("Error Code: {0}".format(resp["content"]["results"]["error message"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Negative Test Failed")

    def test_negative_3_login_verify_user_cant_login_with_invalid_password(self):
        # parameters
        email = self.config.get('LOGIN', 'email')
        password = "abc456"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        resp_json1 = json.loads(resp["content"]["results"]["error message"])

        if resp['status_code'] == 200:
            try:
                if (self.assertEqual(resp_json1["status_code"], "InvalidEmailOrPassword") is None) \
                    and (self.assertEqual(resp_json1["details"], "Incorrect Password.") is None):
                    print ("-|-Result:Test case Pass")
                    print ("test_login_verify_user_cant_login_with_invalid_email")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp_json1["status_code"]))
                    print ("Reason: {0}".format(resp_json1["details"]))
                    print ("Error Code: {0}".format(resp["content"]["results"]["error message"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Negative Test Failed")

    def test_negative_4_login_verify_user_cant_login_with_empty_password(self):
        # parameters
        email = self.config.get('LOGIN', 'email')
        password = ""
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['content']['results']["error message"], "Invalid Email or Pasword.") is None):
                    print ("-|-Result:Test case Pass")
                    print ("test_login_verify_user_cant_login_with_invalid_email")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp['content']['results']["error message"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Negative Test Failed")

    def test_negative_5_login_verify_user_cant_login_with_empty_email(self):
        # Parameters
        email = ""
        password = self.config.get('LOGIN', 'password')
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['content']['results']["error message"], "Invalid Email or Pasword.") is None):
                    print ("-|-Result:Test case Pass")
                    print ("test_login_verify_user_cant_login_with_invalid_email")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp['content']['results']["error message"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Negative Test Failed")

    def test_negative_6_missing_email(self):
        # Parameters
        password = self.config.get('LOGIN', 'password')
        user = {"password": password}
        resp = Renderer_Functions.login(user)
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp["content"]["results"]["error message"], "Invalid Email or Pasword.") is None):
                    print ("-|-Result:Test case Pass")
                    print ("test_login_verify_user_cant_login_with_invalid_email")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["content"]["results"]["error message"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Negative Test Failed")

    def test_negative_7_login_nonregistered_email(self):
        #Parameters
        email = ''.join(random.choice(string.ascii_letters) for ii in range(5)) + '@speechmorphing.com'
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)
        if resp["status_code"] == 200:
            error = resp['content']["results"]["error message"]
            error_json = json.loads(error)
            try:
                if (self.assertEqual(error_json["details"], "User not Found with details.") is None) \
                        and (self.assertEqual(error_json["status_code"], "InvalidEmailOrPassword") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Reason: {0}".format(error_json["details"]))
                    print ("Error Code: {0}".format(error_json["status_code"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Negative Test Failed")

    def test_negative_8_login_activate_account(self):
        #Parameters
        email = ''.join(random.choice(string.ascii_letters) for ii in range(5)) + '@speechmorphing.com'
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        email = resp['results']["user"]["email_address"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)
        if resp["status_code"] == 200:
            error = resp['content']["results"]["error message"]
            error_json = json.loads(error)
            try:
                if (self.assertEqual(error_json["details"], "Please activate your account.") is None) \
                        and (self.assertEqual(error_json["status_code"], "AccountNotActive") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(error_json["status_code"]))
                    print ("Reason: {0}".format(error_json["details"]))
                    print ("Error Code: {0}".format(resp["status_code"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Negative Test Failed")

    def test_negative_9_login_missing_password(self):
        # parameters
        email = self.config.get('LOGIN', 'email')
        user = {"email": email}
        resp = Renderer_Functions.login(user)
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["content"]["results"]["error message"],
                                              "Invalid Email or Pasword.") is None):
                    print("-|-Result:Test case Pass")
                    print("test_login_verify_user_cant_login_with_invalid_email")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["content"]["results"]["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
