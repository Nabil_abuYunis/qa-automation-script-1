import urllib.error as urllibe
import unittest
import Renderer_Functions
import random
import string
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')

class VBSignupSuites(unittest.TestCase):

    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}
    # Read config file
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_signup(self):
        #  Parameters
        email = ''.join(random.choice(string.ascii_letters ) for ii in range(6)) + '@speechmorphing.com'
        # password = "abc123"
        password = ''.join(random.choice(string.ascii_letters ) for ii in range(6))
        # username = "vghung"
        username = ''.join(random.choice(string.ascii_letters ) for ii in range(6))
        # last_name = "test123"
        last_name = ''.join(random.choice(string.ascii_letters ) for ii in range(6))
        # first_name = "test123"
        first_name = ''.join(random.choice(string.ascii_letters ) for ii in range(6))

        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["user"]["email_address"], email.lower()) is None) \
                        and (self.assertEqual(resp['results']["user"]["username"], username) is None) \
                        and (self.assertEqual(resp['results']["user"]["first_name"], first_name) is None) \
                        and (self.assertEqual(resp['results']["user"]["last_name"], last_name) is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))
                    print("email: {0}".format(resp['results']["user"]["email_address"]))
                    print("username: {0}".format(resp['results']["user"]["username"]))
                    print("first_name: {0}".format(resp['results']["user"]["first_name"]))
                    print("last_name: {0}".format(resp['results']["user"]["last_name"]))

            except urllibe.URLError as e:   # if response code is 200 but the fetched email is
                                            # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("-|-Result:Test case Failed")
            print("status_code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_2_signup_invalid_email(self):
        # Parameters
        email = ''.join(random.choice(string.ascii_letters  ) for ii in range(5))
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)
        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp['results']["error_message"], 'incorrect format for email.') is None) :
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))
                    print("details: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:   # if response code is 200 but the fetched email is
                                            # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("-|-Result:Test case Failed")
            print("status_code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_3_signup_missing_email(self):
        # Parameters
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp['results']["error_message"],
                                              "Missing Parameter email.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("-|-Result:Test case Failed")
            print("status_code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_4_signup_empty_email(self):
        # Parameters
        email = ""
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp['results']["error_message"],
                                              "Incorrect value for email.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("-|-Result:Test case Failed")
            print("status_code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_5_signup_invalid_password(self):
        # Parameters
        email = ''.join(random.choice(string.ascii_letters  ) for ii in range(5)) + '@speechmorphing.com'
        password = "abc"
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp['results']["error_message"], 'password too short.') is None) :
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))
                    print("error_message: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:   # if response code is 200 but the fetched email is
                                            # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("-|-Result:Test case Failed")
            print("status_code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_6_signup_missing_password(self):
        # Parameters
        email = ''.join(random.choice(string.ascii_letters) for ii in range(5)) + '@speechmorphing.com'
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp['results']["error_message"],
                                              "Missing Parameter password.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("-|-Result:Test case Failed")
            print("status_code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_7_signup_empty_password(self):
        # Parameters
        email = ''.join(random.choice(string.ascii_letters ) for ii in range(5)) + '@speechmorphing.com'
        password = ""
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp['results']["error_message"],
                                              "Incorrect value for password.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("-|-Result:Test case Failed")
            print("status_code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_8_signup_empty_username(self):
        # Parameters
        email = ''.join(random.choice(string.ascii_letters  ) for ii in range(5)) + '@speechmorphing.com'
        password = "abc123"
        username = ""
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp['results']["error_message"],
                                              "Incorrect value for username.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("-|-Result:Test case Failed")
            print("status_code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_9_signup_missing_username(self):
        # Parameters
        email = ''.join(random.choice(string.ascii_letters ) for ii in range(5)) + '@speechmorphing.com'
        password = "abc123"
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "password": password, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp['results']["error_message"],
                                              "Missing Parameter username.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("-|-Result:Test case Failed")
            print("status_code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_10_signup_empty_lastname(self):
        # Parameters
        email = ''.join(random.choice(string.ascii_letters ) for ii in range(5)) + '@speechmorphing.com'
        password = "abc123"
        username = "vghung"
        last_name = ""
        first_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp['results']["error_message"],
                                              "Incorrect value for last_name.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("-|-Result:Test case Failed")
            print("status_code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_11_signup_missing_lastname(self):
        # Parameters
        email = ''.join(random.choice(string.ascii_letters ) for ii in range(5)) + '@speechmorphing.com'
        password = "abc123"
        username = "vghung"
        first_name = "test123"
        user = {"email": email, "password": password, "username": username,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp['results']["error_message"],
                                              "Missing Parameter last_name.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("-|-Result:Test case Failed")
            print("status_code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_12_signup_empty_firstname(self):
        # Parameters
        email = ''.join(random.choice(string.ascii_letters ) for ii in range(5)) + '@speechmorphing.com'
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        first_name = ""
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp['results']["error_message"],
                                              "Incorrect value for first_name.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("-|-Result:Test case Failed")
            print("status_code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_13_signup_missing_firstname(self):
        # Parameters
        email = ''.join(random.choice(string.ascii_letters) for ii in range(5)) + '@speechmorphing.com'
        password = "abc123"
        username = "vghung"
        last_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name}
        resp = Renderer_Functions.signup(user)

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp['results']["error_message"],
                                              "Missing Parameter first_name.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("-|-Result:Test case Failed")
            print("status_code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_14_already_registered(self):
        # parameters
        email = self.config.get('LOGIN', 'email')
        password = self.config.get('LOGIN', 'password')
        username = "vghung"
        last_name = "test123"
        first_name = "test123"
        user = {"email": email, "password": password, "username": username, "last_name": last_name,
                "first_name": first_name, "middle_name": "koko", "country": "us"}
        # middle_name; nick_name; birth_date; default_language;
        # gender; city; state; country; avatar_image_filename; profile_image_filename
        resp = Renderer_Functions.signup(user)
        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp['results']["error_message"],
                                              'user already registered') is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("-|-Result:Test case Failed")
            print("status_code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
