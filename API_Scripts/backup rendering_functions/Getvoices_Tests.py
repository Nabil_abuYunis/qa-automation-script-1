import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from random import randint
import configparser
from Database import Postgres

class VBGetvoicesSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        print(self.currentResult)

    # without any optional parameters, we should get all the voices
    def test_positive_1_getvoices_all(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.get_voices(resp["cookies"][0], user)

        # Fetch the info from the Age table
        cursor = Postgres.connect_to_db()
        cursor.execute(
            "WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id) WHERE (open_to_public  OR smi_voice_owner_email = 'hung@speechmorphing.com' OR user_email ILIKE 'hung@speechmorphing.com') AND CASE WHEN (length('') = 0 OR trim(name) ~* '()' OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  WHERE keyword ~* '^()$')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, websupported.smi_voice_id  ORDER BY  (websupported.smi_voice_id is not NULL) DESC, name")

        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        #resp = sorted(resp["results"], key=lambda k: k['smi_voice_id'])

        # Choose a random row to test
        random_line = randint(0, int(resp['count']) - 1)
        print("-----")
        # here we will check if the request is passed or failed
        for x in db_info:
            #print(resp['results'][random_line]["smi_voice_id"] + "-" + str(x[0]))
            if resp['results'][random_line]["smi_voice_id"] == str(x[0]):
                print("Found")
                if (self.assertEqual(resp["status_code"], 200)) is None \
                    and (self.assertEqual(resp['results'][random_line]["smi_voice_id"], str(x[0])) is None) \
                    and (self.assertEqual(resp['results'][random_line]["name"], x[1]) is None):
                    print("Get Voice Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))

    # options: gender_id
    def test_positive_2_getvoices_gender_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        gender_id = 2
        send_unsupported = True

        # parameters
        user = {"gender_id": gender_id, "send_unsupported": send_unsupported}

        # Get logged in email
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        email_address = config["LOGIN"]["email"]

        cursor = Postgres.connect_to_db()
        queryAllVoices = "WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id) WHERE (open_to_public  OR smi_voice_owner_email = '" + email_address + "' OR user_email ILIKE '" + email_address + "') AND CASE WHEN (length('') = 0 OR trim(name) ~* '()' OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  WHERE keyword ~* '^()$')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, websupported.smi_voice_id  ORDER BY  (websupported.smi_voice_id is not NULL) DESC, name"
        cursor.execute(queryAllVoices)
        db_infoAll = [r for r in cursor]
        queryGender = "select distinct smi_voice_id from smi_voice where gender_id = " + str (gender_id)
        cursor.execute(queryGender)
        db_infoGender = [r for r in cursor]
        db_info = []
        if gender_id != 0:
            # db_info = [i for i in db_infoGender[0] if i in db_infoAll[0]]
            for i in db_infoGender:
                for j in db_infoAll:
                    # print ("i[0]: ", i[0], ", j[0]: ", j[0])
                    if i[0] == j[0]:
                        db_info.append(j)


        # db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        resp = Renderer_Functions.get_voices(resp["cookies"][0], user)
       # Reorder the API received rows
        resp_status_code = resp["status_code"]
        count = resp['count']
        resp = sorted(resp["results"], key=lambda k: k['smi_voice_id'])

        # Choose a random row to test
        # random_line = randint(0, int(resp['count']) - 1)

        # here we will check if the request is passed or failed
        for x in range(count - 1):
            #print(resp['results'][random_line]["smi_voice_id"] + "-" + str(x[0]))
            # if resp['results'][random_line]["smi_voice_id"] == str(x[0]):
            #     print("Found")
            print (resp[x]["smi_voice_id"], db_info[x][0])
            print (resp[x]["name"], db_info[x][1])
            if (self.assertEqual(resp_status_code, 200)) is None \
                and (self.assertEqual(resp[x]["smi_voice_id"], str(db_info[x][0])) is None) \
                and (self.assertEqual(resp[x]["name"], db_info[x][1]) is None):
                print("Get Voice Pass")
                print("resp: {0}".format(resp))
                print("status_code: {0}".format(resp_status_code))

    # options: age_id
    def test_positive_3_getvoices_age_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # parameters
        user = {"age_id": 5, "send_unsupported": True}

        cursor = Postgres.connect_to_db()
        cursor.execute("WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id) WHERE (open_to_public  OR smi_voice_owner_email = 'hung@speechmorphing.com' OR user_email ILIKE 'hung@speechmorphing.com') AND CASE WHEN (length('') = 0 OR trim(name) ~* '()' OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  WHERE keyword ~* '^()$')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, websupported.smi_voice_id  ORDER BY  (websupported.smi_voice_id is not NULL) DESC, name")

        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        resp = Renderer_Functions.get_voices(resp["cookies"][0], user)

        random_line = randint(0, int(resp['count']) - 1)
        random_line2 = randint(0, int(resp['count']) - 1)

        # here we will check if the request is passed or failed
        for x in db_info:
            #print(resp['results'][random_line]["smi_voice_id"] + "-" + str(x[0]))
            if resp['results'][random_line]["smi_voice_id"] == str(x[0]):
                print("Found")
                if (self.assertEqual(resp["status_code"], 200)) is None \
                    and (self.assertEqual(resp['results'][random_line]["smi_voice_id"], str(x[0])) is None) \
                    and (self.assertEqual(resp['results'][random_line]["name"], x[1]) is None):
                    print("Get Voice Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))
            if resp['results'][random_line2]["smi_voice_id"] == str(x[0]):
                print("Ping")
                if (self.assertEqual(resp["status_code"], 200)) is None \
                    and (self.assertEqual(resp['results'][random_line2]["smi_voice_id"], str(x[0])) is None) \
                    and (self.assertEqual(resp['results'][random_line2]["name"], x[1]) is None):
                    print("Get Voice Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))

    def test_positive_4_getvoices_language_id(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # parameters
        user = {"language_id": 6, "send_unsupported": True}

        cursor = Postgres.connect_to_db()
        cursor.execute("WITH webSupported as (SELECT DISTINCT smi_voice_id FROM canned_clip_website WHERE supported)SELECT DISTINCT smi_voice_id, name, supported, websupported.smi_voice_id is not NULL web_supported, string_agg(keyword, ', ')as keywords  FROM smi_voice  LEFT OUTER JOIN smi_voice_permissions USING (smi_voice_id)  LEFT OUTER JOIN smi_voice_keywords USING (smi_voice_id)  LEFT OUTER JOIN websupported using (smi_voice_id) WHERE (open_to_public  OR smi_voice_owner_email = 'hung@speechmorphing.com' OR user_email ILIKE 'hung@speechmorphing.com') AND CASE WHEN (length('') = 0 OR trim(name) ~* '()' OR smi_voice_id IN (SELECT smi_voice_id FROM smi_voice_keywords  WHERE keyword ~* '^()$')) THEN TRUE ELSE FALSE END  GROUP BY smi_voice_id, name, supported, websupported.smi_voice_id  ORDER BY  (websupported.smi_voice_id is not NULL) DESC, name")

        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        resp = Renderer_Functions.get_voices(resp["cookies"][0], user)

        random_line = randint(0, int(resp['count']) - 1)
        random_line2 = randint(0, int(resp['count']) - 1)

        # here we will check if the request is passed or failed
        for x in db_info:
            print(resp['results'][random_line]["smi_voice_id"] + "-" + str(x[0]))
            if resp['results'][random_line]["smi_voice_id"] == str(x[0]):
                print("Found")
                if (self.assertEqual(resp["status_code"], 200)) is None \
                    and (self.assertEqual(resp['results'][random_line]["smi_voice_id"], str(x[0])) is None) \
                    and (self.assertEqual(resp['results'][random_line]["name"], x[1]) is None):
                    print("Get Voice Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))
            if resp['results'][random_line2]["smi_voice_id"] == str(x[0]):
                print("Ping")
                if (self.assertEqual(resp["status_code"], 200)) is None \
                    and (self.assertEqual(resp['results'][random_line2]["smi_voice_id"], str(x[0])) is None) \
                    and (self.assertEqual(resp['results'][random_line2]["name"], x[1]) is None):
                    print("Get Voice Pass")
                    print("resp: {0}".format(resp))
                    print("status_code: {0}".format(resp["status_code"]))

    def test_negative_5_getvoices_missing_sendUnsupported(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # parameters
        user = {}
        resp = Renderer_Functions.get_voices(resp["cookies"][0], user)

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Missing Parameter send_unsupported.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_6_getvoices_wrong_login(self):
        # parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.get_voices("123456789", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_7_getvoices_missing_login(self):
        # parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.get_voices("NO TOKEN", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")


    def test_negative_8_incorrect_sendUnsupported(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # parameters
        user = {"send_unsupported" : "rules"}
        resp = Renderer_Functions.get_voices(resp["cookies"][0], user)

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "Incorrect value for send_unsupported.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_9_expired_token(self):
        # Parameters
        user = {"send_unsupported": True}
        resp = Renderer_Functions.get_voices("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)