import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from random import randint
from Database import Postgres
import random

class VBLingoSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        print (self.currentResult)

    def test_positive_1_lingo(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        cursor = Postgres.connect_to_db()
        cursor.execute(
            "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
        db_info = [r for r in cursor]
        smi_domain_id = int(random.choice(db_info)[0])
        user = {"smi_domain_id": smi_domain_id}

        resp = Renderer_Functions.lingo(resp["cookies"][0], user)
        print(resp)
        # Fetch the info from the Database
        cursor.execute('SELECT smi_domain_id, domain_name FROM smi_domain WHERE smi_domain_id = ' + str(user['smi_domain_id']))
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])
        print(db_info)
        # Choose a random row to test
        random_line = randint(0, resp['count']-1)

        # here we will check if the request is passed or failed
        if (self.assertEqual(resp['status_code'], 200)) is None \
                and (self.assertEqual(resp['results'][random_line]["smi_domain_id"], str(db_info[0][0])) is None) \
                and (self.assertEqual(resp['results'][random_line]["domain_name"], db_info[0][1]) is None):
            print ("Lingo Pass")
            print ("resp: {0}".format(resp))
            for x in resp['results']:
                print ("Lingo: {0}".format(x["lingo"]) + " | " + "Lingo_Id: {0}".format(x["lingo_id"]))
            print ("SMI_Domain_Id: {0}".format(resp['results'][random_line]["smi_domain_id"]))
            print ("Domain Name: {0}".format(resp['results'][random_line]["domain_name"]))

    def test_negative_2_missing_param(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {}
        resp = Renderer_Functions.lingo(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Missing Parameter smi_domain_id.") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_3_incorrect_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {"smi_domain_id": "hello"}
        resp = Renderer_Functions.lingo(resp["cookies"][0], user)
        print(resp)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Incorrect value for smi_domain_id.") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Negative Test Failed")

    def test_negative_4_wrong_login(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.lingo("123456789", user)
        print(resp)

        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

    def test_negative_7_domain_missing_login(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.lingo("NO TOKEN", user)
        print(resp)

        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

    def test_negative_8_expired_token(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.lingo("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)