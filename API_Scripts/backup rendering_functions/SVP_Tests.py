import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions

class VBSVPSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Start run test case: {0}".format(str(self.id())))
        print( "-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Completed running test case: {0}".format(str(self.id())))
        print( "-----------------------------------------------------------------------------------------------\n")
        print( self.currentResult)

    def test_positive_1_svp(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        resp = Renderer_Functions.svp(resp["cookies"][0])

        # Fetch the info from Database
        #cursor = Postgres.connect_to_db()
        #cursor.execute("SELECT RENDERER_PITCH_MAX, RENDERER_PITCH_MIN, RENDERER_SPEED_MAX, RENDERER_SPEED_MIN, RENDERER_VOLUME_MAX, RENDERER_VOLUME_MIN FROM configurationDB")
        #db_info = [r for r in cursor]
        #db_info = sorted(db_info, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        #resp = sorted(resp["results"], key=lambda k: k['gender_id'])
        print(resp)
        # here we will check if the request is passed or failed
        #and (self.assertEqual(resp[random_line]['img_url'], db_info[random_line][3]) is None) \
        if (self.assertEqual(resp_status_code, 200)) is None \
                and (self.assertEqual(resp['results'][0]['volume']['min'], '50') is None) \
                and (self.assertEqual(resp['results'][0]['volume']['max'], '300') is None) \
                and (self.assertEqual(resp['results'][0]['pitch']['min'], '50') is None) \
                and (self.assertEqual(resp['results'][0]['pitch']['max'], '300') is None) \
                and (self.assertEqual(resp['results'][0]['speed']['min'], '50') is None) \
                and (self.assertEqual(resp['results'][0]['speed']['max'], '300') is None):
            print( "Speed/Volume/Pitch Pass")
            print( "resp: {0}".format(resp))
            print( "Volume Max: {0}".format(resp['results'][0]['volume']['max']))
            print( "Volume Min: {0}".format(resp['results'][0]['volume']['min']))
            print( "Pitch Max: {0}".format(resp['results'][0]['pitch']['max']))
            print( "Pitch Min: {0}".format(resp['results'][0]['pitch']['min']))
            print( "Speed Max: {0}".format(resp['results'][0]['speed']['max']))
            print( "Speed Min: {0}".format(resp['results'][0]['speed']['min']))

    def test_negative_2_wrong_login(self):
        # Parameters
        resp = Renderer_Functions.svp("123456789")

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        #resp = sorted(resp["results"], key=lambda k: k['gender_id'])
        print(resp)
        # here we will check if the request is passed or failed
        #and (self.assertEqual(resp[random_line]['img_url'], db_info[random_line][3]) is None) \
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")


    def test_negative_3_missing_login(self):
        # Parameters
        resp = Renderer_Functions.svp("NO TOKEN")

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        #resp = sorted(resp["results"], key=lambda k: k['gender_id'])
        print(resp)
        # here we will check if the request is passed or failed
        #and (self.assertEqual(resp[random_line]['img_url'], db_info[random_line][3]) is None) \
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

    def test_negative_6_expired_token(self):
        resp = Renderer_Functions.svp("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24")

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")
            
if __name__ == '__main__':
    print( "-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)