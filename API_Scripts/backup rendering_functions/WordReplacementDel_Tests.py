import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from Database import Postgres
import random
import string

class VBWordReplacementDelSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        print(self.currentResult)

    def test_positive_1_word_replacement(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]

        # First add a new word - replacement
        cursor = Postgres.connect_to_db()
        # Select smi_domain_id and their gestures gestures
        query = "select smi_domain_id, gesture_id from smi_domain_allowed_gestures where gesture_id in (select gesture_id from gesture where supported AND open_to_public) " \
                + "AND smi_domain_id in (select smi_domain_id from smi_domain where supported AND open_to_public) ORDER BY smi_domain_id "
        cursor.execute(query)
        db_info = [r for r in cursor]
        # Get random smi_domain_id and it's gesture
        random_line = random.choice(db_info)

        smi_domain_id = random_line[0]
        gesture_id = random_line[1]

        # Parameters
        word = ''.join(random.choice(string.ascii_letters) for i in range(5))
        use_gesture = True

        user = {"smi_domain_id": smi_domain_id, "use_gesture": use_gesture, "word": word, "gesture_id": gesture_id}
        resp = Renderer_Functions.word_replacement_add(cookies, user)

        # ----------------------------
        # now delete the new added word

        #Parameters
        user = {"smi_domain_id": smi_domain_id, "word":word}
        resp = Renderer_Functions.word_replacement_del(cookies, user)

        if (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["success_message"], "word '"+word+"' is deleted.") is None):
            print("Word Replacement Delete Pass")
            print("resp: {0}".format(resp))
            print("status_code: {0}".format(resp["status_code"]))

    def test_negative_2_incorrect_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        user = {"smi_domain_id": "mistletoe", "word": "stop"}
        resp = Renderer_Functions.word_replacement_del(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Incorrect value for smi_domain_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_3_incorrect_word(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        user = {"smi_domain_id": 72057594037927948, "word": 123456789}
        resp = Renderer_Functions.word_replacement_del(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Incorrect value for word.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_3_missing_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        user = {"word":"world"}
        resp = Renderer_Functions.word_replacement_del(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Missing Parameter smi_domain_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_4_missing_word(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        user = {"smi_domain_id": 72057594037927948}
        resp = Renderer_Functions.word_replacement_del(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Missing Parameter word.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_6_del_wrong_login(self):
        # parameters
        word = "hello"
        user = {"smi_domain_id": 72057594037927948, "word":word}
        resp = Renderer_Functions.word_replacement_del("123456789", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_7_del_missing_login(self):
        # parameters
        word = "hello"
        user = {"smi_domain_id": 72057594037927948, "word":word}

        resp = Renderer_Functions.word_replacement_del("NO TOKEN", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_6_expired_token(self):
        # Parameters
        word = "hello"
        user = {"smi_domain_id": 72057594037927948, "word":word}
        resp = Renderer_Functions.word_replacement_del("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)