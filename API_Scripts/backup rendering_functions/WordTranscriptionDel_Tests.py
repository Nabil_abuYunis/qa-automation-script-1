import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from Database import Postgres
import random
import string

class VBWordTranscriptionDelSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        print(self.currentResult)

    def test_positive_1_word_transcription(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()
        cookies = resp["cookies"][0]

        # ----------------------------
        # First add new word - transcription
        # Parameters
        word = ''.join(random.choice(string.ascii_letters) for i in range(5))
        transcription = ''.join(random.choice(string.ascii_letters) for i in range(5))

        cursor = Postgres.connect_to_db()
        query = "select smi_domain_id from smi_domain where supported AND open_to_public"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_domain_id = random.choice(db_info)[0]

        user = {"smi_domain_id": smi_domain_id, "word": word, "transcription": transcription}
        Renderer_Functions.word_transcription_add(cookies, user)

        # ---------------------------
        # Now delete the added word - transcription
        #Parameters
        user = {"smi_domain_id": smi_domain_id, "word": word, "transcription": transcription}
        resp = Renderer_Functions.word_transcription_del(cookies, user)

        if (self.assertEqual(resp["status_code"], 200)) is None \
            and (self.assertEqual(resp['results']["success_message"], "word '" + word + "' is deleted.") is None):
            print("Word Transcription Delete Pass")
            print("resp: {0}".format(resp))
            print("status_code: {0}".format(resp["status_code"]))

    def test_negative_9_word_not_found (self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        cursor = Postgres.connect_to_db()
        query = "select smi_domain_id from smi_domain where supported AND open_to_public"
        cursor.execute(query)
        db_info = [r for r in cursor]
        smi_domain_id = random.choice(db_info)[0]
        word = "kjgfkjvgf"
        user = {"smi_domain_id": smi_domain_id, "word": word}
        resp = Renderer_Functions.word_transcription_del(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error_message"],
                                              "word not found") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error_message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_2_incorrect_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        word = "people"
        user = {"smi_domain_id": "shed", "word": word}
        resp = Renderer_Functions.word_transcription_del(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Incorrect value for smi_domain_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_3_incorrect_word(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        word = "people"
        user = {"smi_domain_id": "shed", "word": word}
        resp = Renderer_Functions.word_transcription_del(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Incorrect value for smi_domain_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_3_missing_domain(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        word = "people"
        user = {"word": word}
        resp = Renderer_Functions.word_transcription_del(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Missing Parameter smi_domain_id.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_4_missing_word(self):

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        #Parameters
        user = {"smi_domain_id": 72057594037927948}
        resp = Renderer_Functions.word_transcription_del(resp["cookies"][0], user)

        if resp["status_code"] == 200:
            try:
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["error message"],
                                              "Missing Parameter word.") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp['results']["error message"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Negative Test Failed")

    def test_negative_6_word_replacement_wrong_login(self):
        #Parameters
        word = "people"
        user = {"smi_domain_id": 72057594037927948, "word": word}
        resp = Renderer_Functions.word_transcription_del("123456789", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_7_word_replacement_missing_login(self):
        #Parameters
        word = "people"
        user = {"smi_domain_id": 72057594037927948, "word": word}
        resp = Renderer_Functions.word_transcription_del("NO TOKEN", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_8_expired_token(self):
        # Parameters
        word = "people"
        user = {"smi_domain_id": 72057594037927948, "word": word}
        resp = Renderer_Functions.word_transcription_del("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)