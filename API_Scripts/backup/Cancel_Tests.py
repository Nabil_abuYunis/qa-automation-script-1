import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from random import randint
import configparser
from Database import Postgres
import JSON_Request as Data_Utils
import random

class VBCancelSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        print(self.currentResult)


    def test_positive_1_cancel(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    # text_ssml = Data_Utils.get_rand_ssml_value(config['LOCATION']['ssml'])
                    # smi_voice_id = "72057594037928282"
                    # domain_id = "72057594037927948"
                    # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                    # domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                    # request_id = "31523"
                    # #request_id = "123456"
                    # #file_format = "wav"

                    text_ssml = 'testing a cancel input by API'
                    # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                    cursor = Postgres.connect_to_db()
                    cursor.execute(
                        "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true")
                    db_info = [r for r in cursor]
                    smi_voice_id = int(random.choice(db_info)[0])
                    request_id = "31523"
                    # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                    cursor.execute(
                        "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
                    db_info = [r for r in cursor]
                    smi_domain_id = int(random.choice(db_info)[0])

                    user = {"smi_voice_id": smi_voice_id, "request_id": str(request_id), "smi_domain_id": smi_domain_id,
                            "text_ssml": text_ssml}

                    print ("Request: {0}".format(user))
                    token = resp["cookies"][0]
                    resp = Renderer_Functions.say(token, user)

                    # Parameters
                    user = {"request_id" : request_id}
                    resp = Renderer_Functions.cancel(token, user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["success_message"],
                                                          "cancelled") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["success_message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_2_missing_request_id_param(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    # text_ssml = Data_Utils.get_rand_ssml_value(config['LOCATION']['ssml'])
                    # smi_voice_id = "72057594037928282"
                    # domain_id = "72057594037927948"
                    # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                    # domain_id = Data_Utils.get_element_id(text_ssml, "domain")

                    text_ssml = 'testing a cancel input by API'
                    # smi_voice_id = Data_Utils.get_element_id(text_ssml, "voice")
                    cursor = Postgres.connect_to_db()
                    cursor.execute(
                        "select Distinct smi_voice_id from smi_voice where language_id = 6 AND supported = true AND open_to_public = true")
                    db_info = [r for r in cursor]
                    smi_voice_id = int(random.choice(db_info)[0])
                    request_id = "31523"
                    # smi_domain_id = Data_Utils.get_element_id(text_ssml, "domain")
                    cursor.execute(
                        "select Distinct smi_domain_id from smi_domain where language_id = 6 AND supported = true AND open_to_public = true")
                    db_info = [r for r in cursor]
                    smi_domain_id = int(random.choice(db_info)[0])

                    # request_id = "31523"
                    #request_id = "123456"
                    #file_format = "wav"

                    user = {"smi_voice_id": smi_voice_id, "request_id": str(request_id), "smi_domain_id": smi_domain_id,
                            "text_ssml": text_ssml}
                    token = resp["cookies"][0]
                    print ("Request: {0}".format(user))
                    resp = Renderer_Functions.say(token, user)

                    # Parameters
                    user = {}
                    resp = Renderer_Functions.cancel(token, user)

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error_message"],
                                                          "Missing Parameter request_id.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error_message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_3_gesture_wrong_login(self):
        # Parameters
        user = {"request_id": "12345"}
        resp = Renderer_Functions.cancel("123456789", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_4_gesture_missing_login(self):
        # Parameters
        user = {"request_id": "12345"}
        resp = Renderer_Functions.cancel("NO TOKEN", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_6_expired_token(self):
        # Parameters
        user = {"request_id" : "7164"}
        resp = Renderer_Functions.cancel("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

    
if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
