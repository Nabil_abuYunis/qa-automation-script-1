import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
import string
from random import randint
import configparser
from Database import Postgres
import random

class VBFilenameSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Start run test case: {0}".format(str(self.id())))
        print( "-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print( "\n---------------------------------------------------------------------------------------------")
        print( "Completed running test case: {0}".format(str(self.id())))
        print( "-----------------------------------------------------------------------------------------------\n")
        print( self.currentResult)

    def test_positive_1_filename_template_yes(self):

        # valid username and password
        email = self.config["LOGIN"]["email"]
        password = self.config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print( "Login Pass")
                    print( "Token: {0}".format(resp["cookies"]))

                    # parameters
                    template = "yes"
                    user = {"template": template}
                    resp = Renderer_Functions.filename(resp["cookies"][0], user)

                    count = resp['results'][0]['count']
                    # Reorder the API received rows
                    resp_status_code = resp["status_code"]
                    resp = sorted(resp["results"][0]["list"], key=lambda k: k['filename_id'])

                    # Choose a random row to test
                    random_line = randint(0, count - 1)

                    # Fetch the info from Database
                    cursor = Postgres.connect_to_db()
                    cursor.execute("SELECT  renderer_text_id, name, text_ssml,open_to_public,template_id, is_template_org, text_owner_email, smi_domain_id  FROM renderer_text where renderer_text_id = " + resp[random_line]["filename_id"] + "")

                    db_info = [r for r in cursor]
                    db_info = sorted(db_info, key=lambda k: k[0])

                    is_temp_org = ""
                    if db_info[0][5] == True:
                        is_temp_org = "yes"
                    else:
                        is_temp_org = 'NULL'

                    temp_id = ""
                    if db_info[0][4] is None:
                        temp_id = str(0)
                    else:
                        temp_id = str(db_info[0][4])
                        
                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp_status_code, 200)) is None \
                            and (self.assertEqual(resp[random_line]["template_id"], temp_id) is None) \
                            and (self.assertEqual(resp[random_line]["template_original"], is_temp_org) is None) \
                            and (self.assertEqual(resp[random_line]["text_ssml"], db_info[0][2]) is None) \
                            and (self.assertEqual(resp[random_line]["smi_domain_id"], str(db_info[0][7])) is None) \
                            and (self.assertEqual(resp[random_line]["filename_id"], str(db_info[0][0])) is None) \
                            and (self.assertEqual(resp[random_line]["text_owner_email"], db_info[0][6]) is None) \
                            and (self.assertEqual(resp[random_line]["open_to_public"], db_info[0][3]) is None) \
                            and (self.assertEqual(resp[random_line]["name"], db_info[0][1]) is None):
                        print( "Filename Pass")
                        print( "resp: {0}".format(resp))

            except urllibe.URLError as e:
                # if response code is 200 but the fetched email is
                # different than the requested
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Login Failed")
            print( "Status Code: {0}".format(resp["status_code"]))
            print( "Reason: {0}".format(resp["details"]))

    def test_positive_6_filename_template_no(self):
        # valid username and password
        email = self.config["LOGIN"]["email"]
        password = self.config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print( "Login Pass")
                    print( "Token: {0}".format(resp["cookies"]))

                    # parameters
                    template = "no"
                    user = {"template": template}
                    resp = Renderer_Functions.filename(resp["cookies"][0], user)

                    count = resp['results'][0]['count']
                    # Reorder the API received rows
                    resp_status_code = resp["status_code"]
                    resp = sorted(resp["results"][0]["list"], key=lambda k: k['filename_id'])

                    # Choose a random row to test
                    random_line = randint(0, count - 1)

                    # Fetch the info from Database
                    cursor = Postgres.connect_to_db()
                    cursor.execute("SELECT  renderer_text_id, name, text_ssml,open_to_public,template_id, is_template_org, text_owner_email, smi_domain_id  FROM renderer_text where renderer_text_id = " + resp[random_line]["filename_id"] + "")

                    db_info = [r for r in cursor]
                    db_info = sorted(db_info, key=lambda k: k[0])

                    is_temp_org = ""
                    if db_info[0][5] == True:
                        is_temp_org = "yes"
                    else:
                        is_temp_org = 'NULL'

                    temp_id = ""
                    if db_info[0][4] is None:
                        temp_id = str(0)
                    else:
                        temp_id = str(db_info[0][4])
                        
                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp_status_code, 200)) is None \
                            and (self.assertEqual(resp[random_line]["template_id"], temp_id) is None) \
                            and (self.assertEqual(resp[random_line]["template_original"], is_temp_org) is None) \
                            and (self.assertEqual(resp[random_line]["text_ssml"], db_info[0][2]) is None) \
                            and (self.assertEqual(resp[random_line]["smi_domain_id"], str(db_info[0][7])) is None) \
                            and (self.assertEqual(resp[random_line]["filename_id"], str(db_info[0][0])) is None) \
                            and (self.assertEqual(resp[random_line]["text_owner_email"], db_info[0][6]) is None) \
                            and (self.assertEqual(resp[random_line]["open_to_public"], db_info[0][3]) is None) \
                            and (self.assertEqual(resp[random_line]["name"], db_info[0][1]) is None):
                        print( "Filename Pass")
                        print( "resp: {0}".format(resp))

            except urllibe.URLError as e:
                # if response code is 200 but the fetched email is
                # different than the requested
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Login Failed")
            print( "Status Code: {0}".format(resp["status_code"]))
            print( "Reason: {0}".format(resp["details"]))

    def test_positive_7_filename_template_both(self):
        # valid username and password
        email = self.config["LOGIN"]["email"]
        password = self.config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print( "Login Pass")
                    print( "Token: {0}".format(resp["cookies"]))

                    # parameters
                    template = "both"
                    user = {"template": template}
                    resp = Renderer_Functions.filename(resp["cookies"][0], user)

                    count = resp['results'][0]['count']
                    # Reorder the API received rows
                    resp_status_code = resp["status_code"]
                    resp = sorted(resp["results"][0]["list"], key=lambda k: k['filename_id'])

                    # Choose a random row to test
                    random_line = randint(0, count - 1)

                    # Fetch the info from Database
                    cursor = Postgres.connect_to_db()
                    cursor.execute("SELECT  renderer_text_id, name, text_ssml,open_to_public,template_id, is_template_org, text_owner_email, smi_domain_id  FROM renderer_text where renderer_text_id = " + resp[random_line]["filename_id"] + "")

                    db_info = [r for r in cursor]
                    db_info = sorted(db_info, key=lambda k: k[0])

                    is_temp_org = ""
                    if db_info[0][5] == True:
                        is_temp_org = "yes"
                    else:
                        is_temp_org = 'NULL'

                    temp_id = ""
                    if db_info[0][4] is None:
                        temp_id = str(0)
                    else:
                        temp_id = str(db_info[0][4])
                        
                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp_status_code, 200)) is None \
                            and (self.assertEqual(resp[random_line]["template_id"], temp_id) is None) \
                            and (self.assertEqual(resp[random_line]["template_original"], is_temp_org) is None) \
                            and (self.assertEqual(resp[random_line]["text_ssml"], db_info[0][2]) is None) \
                            and (self.assertEqual(resp[random_line]["smi_domain_id"], str(db_info[0][7])) is None) \
                            and (self.assertEqual(resp[random_line]["filename_id"], str(db_info[0][0])) is None) \
                            and (self.assertEqual(resp[random_line]["text_owner_email"], db_info[0][6]) is None) \
                            and (self.assertEqual(resp[random_line]["open_to_public"], db_info[0][3]) is None) \
                            and (self.assertEqual(resp[random_line]["name"], db_info[0][1]) is None):
                        print( "Filename Pass")
                        print( "resp: {0}".format(resp))

            except urllibe.URLError as e:
                # if response code is 200 but the fetched email is
                # different than the requested
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Login Failed")
            print( "Status Code: {0}".format(resp["status_code"]))
            print( "Reason: {0}".format(resp["details"]))

    def test_positive_8_domain_id(self):
        # valid username and password
        email = self.config["LOGIN"]["email"]
        password = self.config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)
        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print( "Login Pass")
                    print( "Token: {0}".format(resp["cookies"]))

                    # parameters
                    template = "yes"
                    # get domains id's that have templates
                    # smi_domain_id = "72057594037927948"
                    cursor = Postgres.connect_to_db()
                    cursor.execute("SELECT Distinct smi_domain_id  FROM renderer_text where is_template_org = true "
                                   + "intersect "
                                   + "select Distinct smi_domain_id from smi_domain where language_id = 6")
                    db_info_domains = [r for r in cursor]
                    smi_domain_id = random.choice(db_info_domains)
                    print("Chosen random domain: ", smi_domain_id[0])
                    # user = {"template": template, "smi_domain_id": str(72057594037927948)}
                    user = {"template": template, "smi_domain_id": str(smi_domain_id[0])}
                    resp = Renderer_Functions.filename(resp["cookies"][0], user)

                    count = resp['results'][0]['count']
                    # Reorder the API received rows
                    resp_status_code = resp["status_code"]
                    resp = sorted(resp["results"][0]["list"], key=lambda k: k['filename_id'])

                    # Choose a random row to test
                    random_line = randint(0, count - 1)

                    # Fetch the info from Database
                    cursor = Postgres.connect_to_db()
                    cursor.execute("SELECT renderer_text_id, name, text_ssml, open_to_public, template_id, is_template_org, text_owner_email, smi_domain_id  FROM renderer_text where renderer_text_id = " + resp[random_line]["filename_id"] + "")

                    db_info = [r for r in cursor]
                    db_info = sorted(db_info, key=lambda k: k[0])

                    is_temp_org = ""
                    if db_info[0][5] == True:
                        is_temp_org = "yes"
                    else:
                        is_temp_org = 'NULL'

                    temp_id = ""
                    if db_info[0][4] is None:
                        temp_id = str(0)
                    else:
                        temp_id = str(db_info[0][4])

                    # here we will check if the request is passed or failed
                    if (self.assertEqual(resp_status_code, 200)) is None \
                            and (self.assertEqual(resp[random_line]["template_id"], temp_id) is None) \
                            and (self.assertEqual(resp[random_line]["template_original"], is_temp_org) is None) \
                            and (self.assertEqual(resp[random_line]["text_ssml"], db_info[0][2]) is None) \
                            and (self.assertEqual(resp[random_line]["smi_domain_id"], str(db_info[0][7])) is None) \
                            and (self.assertEqual(resp[random_line]["filename_id"], str(db_info[0][0])) is None) \
                            and (self.assertEqual(resp[random_line]["text_owner_email"], db_info[0][6]) is None) \
                            and (self.assertEqual(resp[random_line]["open_to_public"], db_info[0][3]) is None) \
                            and (self.assertEqual(resp[random_line]["name"], db_info[0][1]) is None):
                        print( "Filename Pass")
                        print( "resp: {0}".format(resp))

            except urllibe.URLError as e:
                # if response code is 200 but the fetched email is
                # different than the requested
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Login Failed")
            print( "Status Code: {0}".format(resp["status_code"]))
            print( "Reason: {0}".format(resp["details"]))

    def test_negative_2_filename_wrong_template_value(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print( "Login Pass")
                    print( "Token: {0}".format(resp["cookies"]))

                    # parameters
                    template = "maybe"
                    user = {"template": template}
                    resp = Renderer_Functions.filename(resp["cookies"][0], user)
                    print(resp["status_code"])
                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"]," correct values for template are 'yes' , 'no', 'both'.") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:
                # if response code is 200 but the fetched email is
                # different than the requested
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Login Failed")
            print( "Status Code: {0}".format(resp["status_code"]))
            print( "Reason: {0}".format(resp["details"]))

    def test_negative_12_filename_wrong_domain(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)
        print(resp)
        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print( "Login Pass")
                    print( "Token: {0}".format(resp["cookies"]))

                    # parameters
                    template = "yes"
                    smi_domain_id = "asvkasvhk"
                    user = {"template": template, "smi_domain_id":smi_domain_id}
                    resp = Renderer_Functions.filename(resp["cookies"][0], user)
                    print(resp["status_code"])
                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],"Incorrect value for smi_domain_id.") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:
                # if response code is 200 but the fetched email is
                # different than the requested
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Login Failed")
            print( "Status Code: {0}".format(resp["status_code"]))
            print( "Reason: {0}".format(resp["details"]))

    def test_negative_3_filename_missing_template(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print( "Login Pass")
                    print( "Token: {0}".format(resp["cookies"]))

                    # parameters
                    template = ""
                    user = {}
                    resp = Renderer_Functions.filename(resp["cookies"][0], user)
                    #resp_extract = resp["results"][0]["list"]

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          "Missing Parameter template. correct values for template are 'yes' , 'no', 'both'.") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Login Failed")
            print( "Status Code: {0}".format(resp["status_code"]))
            print( "Reason: {0}".format(resp["details"]))

    def test_negative_3_filename_incorrect_template(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print( "Login Pass")
                    print( "Token: {0}".format(resp["cookies"]))

                    # parameters
                    template = 123456
                    user = {"template":template}
                    resp = Renderer_Functions.filename(resp["cookies"][0], user)
                    #resp_extract = resp["results"][0]["list"]

                    # here we will check if the request is passed or failed
                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp["error_message"],
                                                          "Incorrect value for template. correct values for template are 'yes' , 'no', 'both'.") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp["error_message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print( "Login Failed")
            print( "Status Code: {0}".format(resp["status_code"]))
            print( "Reason: {0}".format(resp["details"]))

    def test_negative_4_filename_wrong_login(self):
        # parameters
        template = "NO"
        user = {"normalization_domain_id": 1, "template": template}
        resp = Renderer_Functions.filename("123456789", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

    def test_negative_filename_5_missing_login(self):
        # parameters
        template = "NO"
        user = {"normalization_domain_id": 1, "template": template}
        resp = Renderer_Functions.filename("NO TOKEN", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

    def test_negative_7_expired_token(self):
        # Parameters
        template = "yes"
        smi_domain_id = "72057594037927948"
        user = {"template": template, "smi_domain_id": smi_domain_id}
        resp = Renderer_Functions.filename("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

if __name__ == '__main__':
    print( "-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
