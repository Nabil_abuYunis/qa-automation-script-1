import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
import configparser
from Database import Postgres
import random

class VBKeywordSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        print (self.currentResult)

    def test_positive_1_keyword(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print ("Login Pass")
                    print ("Token: {0}".format(resp["cookies"]))

                    # Parameters
                    # Get smi_domain_id that have keywords
                    cursor = Postgres.connect_to_db()
                    cursor.execute("select distinct smi_domain_id from smi_domain_keywords")
                    db_info = [r for r in cursor]
                    smi_domain_id = int(random.choice(db_info)[0])

                    # get keywords from the API
                    user = {"smi_domain_id": smi_domain_id}
                    resp = Renderer_Functions.keyword(resp["cookies"][0], user)
                    print(resp)

                    # get the keywords for the chosen domain and its parents
                    has_parent = True
                    all_keywords = []
                    while has_parent:
                        cursor.execute(
                            'select keyword from smi_domain_keywords where smi_domain_id = ' + str(smi_domain_id))
                        db_info = [r for r in cursor]
                        if db_info.__len__() != 0:
                            for var in db_info:
                                all_keywords.append(var[0])

                        # Check if a domain has a parent
                        cursor.execute("select parent_id from smi_domain WHERE smi_domain_id = " + str(smi_domain_id))
                        db_info = [r for r in cursor]
                        if db_info[0][0] is not None:
                            smi_domain_id = db_info[0][0]
                        else:
                            has_parent = False

                    # all_keywords = sorted(all_keywords, key=lambda k: k[0])
                    all_keywords.sort()
                    resp_status_code = resp["status_code"]
                    resp = sorted(resp["results"], key=lambda k: k['keyword'])
                    print ("all_keywords: ", all_keywords)
                    print("resp: ", resp)
                    # Choose a random row to test
                    # random_line = randint(0, resp['count']-1)

                    # here we will check if the request is passed or failed
                    count = resp.__len__()
                    for x in range(count):
                        print("DB Keyword: {0}".format(all_keywords[x]))
                        # print ("SMI_Domain_Id: {0}".format(resp['results'][x]["smi_domain_id"]))
                        print("resp keyword: {0}".format(resp[x]["keyword"]))
                        if (self.assertEqual(resp_status_code, 200)) is None \
                                and (self.assertEqual(resp[x]["keyword"], all_keywords[x])) is None:
                                # and (self.assertEqual(resp['results'][x]["smi_domain_id"], db_info[x][0]) is None) \
                            print ("Keyword Pass")
                            print ("resp: {0}".format(resp))

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Login Failed")
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))

    def test_negative_2_missing_param(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print ("Login Pass")
                    print ("Token: {0}".format(resp["cookies"]))

                    # Parameters
                    user = {}
                    resp = Renderer_Functions.keyword(resp["cookies"][0], user)
                    print(resp)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Missing Parameter smi_domain_id.") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Login Failed")
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))


    def test_negative_3_incorrect_domain(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print ("Login Pass")
                    print ("Token: {0}".format(resp["cookies"]))

                    # Parameters
                    user = {"smi_domain_id": "hello"}
                    resp = Renderer_Functions.keyword(resp["cookies"][0], user)
                    print(resp)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Incorrect value for smi_domain_id.") is None):
                                print( "-|-Result:Test case Pass")
                                print( "resp: {0}".format(resp))
                                print( "Status Code: {0}".format(resp["status_code"]))
                                print( "Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print( "-|-Result:Test case Failed")
                            print( "Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print( "Negative Test Failed")


            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("Login Failed")
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))

    def test_negative_4_wrong_login(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.keyword("123456789", user)
        print("!!!!!")
        print(resp)

        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

    def test_negative_7_domain_missing_login(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.keyword("NO TOKEN", user)
        print("!!!!!")
        print(resp)

        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print( "-|-Result:Test case Pass")
                    print( "resp: {0}".format(resp))
                    print( "Status Code: {0}".format(resp["status_code"]))
                    print( "Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print( "-|-Result:Test case Failed")
                print( "Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print( "Negative Test Failed")

    def test_negative_6_expired_token(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.keyword("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
