import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from random import randint
import configparser
from Database import Postgres

class VBLexiconSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        print(self.currentResult)

    def test_positive_1_lexicon(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    user = {"lexicon": "hello", "load": "load"}
                    resp = Renderer_Functions.lexicon(resp["cookies"][0], user)

                    # resp_status_code = resp["status_code"]

                    if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results']["success_message"], "success") is None):
                        print("Get Voice Pass")
                        print("resp: {0}".format(resp))
                        print("status_code: {0}".format(resp["status_code"]))

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_2_incorrect_lexicon(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    user = {"lexicon": 123456, "load": "load"}
                    resp = Renderer_Functions.lexicon(resp["cookies"][0], user)

                    resp_status_code = resp["status_code"]

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error_message"],
                                                          "Incorrect value for lexicon.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error_message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_3_incorrect_load(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    user = {"lexicon": "hello", "load": 123456}
                    resp = Renderer_Functions.lexicon(resp["cookies"][0], user)

                    resp_status_code = resp["status_code"]

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error_message"],
                                                          "Incorrect value for load.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error_message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_positive_4_missing_lexicon(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    user = {"load": "load"}
                    resp = Renderer_Functions.lexicon(resp["cookies"][0], user)

                    resp_status_code = resp["status_code"]

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error_message"],
                                                          "Missing Parameter lexicon.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error_message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_5_missing_load(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    user = {"lexicon": "hello"}
                    resp = Renderer_Functions.lexicon(resp["cookies"][0], user)

                    resp_status_code = resp["status_code"]

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error_message"],
                                                          "Missing Parameter load.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error_message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")

            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_6_lexicon_wrong_login(self):
        # parameters
        user = {"lexicon": "hello", "load": "load"}
        resp = Renderer_Functions.lexicon("123456789", user)
        print(resp["status_code"])
        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_7_lexicon_missing_login(self):
        # parameters
        user = {"lexicon": "hello", "load": "load"}
        resp = Renderer_Functions.lexicon("NO TOKEN", user)
        print(resp["status_code"])
        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_8_expired_token(self):
        # Parameters
        user = {"lexicon": "hello", "load": "load"}
        resp = Renderer_Functions.lexicon("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

                            
if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
