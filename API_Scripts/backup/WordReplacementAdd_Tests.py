import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from random import randint
import configparser
from Database import Postgres

class VBWordReplacementAddSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        print(self.currentResult)

    def test_positive_0_add_true(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    smi_domain_id = "72057594037927948"
                    word = "brookback"
                    use_gesture = True
                    gesture_id = 2
                    replacement_word = "witcher"

                    if use_gesture is True:
                        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "gesture_id":gesture_id}
                    else:
                        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}
                    
                    resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

                    if use_gesture is True:
                        if (self.assertEqual(resp["status_code"], 200)) is None \
                            and (self.assertEqual(resp['results']["success_message"], "word '"+word+"' will be replaced with gesture_id = "+str(gesture_id)) is None):
                            print("Word Replacement Pass")
                            print("resp: {0}".format(resp))
                            print("status_code: {0}".format(resp["status_code"]))
                    else:
                        if (self.assertEqual(resp["status_code"], 200)) is None \
                            and (self.assertEqual(resp['results']["success_message"], "word '"+word+"' will be replaced with '"+replacement_word+"'") is None):
                            print("Word Replacement Pass")
                            print("resp: {0}".format(resp))
                            print("status_code: {0}".format(resp["status_code"]))

                    
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_positive_1_add_false(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    smi_domain_id = "72057594037927948"
                    word = "sega"
                    use_gesture = False
                    gesture_id = 2
                    replacement_word = "world"

                    if use_gesture is True:
                        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "gesture_id":gesture_id}
                    else:
                        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}
                    
                    resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

                    if use_gesture is True:
                        if (self.assertEqual(resp["status_code"], 200)) is None \
                            and (self.assertEqual(resp['results']["success_message"], "word '"+word+"' will be replaced with gesture_id = "+str(gesture_id)) is None):
                            print("Word Replacement Pass")
                            print("resp: {0}".format(resp))
                            print("status_code: {0}".format(resp["status_code"]))
                    else:
                        if (self.assertEqual(resp["status_code"], 200)) is None \
                            and (self.assertEqual(resp['results']["success_message"], "word '"+word+"' will be replaced with '"+replacement_word+"'") is None):
                            print("Word Replacement Pass")
                            print("resp: {0}".format(resp))
                            print("status_code: {0}".format(resp["status_code"]))

                    
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_2_incorrect_domain(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    smi_domain_id = "chest"
                    word = "hello"
                    use_gesture = False
                    gesture_id = 2
                    replacement_word = "world"

                    if use_gesture is True:
                        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "gesture_id":gesture_id}
                    else:
                        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}
                    
                    resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Incorrect value for smi_domain_id.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")

                    
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_3_incorrect_word(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    smi_domain_id = "72057594037927948"
                    word = 123456
                    use_gesture = False
                    gesture_id = 2
                    replacement_word = "world"

                    if use_gesture is True:
                        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "gesture_id":gesture_id}
                    else:
                        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}
                    
                    resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Incorrect value for word.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")

                    
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_4_incorrect_use_gesture(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    smi_domain_id = "72057594037927948"
                    word = "dance"
                    use_gesture = 789456
                    gesture_id = 2
                    replacement_word = "world"

                    if use_gesture is True:
                        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "gesture_id":gesture_id}
                    else:
                        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}
                    
                    resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Incorrect value for use_gesture.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")

                    
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_5_incorrect_gesture_id(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    smi_domain_id = "72057594037927948"
                    word = "dance"
                    use_gesture = True
                    gesture_id = "not all of them"
                    replacement_word = "world"

                    user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "gesture_id":gesture_id}
                    resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Incorrect value for gesture_id.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")

                    
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_6_incorrect_replacement(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    smi_domain_id = "72057594037927948"
                    word = "dance"
                    use_gesture = False
                    gesture_id = 2
                    replacement_word = 123456

                    user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}
                    resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Incorrect value for replacement_word.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")

                    
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_7_missing_domain(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    smi_domain_id = "72057594037927948"
                    word = "dance"
                    use_gesture = True
                    gesture_id = 2
                    replacement_word = "world"

                    if use_gesture is True:
                        user = {"word": word, "use_gesture":use_gesture, "gesture_id":gesture_id}
                    else:
                        user = {"word": word, "use_gesture":use_gesture, "replacement_word":replacement_word}
                    
                    resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Missing Parameter smi_domain_id.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")
                        
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_8_missing_word(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    smi_domain_id = "72057594037927948"
                    word = "dance"
                    use_gesture = True
                    gesture_id = 2
                    replacement_word = "world"

                    if use_gesture is True:
                        user = {"smi_domain_id": smi_domain_id,"use_gesture":use_gesture, "gesture_id":gesture_id}
                    else:
                        user = {"smi_domain_id": smi_domain_id,"use_gesture":use_gesture, "replacement_word":replacement_word}
                    
                    resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Missing Parameter word.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")
                        
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_9_missing_use_gesture(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    smi_domain_id = "72057594037927948"
                    word = "dance"
                    use_gesture = True
                    gesture_id = 2
                    replacement_word = "world"

                    if use_gesture is True:
                        user = {"smi_domain_id": smi_domain_id, "word": word, "gesture_id":gesture_id}
                    else:
                        user = {"smi_domain_id": smi_domain_id, "word": word, "replacement_word":replacement_word}
                    
                    resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Missing Parameter use_gesture.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")
                        
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_10_missing_gesture_id(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    smi_domain_id = "72057594037927948"
                    word = "dance"
                    use_gesture = True
                    gesture_id = 2
                    replacement_word = "world"

                    user = {"smi_domain_id": smi_domain_id, "use_gesture": use_gesture, "word": word}
                    resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Missing Parameter gesture_id.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")
                        
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))


    def test_negative_11_missing_replacement(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    smi_domain_id = "72057594037927948"
                    word = "dance"
                    use_gesture = False
                    gesture_id = 2
                    replacement_word = "world"

                    user = {"smi_domain_id": smi_domain_id, "use_gesture": use_gesture, "word": word}
                    resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Missing Parameter replacement_word.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")
                        
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_14_permission_domain(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    smi_domain_id = 72057594037927942
                    word = "dance"
                    use_gesture = True
                    gesture_id = 2
                    replacement_word = "world"

                    user = {"smi_domain_id": smi_domain_id, "word":word, "use_gesture": use_gesture, "gesture_id": gesture_id}
                    resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        print(resp['results']["error message"])
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],"you do not have permission to use this domain") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")
                        
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_15_permission_gesture(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    smi_domain_id = "72057594037927948"
                    word = "dance"
                    use_gesture = True
                    gesture_id = 2
                    replacement_word = "world"

                    user = {"smi_domain_id": smi_domain_id, "word":word, "use_gesture": use_gesture, "gesture_id": gesture_id}
                    resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error_message"],
                                                          "you do not have permission to use this gesture for word replacement.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error_message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")
                        
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_15_permission_gesture(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    smi_domain_id = "72057594037927948"
                    word = "dance"
                    use_gesture = True
                    gesture_id = 2
                    replacement_word = "world"

                    user = {"smi_domain_id": smi_domain_id, "word":word, "use_gesture": use_gesture, "gesture_id": gesture_id}
                    resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error_message"],
                                                          " " + word + " already exists.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error_message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")
                        
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_6_word_replacement_wrong_login(self):
        # parameters
        smi_domain_id = "72057594037927948"
        word = "hello"
        use_gesture = True
        gesture_id = 2
        replacement_word = "ageag"

        if use_gesture is True:
            user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "gesture_id":gesture_id}
        else:
            user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}
        
        resp = Renderer_Functions.word_replacement("123456789", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_11_expired_token(self):
        # parameters
        smi_domain_id = "72057594037927948"
        word = "hello"
        use_gesture = True
        gesture_id = 2
        replacement_word = "ageag"

        if use_gesture is True:
            user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "gesture_id":gesture_id}
        else:
            user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}
        
        resp = Renderer_Functions.word_replacement_add("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")

    def test_negative_7_word_replacement_missing_login(self):
        # parameters
        smi_domain_id = "72057594037927948"
        word = "hello"
        use_gesture = True
        gesture_id = 2
        replacement_word = "ageag"

        if use_gesture is True:
            user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "gesture_id":gesture_id}
        else:
            user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}
        
        resp = Renderer_Functions.word_replacement("NO TOKEN", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_16_already_exists(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    smi_domain_id = "72057594037927948"
                    word = "hello"
                    use_gesture = True
                    gesture_id = 2
                    replacement_word = "world"

                    if use_gesture is True:
                        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "gesture_id":gesture_id}
                    else:
                        user = {"smi_domain_id": smi_domain_id, "use_gesture":use_gesture, "word": word, "replacement_word":replacement_word}
                    
                    resp = Renderer_Functions.word_replacement_add(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error_message"],
                                                          " hello already exists.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error_message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")

                    
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))


if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
