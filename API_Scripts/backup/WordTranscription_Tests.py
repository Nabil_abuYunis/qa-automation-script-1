import urllib.error as urllibe
import unittest
import flask
import Renderer_Functions as Renderer_Functions
from random import randint
import configparser
from Database import Postgres

class VBWordTranscriptionSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        print(self.currentResult)

    def test_positive_1_word_transcription(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    user = {"smi_domain_id": "72057594037927948"}
                    resp = Renderer_Functions.word_transcription(resp["cookies"][0], user)

                    resp_status_code = resp["status_code"]
                    random_line = randint(0, resp["count"] - 1)
                    if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(resp['results'][random_line]["transcription"], "") is None) \
                        and (self.assertEqual(resp['results'][random_line]["smi_domain_id"], "") is None) \
                        and (self.assertEqual(resp['results'][random_line]["word"], "") is None):
                        print("Word Transcription Pass")
                        print("resp: {0}".format(resp))
                        print("status_code: {0}".format(resp["status_code"]))
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_2_incorrect_domain(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    user = {"smi_domain_id": "mistletoe"}
                    resp = Renderer_Functions.word_transcription(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Incorrect value for smi_domain_id.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")
                        
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_3_missing_domain(self):
        config = configparser.ConfigParser()
        config.sections()
        config.read('api_config.ini')
        # valid username and password
        email = config["LOGIN"]["email"]
        password = config["LOGIN"]["password"]
        user = {"email": email, "password": password}
        resp = Renderer_Functions.login(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]["results"]["user"]["email_address"]
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print("Login Pass")
                    print("Token: {0}".format(resp["cookies"]))

                    #Parameters
                    user = {}
                    resp = Renderer_Functions.word_transcription(resp["cookies"][0], user)

                    if resp["status_code"] == 200:
                        try:
                            if (self.assertEqual(resp["status_code"], 200)) is None \
                                    and (self.assertEqual(resp['results']["error message"],
                                                          "Missing Parameter smi_domain_id.") is None):
                                print("-|-Result:Test case Pass")
                                print("resp: {0}".format(resp))
                                print("Status Code: {0}".format(resp["status_code"]))
                                print("Reason: {0}".format(resp['results']["error message"]))

                        except urllibe.URLError as e:
                            print("-|-Result:Test case Failed")
                            print("Reason: {0}".format(e.reason))

                    # Check error type
                    elif resp["status_code"] != 200:
                        print("Negative Test Failed")
                        
            except urllibe.URLError as e:  # if response code is 200 but the fetched email is
                # different than the requested
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print("Login Failed")
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))

    def test_negative_6_word_replacement_wrong_login(self):
        # parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.word_transcription("123456789", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_7_word_replacement_missing_login(self):
        # parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.word_transcription("NO TOKEN", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print("-|-Result:Test case Pass")
                    print("resp: {0}".format(resp))
                    print("Status Code: {0}".format(resp["status_code"]))
                    print("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print("-|-Result:Test case Failed")
                print("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print("Negative Test Failed")

    def test_negative_8_expired_token(self):
        # Parameters
        user = {"smi_domain_id": "72057594037927948"}
        resp = Renderer_Functions.word_transcription("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user)

        # here we will check if the request is passed or failed
        if resp["status_code"] != 200:
            try:
                if (self.assertEqual(resp["status_code"], '401')) is None \
                        and (self.assertEqual(resp["details"],
                                              "Unauthorized Access") is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("Status Code: {0}".format(resp["status_code"]))
                    print ("Reason: {0}".format(resp["details"]))

            except urllibe.URLError as e:
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] == 200:
            print ("Negative Test Failed")


if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
