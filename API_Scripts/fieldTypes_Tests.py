import unittest
import Renderer_Functions
from Database import Postgres
import random
import string
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')


class VBFieldTypesSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Get logged in email
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_field_types(self):
        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        user = {}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "fieldtypes")

        # Fetch the info from the Database
        cursor = Postgres.connect_to_db()
        query = "SELECT template_field_type_id,template_field_type_name FROM public.template_field_type"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["id"])

        # here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        count = resp.__len__()
        for x in range(count):
            if(self.assertEqual(resp_status_code, 200)) is None \
                    and(self.assertEqual(resp[x]["id"], db_info[x][0]) is None) \
                    and(self.assertEqual(resp[x]["name"], db_info[x][1]) is None):
                        print("Field Types Pass")
                        print("resp: {0}".format(resp))
                        print("Id: {0}".format(resp[x]["id"]))
                        print("Name: {0}".format(resp[x]["name"]))
            else:
                    print("Positive Test Failed")
                    assert False
        print("Field Types Pass")

    def test_negative_2_field_types_wrong_login(self):
        # Parameters
        user = {}
        resp = Renderer_Functions.send_request("123456789", user, "fieldtypes")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
                and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_3_field_types_missing_login(self):
        # Parameters
        user = {}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "fieldtypes")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
                and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_4_field_types_expired_token(self):

        # Parameters
        user = {}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "fieldtypes")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
                and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp["details"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
