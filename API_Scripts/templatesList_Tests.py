import unittest
import Renderer_Functions
from Database import Postgres
import random
import string
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')


class VBTemplatesListSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Get logged in email
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_templates_list_include_templates_False(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        include_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get available random recording_set_id
        recording_set_ids_list = Renderer_Functions.get_available_recording_set_ids()
        recording_set_id = random.choice(recording_set_ids_list)[0]
        #recording_set_id = 4398046511109

        user = {"project_id": random_project, "domain_id": random_domain, "include_templates": include_template, "recording_set_id": recording_set_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templateslist")

        # Fetch the info from the Database
        cursor = Postgres.connect_to_db()
        query = "SELECT labeling_id id, false is_template, COALESCE(actual_text, text) AS text_ssml," \
                "nvc.file_name AS nvc_filename, NULL::bigint renderer_text_id, NULL::text as name, NULL::boolean open_to_public, " \
                "NULL::text text_ssml_header, COALESCE(actual_text, text) AS text, FALSE can_edit " \
                "FROM(SELECT * FROM nvc WHERE nvc_id IN(SELECT max(nvc_id) FROM nvc WHERE short_file_name IN(" \
                "SELECT short_file_name FROM recording_set_clip WHERE recording_set_id = "+str(recording_set_id) + \
                ") GROUP BY short_file_name)) nvc INNER JOIN(SELECT short_file_name, max(labeling_id) labeling_id " \
                "FROM nvc_annotation_progress WHERE nvc_annotation_status >= 6 " \
                "GROUP BY short_file_name) prog USING (short_file_name) INNER JOIN ovc USING(ovc_id) INNER JOIN clean_training_text USING(training_text_id) " \
                "LEFT OUTER JOIN clean_actual_text USING(ovc_id) ORDER BY 3, 1"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["id"])

        # here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        count = resp.__len__()
        for x in range(count):
            if(self.assertEqual(resp_status_code, 200)) is None \
                    and(self.assertEqual(resp[x]["id"], db_info[x][0]) is None) \
                    and(self.assertEqual(resp[x]["is_template"], db_info[x][1]) is None) \
                    and (self.assertEqual(resp[x]["text_ssml"], db_info[x][2]) is None) \
                    and (self.assertEqual(resp[x]["nvc_filename"], db_info[x][3]) is None) \
                    and (self.assertEqual(resp[x]["text"], db_info[x][8]) is None) \
                    and (self.assertEqual(resp[x]["can_edit"], db_info[x][9]) is None):
                        print("Templates list include templates false Pass")
                        print("resp: {0}".format(resp))
                        print("Id: {0}".format(resp[x]["id"]))
                        print("Is template: {0}".format(resp[x]["is_template"]))
                        print("Text ssml: {0}".format(resp[x]["text_ssml"]))
                        print("nvc filename: {0}".format(resp[x]["nvc_filename"]))
                        print("text: {0}".format(resp[x]["text"]))
                        print("can edit: {0}".format(resp[x]["can_edit"]))
            else:
                    print("Positive Test Failed")
                    assert False
        print("Templates list include templates false Pass")

    def test_positive_2_templates_list_include_templates_True(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        include_template = True

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get available random recording_set_id
        recording_set_ids_list = Renderer_Functions.get_available_recording_set_ids()
        recording_set_id = random.choice(recording_set_ids_list)[0]

        user = {"project_id": random_project, "domain_id": random_domain, "include_templates": include_template, "recording_set_id": recording_set_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templateslist")

        # Fetch the info from the Database
        cursor = Postgres.connect_to_db()
        query = "(SELECT labeling_id id, false is_template, COALESCE(actual_text, text) AS text_ssml," \
                "nvc.file_name AS nvc_filename, NULL::bigint renderer_text_id, NULL::text as name, " \
                "NULL::boolean open_to_public, NULL::text text_ssml_header, COALESCE(actual_text, text) AS text, FALSE can_edit " \
                "FROM(SELECT * FROM nvc WHERE nvc_id IN(SELECT max(nvc_id) FROM nvc WHERE short_file_name IN(" \
                "SELECT short_file_name FROM recording_set_clip WHERE recording_set_id = "+str(recording_set_id) + ") " \
                "GROUP BY short_file_name)) nvc INNER JOIN(SELECT short_file_name, max(labeling_id) labeling_id " \
                "FROM nvc_annotation_progress WHERE nvc_annotation_status >= 6 " \
                "GROUP BY short_file_name) prog USING (short_file_name) INNER JOIN ovc USING(ovc_id) INNER JOIN clean_training_text USING(training_text_id)" \
                "LEFT OUTER JOIN clean_actual_text USING(ovc_id) ORDER BY 3, 1)" \
                "UNION ALL " \
                "(SELECT template_id id, true is_template, substr(substr(substr(text_ssml, position('<speak' in text_ssml))," \
                "position('>' in substr(text_ssml, position('<speak' in text_ssml))) + 1), 0, " \
                "position('</speak' in substr(substr(text_ssml, position('<speak' in text_ssml))," \
                "position('>' in substr(text_ssml, position('<speak' in text_ssml))) + 1))) text_ssml," \
                "NULL::text nvc_filename, renderer_text_id, name, rt.open_to_public, " \
                "substr(text_ssml, 0, position('<speak' in text_ssml) + position('>' in substr(text_ssml, position('<speak' in text_ssml)))) text_ssml_header, " \
                "REPLACE(regexp_replace(substr(substr(substr(text_ssml, position('<speak' in text_ssml))," \
                "position('>' in substr(text_ssml, position('<speak' in text_ssml))) + 1), 0, " \
                " position('</speak' in substr(substr(text_ssml, position('<speak' in text_ssml))," \
                " position('>' in substr(text_ssml, position('<speak' in text_ssml))) + 1))),'<say-as.*?interpret-as.*?>', ' ', 'g'), '</say-as>', ' '), " \
                "CASE WHEN 'admin@speechmorphing.com' ='" + email + "' OR text_owner_email ILIKE '"+email+"' " \
                "OR smi_domain_owner_project_id ILIKE '"+str(random_project)+"' THEN TRUE ELSE FALSE END can_edit " \
                "FROM renderer_text rt INNER JOIN smi_domain USING(smi_domain_id) INNER JOIN template USING(template_id) WHERE rt.smi_domain_id IS NOT NULL " \
                "AND name NOT ILIKE '%_quickrender'  AND template_id IS NOT NULL AND rt.smi_domain_id = '"+str(random_domain)+"'   AND is_template_org " \
                "AND COALESCE(is_deleted, FALSE) = FALSE AND(smi_domain.open_to_public OR ('admin@speechmorphing.com' = '"+email+"' " \
                "OR (smi_domain_owner_project_id ILIKE '"+str(random_project)+"' OR rt.smi_domain_id IN (SELECT rt.smi_domain_id FROM smi_domain_permissions " \
                "WHERE project_id ILIKE '"+str(random_project)+"')))) AND (rt.open_to_public " \
                "OR ('admin@speechmorphing.com' = '"+email+"' OR text_owner_email ILIKE '"+email+"' " \
                "OR renderer_text_id IN (SELECT renderer_text_id FROM renderer_text_permissions WHERE user_email ILIKE '"+email+"' AND is_write)" \
                "OR text_owner_project_id IN (SELECT text_owner_project_id FROM user_project_permissions WHERE user_email ILIKE '"+email+"' " \
                " AND user_permission_id IN (11, 13)))) ORDER BY 3, 1)"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["id"])

        # here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        count = resp.__len__()
        for x in range(count):
            if(self.assertEqual(resp_status_code, 200)) is None \
                    and(self.assertEqual(resp[x]["id"], db_info[x][0]) is None) \
                    and(self.assertEqual(resp[x]["is_template"], db_info[x][1]) is None) \
                    and (self.assertEqual(resp[x]["text_ssml"], db_info[x][2]) is None) \
                    and (Renderer_Functions.check_nvc_filename_value(resp[x], db_info[x])) \
                    and (self.assertEqual(resp[x]["text"], db_info[x][8]) is None) \
                    and (self.assertEqual(resp[x]["can_edit"], db_info[x][9]) is None):
                        print("Templates list include templates false Pass")
                        print("resp: {0}".format(resp))
                        print("Id: {0}".format(resp[x]["id"]))
                        print("Is template: {0}".format(resp[x]["is_template"]))
                        print("Text ssml: {0}".format(resp[x]["text_ssml"]))
                        print("text: {0}".format(resp[x]["text"]))
                        print("can edit: {0}".format(resp[x]["can_edit"]))
                        if resp[x]["is_template"] is False:
                            print("nvc filename: {0}".format(resp[x]["nvc_filename"]))

            else:
                    print("Positive Test Failed")
                    assert False
        print("Templates list include templates false Pass")

    def test_negative_3_templates_list_wrong_login(self):

        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        include_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get available random recording_set_id
        recording_set_ids_list = Renderer_Functions.get_available_recording_set_ids()
        recording_set_id = random.choice(recording_set_ids_list)[0]

        user = {"project_id": random_project, "domain_id": random_domain, "include_templates": include_template,
                "recording_set_id": recording_set_id}
        resp = Renderer_Functions.send_request("123456789", user, "templateslist")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
                and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_4_templates_list_missing_login(self):

        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        include_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get available random recording_set_id
        recording_set_ids_list = Renderer_Functions.get_available_recording_set_ids()
        recording_set_id = random.choice(recording_set_ids_list)[0]

        user = {"project_id": random_project, "domain_id": random_domain, "include_templates": include_template,
                "recording_set_id": recording_set_id}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "templateslist")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
                and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_5_templates_list_expired_token(self):

        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        include_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get available random recording_set_id
        recording_set_ids_list = Renderer_Functions.get_available_recording_set_ids()
        recording_set_id = random.choice(recording_set_ids_list)[0]

        user = {"project_id": random_project, "domain_id": random_domain, "include_templates": include_template,
                "recording_set_id": recording_set_id}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "templateslist")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_6_templates_list_missing_project_id(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        include_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get available random recording_set_id
        recording_set_ids_list = Renderer_Functions.get_available_recording_set_ids()
        recording_set_id = random.choice(recording_set_ids_list)[0]

        user = {"domain_id": random_domain, "include_templates": include_template,
                "recording_set_id": recording_set_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templateslist")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and(self.assertEqual(resp['results']["error_message"], "missing parameter project_id.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_7_templates_list_incorrect_project_id(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        include_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email,
                                                                                                 language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get available random recording_set_id
        recording_set_ids_list = Renderer_Functions.get_available_recording_set_ids()
        recording_set_id = random.choice(recording_set_ids_list)[0]

        user = {"project_id": None, "domain_id": random_domain, "include_templates": include_template,
                "recording_set_id": recording_set_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templateslist")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and (
                self.assertEqual(resp['results']["error_message"], "incorrect value for project_id.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_8_templates_list_missing_domain_id(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        include_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get available random recording_set_id
        recording_set_ids_list = Renderer_Functions.get_available_recording_set_ids()
        recording_set_id = random.choice(recording_set_ids_list)[0]

        user = {"project_id": random_project, "include_templates": include_template, "recording_set_id": recording_set_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templateslist")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and (self.assertEqual(resp['results']["error_message"], "missing parameter domain_id.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_9_templates_list_incorrect_domain_id(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        include_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email,language_id)
        random_project = random.choice(allowed_projects)

        # Get available random recording_set_id
        recording_set_ids_list = Renderer_Functions.get_available_recording_set_ids()
        recording_set_id = random.choice(recording_set_ids_list)[0]

        user = {"project_id": random_project, "domain_id": None, "include_templates": include_template, "recording_set_id": recording_set_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templateslist")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and (
                self.assertEqual(resp['results']["error_message"], "incorrect value for domain_id.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_10_templates_list_missing_recording_set_id(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        include_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        user = {"project_id": random_project, "domain_id": random_domain, "include_templates": include_template}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templateslist")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and (self.assertEqual(resp['results']["error_message"], "missing parameter recording_set_id.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_11_templates_list_incorrect_recording_set_id(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        include_template = False
        recording_set_id = ''.join(random.choice(string.ascii_letters) for ii in range(5))

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        user = {"project_id": random_project, "domain_id": random_domain, "include_templates": include_template, "recording_set_id": recording_set_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templateslist")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and (self.assertEqual(resp['results']["error_message"], "incorrect value for recording_set_id.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_12_templates_list_missing_include_templates(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get available random recording_set_id
        recording_set_ids_list = Renderer_Functions.get_available_recording_set_ids()
        recording_set_id = random.choice(recording_set_ids_list)[0]

        user = {"project_id": random_project, "domain_id": random_domain, "recording_set_id": recording_set_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templateslist")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and (self.assertEqual(resp['results']["error_message"], "missing parameter include_templates.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_13_templates_list_incorrect_include_templates(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        include_template = ''.join(random.choice(string.ascii_letters) for ii in range(5))

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get available random recording_set_id
        recording_set_ids_list = Renderer_Functions.get_available_recording_set_ids()
        recording_set_id = random.choice(recording_set_ids_list)[0]

        user = {"project_id": random_project, "domain_id": random_domain, "include_templates": include_template, "recording_set_id": recording_set_id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templateslist")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and (
                self.assertEqual(resp['results']["error_message"], "incorrect value for include_templates.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
