import unittest
import Renderer_Functions
from Database import Postgres
import random
import string
import configparser

# for logging purposes
lgr = Renderer_Functions.init_logger('resultfile.log')


class VBTemplatesPhonemesSuites(unittest.TestCase):
    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    # Get logged in email
    config = configparser.ConfigParser()
    config.sections()
    config.read('api_config.ini')

    def setUp(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Start run test case: {0}".format(str(self.id())))
        lgr.info(self.id())
        print("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print("\n---------------------------------------------------------------------------------------------")
        print("Completed running test case: {0}".format(str(self.id())))
        print("-----------------------------------------------------------------------------------------------\n")
        lgr.info(str(self.currentResult) + "\n-----------------------------------------------------------------------"
                                           "----------------------------")
        print(self.currentResult)

    def test_positive_1_templates_phonemes_is_template_True(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        is_template = True

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get distinct template ids
        cursor = Postgres.connect_to_db()
        query = "SELECT DISTINCT template_id FROM renderer_text WHERE is_template_org = true"
        cursor.execute(query)
        templates_ids = [r for r in cursor]
        id = random.choice(templates_ids)[0]
        #id = 3298534883871

        user = {"project_id": random_project, "domain_id": random_domain, "is_template": is_template, "id": id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesphonemes")

        # Fetch the info from the Database
        query = "SELECT distinct template_id,  word_number, COALESCE (sub_field_id,0) as sub_field_id, word_text, pos_code,"  \
                " tobi_boundary_tone, tobi_phrase_break, tobi_focus_type, tobi_compressed_f0, " \
                "COALESCE(template_field_type_id,0) as template_field_type_id, sensitivity_gender,  COALESCE (mood_id,0) as mood_id,  COALESCE (sensitivity_next_word,0) as sensitivity_next_word, " \
                "word_phoneme_number, phoneme_text, COALESCE(stress_id, 0) as stress_id, COALESCE(tobi_pitch_accent, 0)as tobi_pitch_accent, " \
                " COALESCE(delayed_peak_id, 0) as delayed_peak_id, COALESCE(duration_milli, 0) as duration_milli FROM template_words A " \
                "INNER JOIN  template_words_phonemes using (template_id, word_number) where template_id  ="+str(id) + \
                " ORDER BY template_id, word_number, word_phoneme_number"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["id"])

        # here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        count = resp.__len__()
        for x in range(count):
            if(self.assertEqual(resp_status_code, 200)) is None \
                    and(self.assertEqual(resp[x]["id"], db_info[x][0]) is None) \
                    and(self.assertEqual(resp[x]["word_number"], db_info[x][1]) is None) \
                    and (self.assertEqual(resp[x]["tobi_pitch_accent"], db_info[x][16]) is None) \
                    and (Renderer_Functions.check_response_vs_db_value(resp[x], db_info[x], "pos_code", 4))\
                    and (self.assertEqual(resp[x]["sensitivity_next_word"], db_info[x][12]) is None) \
                    and (self.assertEqual(resp[x]["template_field_type_id"], db_info[x][9]) is None) \
                    and (self.assertEqual(resp[x]["word_text"], db_info[x][3]) is None) \
                    and (self.assertEqual(resp[x]["tobi_boundary_tone"], db_info[x][5]) is None) \
                    and (self.assertEqual(resp[x]["sensitivity_gender"], db_info[x][10]) is None) \
                    and (self.assertEqual(resp[x]["phoneme_text"], db_info[x][14]) is None) \
                    and (self.assertEqual(resp[x]["tobi_phrase_break"], db_info[x][6]) is None) \
                    and (Renderer_Functions.check_response_vs_db_value(resp[x], db_info[x], "tobi_compressed_f0", 8))\
                    and (self.assertEqual(resp[x]["stress_id"], db_info[x][15]) is None) \
                    and (self.assertEqual(resp[x]["tobi_focus_type"], db_info[x][7]) is None) \
                    and (self.assertEqual(resp[x]["delayed_peak_id"], db_info[x][17]) is None) \
                    and (self.assertEqual(resp[x]["word_phoneme_number"], db_info[x][13]) is None) \
                    and (self.assertEqual(resp[x]["duration_milli"], db_info[x][18]) is None) \
                    and (self.assertEqual(resp[x]["mood_id"], db_info[x][11]) is None) \
                    and (self.assertEqual(resp[x]["sub_field_id"], db_info[x][2]) is None):
                        print("Templates phonemes include templates True Pass")
                        print("resp: {0}".format(resp))
            else:
                    print("Positive Test Failed")
                    assert False
        print("Templates phonemes include templates True Pass")

    def test_positive_2_templates_phonemes_is_template_False(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        is_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get distinct labeling ids
        cursor = Postgres.connect_to_db()
        query = "SELECT DISTINCT labeling_id FROM nvc_phoneme_labels"
        cursor.execute(query)
        labeling_ids = [r for r in cursor]
        id = random.choice(labeling_ids)[0]

        user = {"project_id": random_project, "domain_id": random_domain, "is_template": is_template, "id": id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesphonemes")

        # Fetch the info from the Database
        '''
        # old query
        query = "WITH lang as (SELECT language_id FROM smi_domain WHERE smi_domain_id = "+str(random_domain) +\
                ") SELECT labeling_id as id, word_number, word_phoneme_number, rank() OVER (PARTITION BY word_number ORDER BY word_number, " \
                " word_phoneme_number)  as in_word_phoneme_number, phoneme, phoneme_attribute, tobi_pitch_accent, stress_id, word_start," \
                "word, phrase_break, compressed_f0, tobi_boundary_tone, tobi_peak_indicator, focus, style FROM(" \
                "SELECT labeling_id, count(CASE WHEN word_start THEN 1 ELSE NULL END) OVER(ORDER BY labeling_id, pos) word_number, pos as word_phoneme_number," \
                "CASE WHEN lang.language_id = 6 THEN ipa WHEN lang.language_id = 10 THEN (CASE WHEN(phoneme_attribute & 65536) > 0 THEN 'ˈ' ELSE '' END || ipa) " \
                " END phoneme, phoneme_attribute, tobi_pitch_accent,CASE WHEN lang.language_id = 6 THEN CASE WHEN phoneme ILIKE '%1' THEN 1 " \
                "WHEN phoneme ILIKE '%2' THEN 2 ELSE 0 END WHEN lang.language_id = 10 THEN CASE WHEN (phoneme_attribute & 65536) > 0 " \
                "THEN 1 ELSE 0 END  END stress_id, word_start,word, phrase_break_id phrase_break, compressed_f0, tobi_boundary_tone, " \
                "tobi_peak_indicator, focus, style FROM nvc_phoneme_labels  npl INNER JOIN lang ON TRUE " \
                "INNER JOIN cmu_2_ipa c2i ON(phoneme = cmu AND c2i.language_id = lang.language_id) INNER JOIN prosody_phrase_break ppb ON (npl.phrase_break = ppb.phrase_break " \
                " AND lang.language_id = ppb.language_id) WHERE labeling_id = "+str(id)+" AND NOT (phoneme = 'SIL' AND (pos = 0 OR pos = (SELECT max(pos) FROM nvc_phoneme_labels " \
                "WHERE phoneme != '#' AND labeling_id = "+str(id)+" ))) ORDER BY 1, 2, 3)  innerQuery"
        '''
        query = "WITH lang as (SELECT language_id FROM smi_domain WHERE smi_domain_id = " + str(random_domain) + ") SELECT labeling_id as id, word_number, word_phoneme_number, rank() OVER(PARTITION BY " \
                "word_number ORDER BY word_number, word_phoneme_number) as in_word_phoneme_number, phoneme, phoneme_attribute, tobi_pitch_accent, stress_id, word, phrase_break, compressed_f0, tobi_boundary_tone, focus, style " \
                "FROM(SELECT labeling_id, count(CASE WHEN word_start THEN 1 ELSE NULL END)  OVER(ORDER BY labeling_id, pos) word_number, pos as word_phoneme_number, CASE WHEN lang.language_id = 6 THEN " \
                "ipa WHEN lang.language_id = 10 THEN(CASE WHEN(phoneme_attribute & 65536) > 0 THEN '?' ELSE '' END || ipa) END phoneme, phoneme_attribute, tobi_pitch_accent, CASE WHEN lang.language_id = 6 THEN" \
                " CASE WHEN phoneme ILIKE '%1' THEN 1 WHEN phoneme ILIKE '%2' THEN 2 ELSE 0 END WHEN lang.language_id = 10 THEN CASE WHEN(phoneme_attribute & 65536) > 0 THEN 1 ELSE 0 END END stress_id, word_start, word, phrase_break_id" \
                " phrase_break, compressed_f0, tobi_boundary_tone, focus, style FROM nvc_phoneme_labels LEFT OUTER JOIN prosody_phrase_break USING(phrase_break) INNER JOIN lang ON TRUE INNER JOIN cmu_2_ipa c2i ON(phoneme=cmu" \
                " AND c2i.language_id = lang.language_id) WHERE labeling_id = "+ str(id)+" AND NOT(phoneme='SIL' AND(pos=0 OR pos = (SELECT max(pos) FROM nvc_phoneme_labels WHERE phoneme != '#' AND labeling_id = "+str(id)+" )))" \
                " ORDER BY 1, 2, 3) innerQuery"
        cursor.execute(query)
        db_info = [r for r in cursor]
        db_info = sorted(db_info, key=lambda k: k[0])

        # Reorder the API received rows
        resp_status_code = resp["status_code"]
        resp = sorted(resp["results"], key=lambda k: k["id"])

        # here we will check if the request is passed or failed
        # If they are not the same amount then fail
        if resp.__len__() != db_info.__len__():
            assert False

        count = resp.__len__()
        for x in range(count):
            if (self.assertEqual(resp_status_code, 200)) is None \
                    and (self.assertEqual(resp[x]["id"], db_info[x][0]) is None) \
                    and (self.assertEqual(resp[x]["word_number"], db_info[x][1]) is None) \
                    and (self.assertEqual(resp[x]["tobi_pitch_accent"], db_info[x][6]) is None) \
                    and (self.assertEqual(resp[x]["word_text"], db_info[x][8]) is None) \
                    and (self.assertEqual(resp[x]["tobi_boundary_tone"], db_info[x][11]) is None) \
                    and (self.assertEqual(resp[x]["phoneme_text"], db_info[x][4]) is None) \
                    and (Renderer_Functions.check_response_vs_db_value(resp[x], db_info[x], "compressed_f0", 11)) \
                    and (self.assertEqual(resp[x]["stress_id"], db_info[x][7]) is None) \
                    and (self.assertEqual(resp[x]["word_phoneme_number"], db_info[x][3]) is None):
                print("Templates phonemes include templates False Pass")
                print("resp: {0}".format(resp))
            else:
                print("Positive Test Failed")
                assert False
        print("Templates phonemes include templates False Pass")

    def test_negative_3_templates_phonemes_wrong_login(self):

        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        is_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get distinct labeling ids
        cursor = Postgres.connect_to_db()
        query = "SELECT DISTINCT labeling_id FROM nvc_labels"
        cursor.execute(query)
        labeling_ids = [r for r in cursor]
        id = random.choice(labeling_ids)[0]

        user = {"project_id": random_project, "domain_id": random_domain, "is_template": is_template, "id": id}
        resp = Renderer_Functions.send_request(123456789,user, "templatesphonemes")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
                and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_4_templates_phonemes_missing_login(self):

        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        is_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get distinct labeling ids
        cursor = Postgres.connect_to_db()
        query = "SELECT DISTINCT labeling_id FROM nvc_labels"
        cursor.execute(query)
        labeling_ids = [r for r in cursor]
        id = random.choice(labeling_ids)[0]

        user = {"project_id": random_project, "domain_id": random_domain, "is_template": is_template, "id": id}
        resp = Renderer_Functions.send_request("NO TOKEN", user, "templatesphonemes")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
                and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_5_templates_phonemes_expired_token(self):

        email = self.config["LOGIN"]["email"]

        # Parameters
        language_id = 6
        is_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get distinct labeling ids
        cursor = Postgres.connect_to_db()
        query = "SELECT DISTINCT labeling_id FROM nvc_labels"
        cursor.execute(query)
        labeling_ids = [r for r in cursor]
        id = random.choice(labeling_ids)[0]

        user = {"project_id": random_project, "domain_id": random_domain, "is_template": is_template, "id": id}
        resp = Renderer_Functions.send_request("XSRF-TOKEN=6a939c0b-ad7b-44ce-9a07-947f6e34bd24", user, "templatesphonemes")

        # here we will check if the request is passed or failed
        if resp["status_code"] == '401' \
            and (self.assertEqual(resp["details"], "Unauthorized Access") is None):
                print ("-|-Result:Test case Pass")
                print ("resp: {0}".format(resp))
                print ("Status Code: {0}".format(resp["status_code"]))
                print ("Reason: {0}".format(resp["details"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_6_templates__phonemes_missing_project_id(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        is_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get distinct labeling ids
        cursor = Postgres.connect_to_db()
        query = "SELECT DISTINCT labeling_id FROM nvc_labels"
        cursor.execute(query)
        labeling_ids = [r for r in cursor]
        id = random.choice(labeling_ids)[0]

        user = {"domain_id": random_domain, "is_template": is_template, "id": id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesphonemes")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and(self.assertEqual(resp['results']["error_message"], "missing parameter project_id.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_7_templates_phonemes_incorrect_project_id(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        is_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get distinct labeling ids
        cursor = Postgres.connect_to_db()
        query = "SELECT DISTINCT labeling_id FROM nvc_labels"
        cursor.execute(query)
        labeling_ids = [r for r in cursor]
        id = random.choice(labeling_ids)[0]

        user = {"project_id": None, "domain_id": random_domain, "is_template": is_template, "id": id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesphonemes")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and (
                self.assertEqual(resp['results']["error_message"], "incorrect value for project_id.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_8_templates_phonemes_missing_domain_id(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        is_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get distinct labeling ids
        cursor = Postgres.connect_to_db()
        query = "SELECT DISTINCT labeling_id FROM nvc_labels"
        cursor.execute(query)
        labeling_ids = [r for r in cursor]
        id = random.choice(labeling_ids)[0]

        user = {"project_id": random_project, "is_template": is_template, "id": id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesphonemes")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and (self.assertEqual(resp['results']["error_message"], "missing parameter domain_id.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_9_templates_phonemes_incorrect_domain_id(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        is_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get distinct labeling ids
        cursor = Postgres.connect_to_db()
        query = "SELECT DISTINCT labeling_id FROM nvc_labels"
        cursor.execute(query)
        labeling_ids = [r for r in cursor]
        id = random.choice(labeling_ids)[0]

        user = {"project_id": random_project, "domain_id": None, "is_template": is_template, "id": id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesphonemes")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and (
                self.assertEqual(resp['results']["error_message"], "incorrect value for domain_id.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_10_templates_phonemes_missing_id(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        is_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        user = {"project_id": random_project, "domain_id": random_domain, "is_template": is_template}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesphonemes")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and (self.assertEqual(resp['results']["error_message"], "missing parameter id.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_11_templates_phonemes_incorrect_id(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        is_template = False

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        user = {"project_id": random_project, "domain_id": random_domain, "is_template": is_template, "id": None}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesphonemes")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and (self.assertEqual(resp['results']["error_message"], "incorrect value for id.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_12_templates_phonemes_missing_is_templates(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get distinct labeling ids
        cursor = Postgres.connect_to_db()
        query = "SELECT DISTINCT labeling_id FROM nvc_labels"
        cursor.execute(query)
        labeling_ids = [r for r in cursor]
        id = random.choice(labeling_ids)[0]

        user = {"project_id": random_project, "domain_id": random_domain, "id": id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesphonemes")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and (self.assertEqual(resp['results']["error_message"], "missing parameter is_template.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False

    def test_negative_13_templates_phonemes_incorrect_is_templates(self):

        email = self.config["LOGIN"]["email"]

        # Login and get token
        resp = Renderer_Functions.initiate_login()

        # Parameters
        language_id = 6
        is_template = ''.join(random.choice(string.ascii_letters) for ii in range(5))

        # Get allowed projects
        allowed_projects = Renderer_Functions.get_allowed_projects_that_contains_allowed_domains(email, language_id)
        random_project = random.choice(allowed_projects)

        # Get project allowed domains
        # open_to_public is False: Get domains that belong to the user
        domains = Renderer_Functions.get_allowed_domains_in_specific_project(email, language_id, random_project)
        random_domain = random.choice(domains)[0]

        # Get distinct labeling ids
        cursor = Postgres.connect_to_db()
        query = "SELECT DISTINCT labeling_id FROM nvc_labels"
        cursor.execute(query)
        labeling_ids = [r for r in cursor]
        id = random.choice(labeling_ids)[0]

        user = {"project_id": random_project, "domain_id": random_domain, "is_template": is_template, "id": id}
        resp = Renderer_Functions.send_request(resp["cookies"][0], user, "templatesphonemes")

        # here we will check if the request is passed or failed
        if resp["status_code"] == 200 \
                and (
                self.assertEqual(resp['results']["error_message"], "incorrect value for is_template.") is None):
            print("-|-Result:Test case Pass")
            print("resp: {0}".format(resp))
            print("Status Code: {0}".format(resp["status_code"]))
            print("Reason: {0}".format(resp['results']["error_message"]))
        # Check error type
        else:
            print("Negative Test Failed")
            assert False


if __name__ == '__main__':
    print("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
