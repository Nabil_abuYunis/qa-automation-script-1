
This script is for the ProsodyMain tool.

Required installation to run this script:
There is no required additional installations to run this script other than the already built in libraries.

Before running the script, these folders and files must exist:
�conf.txt� file, script renames the output files according to the values in this file. 
Script must be placed beside "ProsodyMain.exe". 
Script would process the text files from "testFolder" and also save the output to this folder.
Within �testFolder�, there should be two folder, smoketest and functionaltest, one for each test type.

How to run the script:
In order run this script, you need to enter "Python batch_synth.exe", then the script asks you to enter a voice index and the test type.

More info about the script
After running this script a log file will be created, this file lists the script run step by step. The script also does check after each step if it was processed successfully. The script will also calculate how much time it took to synthesize each lab file, displaying these times when finishing each file and also list the files and their process time at the end when finishing synthesizing all the files.


Script run steps:
1. Get the names of the text files from the specific test type folder (smoketest/functionaltest) in "testfolder".
2. Create a folder according to the text file name.
3. Run the command line with each text file.
4. Rename the output wav files and copy them to their own folder.
5. Rename the output lab files and copy them to their own folder.
6. Delete the help file (with suffix temp) for the smoke tests.
7. Calculate the time taken to process the files.
