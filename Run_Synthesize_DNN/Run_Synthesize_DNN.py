import glob
import os
import shutil
import subprocess
import sys
import logging
import datetime
import zipfile
import ConfigParser

main_folder = os.getcwd()
run_time = {}  # to save run time process for all lab files
testfolder_path = main_folder + '/testFolder/'
synthesis_path = main_folder + '/synth_dnn/'
# Connection to share folder is already created manually
share_folder = r'/mnt/8-share/'

# import info from conf.txt
configParser = ConfigParser.RawConfigParser()
configFilePath = r'conf.txt'
configParser.read(configFilePath)
training_version = configParser.get('Config', 'Training Version')
training_sentences = configParser.get('Config', 'Training Sentences')
synthesize_version = configParser.get('Config', 'Synthesize Version')
type_t = configParser.get('Config', 'Type')
file_suffix = '.' + synthesize_version + 'S' + type_t + '.nvc_'\
              + filter(str.isdigit, training_sentences) + type_t + '_' + training_sentences
folder_name = 'V' + filter(str.isdigit, training_version) + '_' + type_t + '_' + training_sentences + '_'\
              + 'V' + filter(str.isdigit, synthesize_version)

# create logger for logging purposes
lgr = logging.getLogger('resultfile')
lgr.setLevel(logging.DEBUG)
# add a file handler
fh = logging.FileHandler('resultfile.log')
fh.setLevel(logging.DEBUG)
# create a formatter and set the formatter for the handler.
frmt = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh.setFormatter(frmt)
# add the Handler to the logger
lgr.addHandler(fh)


# Compress the output folder
def zip_dir(zipname, dir_to_zip):
    dir_to_zip_len = len(dir_to_zip.rstrip(os.sep)) + 1
    with zipfile.ZipFile(zipname, mode='w', compression=zipfile.ZIP_DEFLATED) as zf:
        for dirname, subdirs, files in os.walk(dir_to_zip):
            for filename in files:
                path = os.path.join(dirname, filename)
                entry = path[dir_to_zip_len:]
                zf.write(path, entry)

#########################################################
# 1. Create a folder according to the Config file values, if it does not exist
#########################################################
if not os.path.exists(testfolder_path + folder_name):
    os.makedirs(testfolder_path + folder_name)
# Check if folder was created
if not os.path.exists(testfolder_path + folder_name):
    lgr.warn('1. Directory was not created')
    sys.exit()
else:
    lgr.info('1. Directory {0} was created Successfully'.format(folder_name))

#########################################################
# 2. Get the name of all the lab files from the testFolder
#########################################################
lgr.info('2. Getting lab files names from testFolder')
files_names = glob.glob(testfolder_path + '/*.lab')
lgr.info('Files found: {0}'.format(files_names))
# if there are no files in TestFolder then exit
if files_names == "":
    lgr.warn('TestFolder is empty')
    sys.exit()

for file_name in files_names:
    lgr.info('##########################################\n'
             'Synthesizing file: {0}'.format(file_name))
    start_time = datetime.datetime.now().replace(microsecond=0)

    ##########################################
    # 3. Clean the wav folder for the new run
    ##########################################
    lgr.info('3. Deleting wav folder content')

    for item in os.listdir(synthesis_path + 'wav'):
        os.remove(synthesis_path + 'wav/' + item)
    # Check if res folder is empty
    if not os.listdir(synthesis_path + 'wav'):
        lgr.info('wav folder is empty')
    else:
        lgr.warn('wav folder was not cleaned')

    #########################################################
    # 4. Delete the old label file "kristin_test.lab" in for_dnn_synth folder if exist
    #    in order to replace it with a new one
    #########################################################
    # lgr.info('4. Deleting the current "kristin_test.lab" in for_dnn_synth in order to replace it with a new one')
    # if os.path.exists(synthesis_path + 'kristin_test.lab'):
    #     os.remove(synthesis_path + 'kristin_test.lab')
    # # Check if file was deleted
    # if os.path.exists(synthesis_path + 'kristin_test.lab'):
    #     lgr.warn('kristin_test.lab was not deleted')
    # else:
    #     lgr.info('kristin_test.lab is deleted')

    #########################################################
    # 5. Copy a label file from "testFolder" to "for_dnn_synth" directory
    #########################################################
    file_name_split = file_name.split('/')
    filename_only = file_name_split[-1]
    lgr.info('5. Copying a label file from "testFolder" to synthesis directory')
    shutil.copy2(file_name, synthesis_path + filename_only)
    # Check if "kristin_test.lab" was copied successfully
    if not os.path.exists(synthesis_path + filename_only):
        lgr.warn('{0} was not copied'.format(filename_only))
        continue
    else:
        lgr.info('{0} was copied successfully'.format(filename_only))

    #########################################################
    # 6. Run run_full_voice.sh file
    #########################################################
    lgr.info('6. Running run_full_voice.sh')
    os.chdir(synthesis_path)
    # process = subprocess.Popen(synthesis_path + 'run_full_voice.sh', file_name, stdout=subprocess.PIPE)

    command = [synthesis_path + 'run_full_voice.sh', filename_only]
    process = subprocess.Popen(command, stdout=subprocess.PIPE)

    out, err = process.communicate()
    process.wait()
    lgr.info(out)
    lgr.info('Process run_full_voice.sh is finished')

    #########################################################
    # 7. Copy the synthesized wav files from the "wav" folder to the "testFolder" folder
    # create a new folder with the synthesized source file name and compress it
    #########################################################
    lgr.info('7. copying the synthesized wav files from the "wav" folder to the "testFolder" folder')
    lgr.info('moving wav files in "wav" folder to the new created folder {0}'.format(folder_name))
    # move wav files in "Res" folder to the output folder
    for item in os.listdir(synthesis_path + 'wav'):
        base, ext = os.path.splitext(item)
        if ext == '.wav':
            s = os.path.join(synthesis_path + 'wav', item)
            item = item.split('.')
            d = os.path.join(testfolder_path + folder_name, item[0] + '.' + item[1] + file_suffix + '.wav')
            shutil.move(s, d)

    # Time took to synthesize the file
    end_time = datetime.datetime.now().replace(microsecond=0)
    # gather the files and their run time to be displayed at the end of the log file
    run_time[file_name] = end_time - start_time
    lgr.info('time taken to process this file: {0}'.format(run_time[file_name]))

# Compress the output folder
zip_dir(testfolder_path + folder_name + '.zip', testfolder_path + folder_name)

# send the compressed file to the share folder
print ('Uploading the compressed folder...')
lgr.info('Uploading the compressed folder...')

shutil.copy2(testfolder_path + folder_name + '.zip', share_folder + folder_name + '.zip')
# check if file was uploaded successfully
print ('upload path check: {0}'.format(share_folder + folder_name + '.zip'))
if os.path.exists(share_folder + folder_name + '.zip'):
    lgr.info('Compressed file was uploaded Successfully')
else:
    lgr.warn('Compressed file upload is failed')

# Conclude the proccessed files and it's run time
lgr.info('All files run times:')
for k, v in run_time.items():
    lgr.info('File: {0}, Time: {1}  '.format(k, v))
