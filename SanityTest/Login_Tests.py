import urllib.error as urllibe
import unittest
import flask
import json
import Renderer_Functions as Renderer_Functions


class VBLoginSuites(unittest.TestCase):

    currentResult = None  # holds last result object passed to run method'
    TestResult = {}
    Summary = {}

    def create_app(self):
        app = flask(__name__)
        app.config['TESTING'] = True
        return app

    def setUp(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Start run test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")

    def run(self, result=None):
        self.currentResult = result  # remember result for use in tearDown
        unittest.TestCase.run(self, result)  # call superclass run method

    def tearDown(self):
        print ("\n---------------------------------------------------------------------------------------------")
        print ("Completed running test case: {0}".format(str(self.id())))
        print ("-----------------------------------------------------------------------------------------------\n")
        print (self.currentResult)

    def test_positive_1_login_verify_user_can_login_with_valid_username_password(self):
        # parameters
        email = "hung@speechmorphing.com"
        password = "abc123"
        user = {"email": email, "password": password}
        resp = Renderer_Functions.loggin(user)

        if resp["status_code"] == 200:
            try:
                email_address = resp["content"]['results']["user"]["email_address"]
                print(email_address)
                if (self.assertEqual(resp["status_code"], 200)) is None \
                        and (self.assertEqual(email_address, email) is None):
                    print ("-|-Result:Test case Pass")
                    print ("resp: {0}".format(resp))
                    print ("User: {0}".format(resp["content"]["results"]["user"]["email_address"]))
                    print ("Token: {0}".format(resp["cookies"]))
                    print ("Status code: {0}".format(resp["status_code"]))

            except urllibe.URLError as e:   # if response code is 200 but the fetched email is
                                            # different than the requested
                print ("-|-Result:Test case Failed")
                print ("Reason: {0}".format(e.reason))

        # Check error type
        elif resp["status_code"] != 200:
            print ("-|-Result:Test case Failed")
            print ("Status Code: {0}".format(resp["status_code"]))
            print ("Reason: {0}".format(resp["details"]))
            print ("Error Code: {0}".format(resp["error_code"]))

    

if __name__ == '__main__':
    print ("-------------Test Result----------------\n")
    testResult = unittest.main(verbosity=1)
